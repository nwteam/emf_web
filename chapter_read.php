<?php
session_start();
require 'util/include.php';

$detect = new Mobile_Detect();
if ($detect->isMobile() || $detect->isTablet()) {
    
} else {
    //redirect(LP_PATH . 'pc.html');
}

$sysdate = date('Y-m-d', time());
$systime = date('Y-m-d H:i:s', time());

$login_id = $_SESSION['login_id'];

$b_id = $_GET['b_id'];
$c_id = $_GET['c_id'];
$b_name = $_GET['b_name'];
$b_auth = $_GET['b_auth'];

$r_chapter = mysqli_query($link, "select * from mz_chapter where chapter_id = '" . $c_id . "' and book_id = '" . $b_id . "'");
$row_chapter = mysqli_fetch_array($r_chapter);
$folder = $row_chapter['page_folder_name'];
$total_page = $row_chapter['total_page'];
$r_page = mysqli_query($link, "select * from mz_read_history where chapter_id = '" . $c_id . "' and book_id = '" . $b_id . "' and login_id = '" . $login_id . "'");
$row_page = mysqli_fetch_array($r_page);
$last_page = $row_page['page_no'];
if ($last_page == '') {
    $last_page = '1';
}
?>
<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
        <title>EMF</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
        <script src="js/modal.js"></script>
        <script src="js/cmn.js"></script>
        <script src="js/jquery.flexslider-min.js"></script>
        <script src="js/modernizr-transitions.js"></script>
        <script src="js/jquery.masonry.js"></script>
        <link href="css/base.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/flexslider.css" rel="stylesheet" media="all">
        <link rel="shortcut icon" href="images/favicon.ico">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <script>
            $(function() {
                var strCookie = document.cookie;
                var arrCookie = strCookie.split("; ");
                var page_no = 1;
                for (var i = 0; i < arrCookie.length; i++) {
                    var arr = arrCookie[i].split("=");
                    if ("<?php echo $b_id; ?>" == arr[0] && "<?php echo $c_id; ?>" == arr[1]) {
                        page_no = arr[2];
                        document.cookie = "last_page=" + page_no;
                        break;
                    }
                }
                if (page_no == 1) {
                    $("#lastRead").css("display", "none");
                }
            });
        </script>
        <script>
            function moveToLastPage() {
                var strCookie = document.cookie;
                var arrCookie = strCookie.split("; ");
                var page_no = 1;
                for (var i = 0; i < arrCookie.length; i++) {
                    var arr = arrCookie[i].split("=");
                    if ("<?php echo $b_id; ?>" == arr[0] && "<?php echo $c_id; ?>" == arr[1]) {
                        page_no = arr[2];
                        document.cookie = "last_page=" + page_no;
                        break;
                    }
                }
                if (page_no == 1) {
                    $("#lastRead").css("display", "none");
                }
                var pageurl = "read.php?page=" + page_no + "&folder=<?php echo $folder; ?>&total_page=<?php echo $total_page; ?>&c_id=<?php echo $c_id; ?>&b_id=<?php echo $b_id; ?>&b_name=<?php echo $b_name; ?>&b_auth=<?php echo $b_auth ?>";
                window.location.href = pageurl;
            }
            
            function readPage(pageurl) {
                window.location.href = pageurl;
            }
        </script>
        <style>
            .header img {
                width: 112px;
                height: auto;
                z-index: -1;
                float: left;
            }  
        </style>
    </head>
    <body>
        <header class="clearfix">
            <a class="header" href="<?php echo LP_PATH ?>index.php"><img src="images/logo-f.png" alt="EMF"></a><!--M-->
            <h1 style="margin-left:20px;vertical-align: bottom;" class="mangaTitle">
                <a style="color:white;" href="chapter.php?b_id=<?php echo $b_id ?>&b_name=<?php echo $b_name; ?>&b_auth=<?php echo $b_auth; ?>" title="<?php echo $b_name; ?>">
                    <?php echo $b_name; ?>
                </a>
            </h1>
        </header>
        <section id="volRead">
            <dl>
                <dt>
                <img src="<?php echo COMIC_PATH?><?php echo $row_chapter['cover_img_path']; ?>" alt="<?php echo $row_chapter['chapter_name']; ?>"  onclick="readPage('read.php?page=1&folder=<?php echo $folder; ?>&total_page=<?php echo $total_page; ?>&c_id=<?php echo $c_id; ?>&b_id=<?php echo $b_id; ?>&b_name=<?php echo urlencode($b_name) ?>&b_auth=<?php echo $b_auth ?>');"/>
                </dt>
                <dd>
                    <?php echo $row_chapter['sub_title']; ?>　<?php echo $row_chapter['chapter_name']; ?>
                </dd>
            </dl>
            <ul>
                <li><a class="read_start_btn" href="read.php?page=1&folder=<?php echo $folder; ?>&total_page=<?php echo $total_page; ?>&c_id=<?php echo $c_id; ?>&b_id=<?php echo $b_id; ?>&b_name=<?php echo urlencode($b_name); ?>&b_auth=<?php echo $b_auth ?>">最初から読む</a></li>

                <li id="lastRead"><a class="read_start_btn" href='#' onclick='moveToLastPage()'>途中から読む</a></li>
            </ul>
        </section>
        <br /><br />

        <div style="margin: 0 auto; width: 320px;">
            <!--  バナーエリア⑥-->
            <?php if ($_SESSION['ad_flg'] == 0) { ?>

            <?php } else { ?>

            <?php } ?>
        </div>
        <footer>
            <p><small>&#169; EMF</small></p>
        </footer>
        <script>
                    $(function() {
                        $(".menu").css("display", "none");
                        $(".button-toggle").on("click", function() {
                            $(".menu").slideToggle();
                        });
                    });
        </script>
    </body>
    <?php include_once("analyticstracking.php") ?>
</html>