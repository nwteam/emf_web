<?php
require_once 'util/include.php';
include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック
$nav_no="nav4";

$month = date('m', time());
$week = date('W', time());

if ($action == 'month') {
    $week_active=false;
    $month_active=true;
    //月間
    $sql = "SELECT * FROM `mz_book_read_cnt` WHERE 1 and month='".$month."' group by `book_id`,`year`,`month` order by sum(read_cnt) desc";
}else{
    $week_active=true;
    $month_active=false;
    //週間
    $sql = "SELECT * FROM `mz_book_read_cnt` WHERE 1 and week='".$week."' group by `book_id`,`year`,`week` order by sum(read_cnt) desc";
}

$r_new_books = mysqli_query($link, $sql);
//カテゴリプルダウンリスト取得
include("common/common_category_list.php");
//新着のチャプターを取得
$newChapters = array();
while ($row = mysqli_fetch_array($r_new_books)) {
    $newChapters[] = $row;
}
?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top">
<header id="header">
    <?php include("common/common_header.php") ?>
    <?php include("common/common_nav.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
    <div class="inner clearfix">
        <div class="contents">
            <h2><span class="h_ranking gold">ランキング</span></h2>
            <div class="ranking-menu">
                <p class="btn-ranking"><a href="rank.php?action=week" <?php if($week_active){echo "class='active'";}?>><span>週刊ランキング</span></a><a href="rank.php?action=month"<?php if($month_active){echo "class='active'";}?>><span>月刊ランキング</span></a></p>
            </div>
            <div class="ranking-area">
                <ul class="ranking-list clearfix">
                    <?php $i=1; ?>
                    <?php foreach ($newChapters as $chapter) { ?>
                        <?php $newPageUrl = "index.php?action=readCnt&b_id=" . $chapter['book_id'] . "&b_name=" . urldecode($chapter['book_name']) . "&b_auth=" . urlencode($chapter['book_auth']); ?>
                        <?php $newImgUrl = COMIC_PATH . $chapter['cover_img_path']; ?>
                        <?php $newBookName = $chapter['book_name'] ?>
                        <?php $newBookAuth = $chapter['book_auth'] ?>
                        <?php if(mb_strlen($newBookName)>28){$newBookName = mb_substr($newBookName,0,28)."...";}?>
                        <?php if(mb_strlen($newBookAuth)>9){$newBookAuth = mb_substr($newBookAuth,0,9)."...";}?>
                        <?php if($i<=10){$rank_no = "no".$i;}else{$rank_no = "";} ?>
                        <li><a href="<?php echo $newPageUrl ?>"> <span class="image" style="background: url(<?php echo $newImgUrl ?>) 50% 50% no-repeat #fff;  background-size:140px auto;"><span class="rank <?php echo $rank_no;?>"></span></span> <span class="title"><?php echo $newBookName ?><span class="name"><?php echo $newBookAuth ?></span></span> </a>
                            <ul class='category clearfix'>
                                <?php
                                /* ジャンル */
                                $cateIds = explode(",", $chapter['cat_id']);
                                if (!empty($cateIds)) {
                                    foreach ($cateIds as $cateId) {
                                        ?>
                                        <li>
                                            <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <?php $i++; ?>
                    <?php } ?>
                </ul>
                <!--
                <div class="_pagination_container">
                    <p class="count">140504タイトル中1～28タイトル1ぺージ目を表示</p>
                    <nav class="pagination"> <a href="index.html" class="prev">&lt;</a> <a href="index.html">1</a> <a href="index.html">2</a> <a href="index.html">3</a> <span>4</span> <a href="index.html">5</a> <span class="dot">...</span> <a href="index.html">5000</a> <a href="index.html" class="next">&gt;</a> </nav>
                </div>
                -->
            </div>
        </div>
        <!-- /.contents-->
        <?php include("common/common_side.php") ?>
    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
</body>
<?php include_once("analyticstracking.php") ?>
</html>
