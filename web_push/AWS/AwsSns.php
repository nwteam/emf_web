<?php

require_once 'aws.phar';

use Aws\Common\Aws;
use Aws\Common\Enum\Region;
use Aws\Sns\SnsClient as SnsClient;

/**
 * ----- AmazonSNS 利用手順 -----
 * ■iOS
 * http://dev.classmethod.jp/cloud/aws-amazon-sns-mobile-push-ios/
 *
 * ■Android
 * http://dev.classmethod.jp/smartphone/android/amazon-sns-mobile-push/
 * 
 * ■AWS SDK for PHP（AwsSNSのクラスドキュメント）
 * http://docs.aws.amazon.com/aws-sdk-php/latest/class-Aws.Sns.SnsClient.html
 * 
 * ■PHPサンプル
 * http://qiita.com/uda0922/items/262d0ed0e70136207c81
 * http://aws.typepad.com/aws_japan/2013/08/
 * 
 * ■ハマるポイント（EndpointArnのEnabled属性）
 * http://qiita.com/ykf/items/4978a9ccf0dd1cc6a19b
 * 
 * ■AWS_ACCESS_KEY / AWS_SECRET_KEY
 * ①http://aws.amazon.com の右上の Accountから「Security Credentials」へ移動。
 * 　Access Keys（Access Key ID and Secret Access Key）メニューから「Create New Access Key」をクリック。
 * 　生成された「Access Key ID」「Secret Access Key」を取得する。
 *    ==> プログラム定数「AWS_ACCESS_KEY」と「AWS_SECRET_KEY」にセットする
 * 
 * ■Topicは作っておく
 */

/**
 * AWS-SNS利用クラス
 */
class AwsSns {

    /**
     * 環境設定用の定数
     */
    const AWS_ACCESS_KEY = 'AKIAIEMRT5VT3YMXPNZQ';
    const AWS_SECRET_KEY = '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';
    const AWS_TOPIC_TOPIC_OWNER = '702966196430';
    const AWS_REGION = 'ap-northeast-1';

    /**
     * 環境設定用のメンバ変数（「AmazonSNS ManagementConsole」で登録した情報）
     */
    public $sendPrams = array(
        array(//iOS - APNS
            'AppArn' => '',
            'Name' => 'APNS',
            'TopicName' => 'manzoku-apns',
        ),
        array(//Android - GCM
            'AppArn' => 'arn:aws:sns:ap-northeast-1:702966196430:app/GCM/manzoku-gcm',
            'Name' => 'GCM',
            'TopicName' => 'manzoku-gcm',
        ),
    );

    const APNS = 0;
    const GCM = 1;

    private $client = null;

    /**
     * インスタンス生成＆初期設定
     */
    private function getInstance() {
        if (is_null($this->client)) {
            $this->client = SnsClient::factory(array(
                    'key' => self::AWS_ACCESS_KEY,
                    'secret' => self::AWS_SECRET_KEY,
                    'region' => Region::TOKYO
            ));
        }
        return $this->client;
    }

    /**
     * ＜処理概要＞
     * DeviceTokenを登録し、AwsSnsから受信した「EndpointArn（端末との紐付け情報）」を返却する。
     * EndpointArnを該当トピックに紐づける（トピックはiOS/Android毎に分かれる）
     * 
     * @param  int    $appsId    APPSのID（利用するApplicationARNのID）                         
     *                              =self::APNS
     *                              =self::GCM
     * @param  string $pushId    DeviceToken（iOS）、または RegistrationID（Android）
     * @return array  $result（array：成功、false：失敗）
     * ---- 成功時のarray内容 -----
     * $result['TopicArn']
     *        ['EndpointArn']
     */
    public function createPlatformEndpoint($appsId, $pushId) {

        /**
         * AmazonSNSにEndpointを登録（既に登録済の場合にはEndpointの取得のみ）
         */
        $options = array(
            'PlatformApplicationArn' => $this->sendPrams[$appsId]['AppArn'],
            'Token' => $pushId
        );
        try {
            $resEp = $this->getInstance()->createPlatformEndpoint($options);
        } catch (Exception $e) {
            preg_match("/(.*)(Token Reason: Endpoint )(.*)( already exists)/is", $e->getMessage(), $retArr);
            if (isset($retArr[3])) { //Endpointは既に登録済か？
                $resEp = array(
                    'EndpointArn' => trim($retArr[3]), //【補足】Endpointを１つだけ取得するAPIが無い為、例外メッセージから取得する
                );
                $this->log(__METHOD__ . ": EndPointArn[Already]=" . $resEp['EndpointArn'], 'debug');
            } else {
                $resEp = false;
            }
            $this->log(__METHOD__ . ": " . $e->getMessage(), 'debug');
        }

        if (empty($resEp)) { //Endpoint登録or取得失敗か？
            return false;
        }

        /**
         * 紐づけるTopicを作成する（既にTopicが作成済みの場合には処理しない）
         */
        $topicName = $this->sendPrams[$appsId]['TopicName'];
        $resTp = $this->client->createTopic(array(
            'Name' => $topicName,
        ));
        if (empty($resTp)) {
            $this->log(__METHOD__ . ": トピック[{$topicName}]の作成に失敗しました", 'debug');
            return false;
        }

        /**
         * EndpointをTopicに紐づける
         */
        $retSb = $this->client->subscribe(array(
            'TopicArn' => $resTp['TopicArn'],
            'Protocol' => 'application',
            'Endpoint' => $resEp['EndpointArn'],
        ));
        if (empty($retSb)) {
            $this->log(__METHOD__ . ": TopicArn[" . $resTp['TopicArn'] . "]に Endpoint[" . $resEp['EndpointArn'] . "] を紐付けできませんでした", 'debug');
            return false;
        }

        $result = array(
            'TopicArn' => $resTp['TopicArn'],
            'EndpointArn' => $resEp['EndpointArn'],
            'SubscriptionArn' => $retSb['SubscriptionArn'],
        );
        return $result;
    }

    /**
     * ＜処理概要＞
     * DeviceTokenを利用してAwsSnsから「EndpointArn（端末との紐付け情報）」を取得する。
     * EndpointArnをAwsSNSから削除する
     * 
     * @param  string  $endpointArn
     * @param  string  $subscriptionArn
     * @return boolean $result（true：成功、false：失敗）
     */
    public function deleteEndpoint($endpointArn, $subscriptionArn) {

        /**
         * Endpointを削除する
         */
        $retSb = $this->getInstance()->deleteEndpoint(array(
            'EndpointArn' => $endpointArn,
        ));
        if (empty($retSb)) {
            $this->log(__METHOD__ . ": Endpoint[" . $endpointArn . "] を削除できませんでした", 'debug');
            return false;
        }

        /**
         * Subscriptionを解除する
         */
        $retSb = $this->client->unsubscribe(array(
            'SubscriptionArn' => $subscriptionArn,
        ));
        if (empty($retSb)) {
            $this->log(__METHOD__ . ": Subscription[" . $subscriptionArn . "] の購読を解除できませんでした", 'debug');
            return false;
        }

        return true;
    }

    /**
     * ＜処理概要＞
     * AmazonSNSにTopic指定でPush通知リクエスト送信する。
     * （iOS/Android別に送信）
     * 
     * @param  int    $appsId        APPSのID（利用するApplicationARNのID）
     *                                  =self::APNS    iOS      
     *                                  =self::GCM     Android
     * @param  string $message       送信メッセージ
     * @param  int    $badge         表示バッジ数
     * @return 結果
     */
    public function requestPushMessageByTopic($appsId, $message, $badge = 0) {
        try {
            $name = $this->sendPrams[$appsId]['Name'];
            $topicArnFormat = 'arn:aws:sns:' . self::AWS_REGION . ":" . self::AWS_TOPIC_TOPIC_OWNER . ":" . $name;

            if ($appsId == self::APNS) { //iOSか？
                $messageJson = json_encode(array(
                    'default' => $message,
                    $name => json_encode(array(
                        'aps' => array(
                            'sound' => 'default',
                            'alert' => $message,
                            'badge' => $badge,
                        )
                    )),
                ));
            } else { //Androidか？
                $messageJson = json_encode(array(
                    'default' => $message,
                    $name => json_encode(array(
                        'data' => array(
                            'message' => $message,
                        )
                    )),
                ));
            }

            /**
             * トピックNoに紐付いているユーザーにPush通知をリクエスト
             */
            $ret = $this->publishJson(array(
                'MessageStructure' => 'json',
                'TargetArn' => $topicArnFormat,
                'Message' => $messageJson,
            ));
        } catch (Exception $e) {
            $this->log(__METHOD__ . ": " . $e->getMessage(), 'debug');
            return false;
        }
        return $ret;
    }

    /**
     * ＜処理概要＞
     * AmazonSNSにEndPoint指定でPush通知リクエスト送信する。
     * （iOS/Android別、開発/リリース別に送信）
     * 
     * @param  int    $appsId        APPSのID（利用するApplicationARNのID）
     *                                  =self::APNS  iOS
     *                                  =self::GCM   Android
     * @param  int    $endPointArn   送信先のEndPointArn
     * @param  string $message       送信メッセージ
     * @param  int    $badge         表示バッジ数（iOSのみ有効）
     * @return 結果
     */
    public function requestPushMessageByEndPoint($appsId, $endPointArn, $message, $badge = 0) {
        try {
            $name = $this->sendPrams[$appsId]['Name'];
            $ret = $this->setEndpointEnable($endPointArn, 'true'); //Enable属性をtrue（Push通知可）に変更
            if (empty($ret)) { //変更失敗か？
                $this->log(__METHOD__ . ": [Cron] EndPointArn の trueセットに失敗しました（AppsID={$appsId}/EndPointArn={$endPointArn}", 'debug');
            }
            if ($appsId == self::APNS) { //iOSか？
                $ret = $this->publishJson(array(
                    'MessageStructure' => 'json',
                    'TargetArn' => $endPointArn,
                    'Message' => json_encode(array(
                        $name => json_encode(array(
                            'aps' => array(
                                'sound' => 'default',
                                'alert' => $message,
                                'badge' => $badge,
                            )
                        ))
                    ))
                ));
            } else { //Androidか？
                $ret = $this->publishJson(array(
                    'MessageStructure' => 'json',
                    'TargetArn' => $endPointArn,
                    'Message' => json_encode(array(
                        'default' => $message,
                        $name => json_encode(array(
                            'data' => array(
                                'message' => $message,
                            )
                        ))
                    ))
                ));
            }
        } catch (Exception $e) {
            $this->log(__METHOD__ . ": " . $e->getMessage(), 'debug');
            return false;
        }
        return $ret;
    }

    /**
     * Endpointを指定して、Push通知を送信（メッセージのみ）
     * 
     * @param string $message      送信するメッセージ
     * @param string $endpointArn  EndpointArn（端末との紐付け情報）
     * @return 結果（true:成功、false:失敗）
     */
    public function publish($message, $endpointArn) {
        try {
            $res = $this->getInstance()->publish(array(
                'Message' => $message,
                'TargetArn' => $endpointArn
            ));
        } catch (Exception $e) {
            $this->log(__METHOD__ . ": " . $e->getMessage(), 'debug');
            return false;
        }
        return $res;
    }

    /**
     * Endpointを指定して、Push通知を送信（JSON形式：メッセージ＋バッジが可能）
     * 
     * @param  array  $args  JSONデータ
     * @return 結果
     */
    public function publishJson(array $args) {
        try {
            $res = $this->getInstance()->publish($args);
        } catch (Exception $e) {
            $this->log(__METHOD__ . ": " . $e->getMessage(), 'debug');
            return false;
        }
        return $res;
    }

    /**
     * Endpointを指定して、Enable属性をセットする
     * 
     * @param  string  $endPointArn  EndPointArn
     * @param  boolean $value        設定値(true/false)
     * @return 結果
     */
    public function setEndpointEnable($endPointArn, $value) {
        try {
            $res = $this->getInstance()->setEndpointAttributes(array(
                'EndpointArn' => $endPointArn,
                'Attributes' => array(
                    'Enabled' => $value,
                ),
            ));
        } catch (Exception $e) {
            $this->log(__METHOD__ . ": " . $e->getMessage(), 'debug');
            return false;
        }
        return $res;
    }

    /**
     * ログの出力
     *
     * @param  $message 出力するメッセージ
     * @param  $level   ログレベル
     */
    public function log($message, $level = 'debug') {
        error_log($message, 0);
        echo $message;
    }

}
