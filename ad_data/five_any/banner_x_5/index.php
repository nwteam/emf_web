<?php if (defined('DIRECT_AD') && DIRECT_AD != "") { ?>
    <?php
    //最大表示広告数
    define("AD_DISP_MAX", 5);

    //ドキュメントルートからの相対パス取得
    $rootPath = str_replace($_SERVER["DOCUMENT_ROOT"], "", dirname(__FILE__));

    //広告データ一覧を取得
    $filename = dirname(__FILE__) . '/ad_list_utf8.csv';
    $handle = fopen($filename, "r");
    $adList = array();

    while ($array = fgetcsv($handle)) {
        if (!empty($array) && is_array($array)) {
            if (substr($array[0], 0, 1) == "#") {
                continue;
            }
            $adList[] = $array;
        }
    }

    //広告Noを取得(0相対)
    $adNo = file_get_contents(dirname(__FILE__) . '/ad_no.txt');
    if ($adNo === false) {
        $adNo = rand(0, sizeof($adList) - 1);
    } else {
        if (++$adNo > sizeof($adList) - 1) {
            $adNo = 0;
        }
    }

    //広告Noを保存
    file_put_contents(dirname(__FILE__) . '/ad_no.txt', $adNo);
    ?>

    <?php for ($i = 0; $i < AD_DISP_MAX; $i++) { ?>
        <a href="<?php echo $adList[$adNo][3] ?>" title="<?php echo $adList[$adNo][1] ?>" target="_blank" style="display:block;padding:5px;border-bottom:solid 1px #CCC;">
            <img src="<?php echo $rootPath ?>/images/<?php echo $adList[$adNo][0] ?>" alt="<?php echo $adList[$adNo][1] ?>?>" width="60" style="float:left;border:solid 1px #CCC;margin-right:5px;"/>
            <h2>
                <span style="font-weight: bold;color:#0E67D9">No.★ </span>&nbsp;
                <span style="font-size:15px;color:#FC1684;"><?php echo $adList[$adNo][1] ?></span>
            </h2>
            <p style="margin-top:5px;color:#000;line-height: 1.5em;"><?php echo nl2br($adList[$adNo][2]) ?></p>
            <div style="clear:both;"><hr style="display:none;" /></div>
        </a>

        <?php
        if (++$adNo > sizeof($adList) - 1) {
            $adNo = 0;
        }
        ?>

    <?php } ?>
<?php } ?>
