<?php if (defined('DIRECT_AD') && DIRECT_AD != "") { ?>
    <?php
    //ドキュメントルートからの相対パス取得
    $rootPath = str_replace($_SERVER["DOCUMENT_ROOT"], "", dirname(__FILE__));

    //広告データ一覧を取得
    $filename = dirname(__FILE__) . '/ad_list_utf8.csv';
    $handle = fopen($filename, "r");
    $adList = array();

    while ($array = fgetcsv($handle)) {
        if (!empty($array) && is_array($array)) {
            if (substr($array[0], 0, 1) == "#") {
                continue;
            }
            $adList[] = $array;
        }
    }

    //広告表示用の乱数を取得
    $rand = rand(0, sizeof($adList) - 1);
    ?>
    <a href="<?php echo $adList[$rand][3] ?>" title="<?php echo $adList[$rand][1] ?>" target="_blank" style="display:block;padding:5px;border-bottom:solid 1px #CCC;">
        <img src="<?php echo $rootPath ?>/images/<?php echo $adList[$rand][0] ?>" alt="<?php echo $adList[$rand][1] ?>?>" width="60" style="float:left;border:solid 1px #CCC;margin-right:5px;"/>
        <h2>
            <span style="font-weight: bold;color:#0E67D9">No.★ </span>&nbsp;
            <span style="font-size:15px;color:#FC1684;"><?php echo $adList[$rand][1] ?></span>
        </h2>
        <p style="margin-top:5px;color:#000;line-height: 1.5em;"><?php echo nl2br($adList[$rand][2]) ?></p>
        <div style="clear:both;"><hr style="display:none;" /></div>
    </a>
<?php } ?>