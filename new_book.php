<?php
require_once 'util/include.php';
include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック
$nav_no="nav1";
$showrow = 28;
$curpage = empty($_GET['page']) ? 1 : $_GET['page'];
$url = "?page={page}";
//新着漫画を28件取得
$sql = "SELECT DISTINCT mz_book.*";
$sql .= " FROM mz_book FORCE INDEX(unique_book_id)";
$sql .= " INNER JOIN mz_chapter ON mz_book.book_id = mz_chapter.book_id";
$sql .= " WHERE mz_chapter.insert_time <= '" . time() . "'";
$sql .= " ORDER BY mz_book.insert_time DESC";
$total = mysqli_num_rows(mysqli_query($link,$sql));
if (!empty($_GET['page']) && $total != 0 && $curpage > ceil($total / $showrow)){
    $curpage = ceil($total_rows / $showrow);
}
//$sql .= " LIMIT 28;";
$sql .= " LIMIT " . ($curpage - 1) * $showrow . ",$showrow;";
$r_new_books = mysqli_query($link, $sql);
$newChapters = array();
while ($row = mysqli_fetch_array($r_new_books)) {
    $newChapters[] = $row;
}
//本月更新予定情報取得
$this_month_first = date('Y-m-01', strtotime($sysdate));
$this_month_last = date('Y-m-d', strtotime("$this_month_first +1 month -1 day"));
$this_year = date('Y', strtotime($sysdate));
$this_month = date('m', strtotime($sysdate));
$sql = "SELECT * FROM mz_book WHERE book_id IN (SELECT DISTINCT book_id FROM mz_chapter WHERE insert_time BETWEEN unix_timestamp('".$this_month_first."') AND unix_timestamp('".$this_month_last."')) ORDER BY insert_time DESC";
$r_this_month = mysqli_query($link, $sql);
$this_month_arr = array();
while ($row_this_month = mysqli_fetch_array($r_this_month)) {
    $this_month_arr[] = $row_this_month;
}
//カテゴリプルダウンリスト取得
include("common/common_category_list.php");
//読み回数計数
include("common/common_book_read_cnt.php");
?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top">
<header id="header">
    <?php include("common/common_header.php") ?>
    <?php include("common/common_nav.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
    <div class="inner clearfix">
        <div class="contents">
            <h2><span class="h_new pink">新着マンガ</span></h2>
            <ul class="new-list clearfix">
                <?php foreach ($newChapters as $chapter) { ?>
                    <?php $newPageUrl = "index.php?action=readCnt&b_id=" . $chapter['book_id'] . "&b_name=" . urldecode($chapter['book_name']) . "&b_auth=" . urlencode($chapter['book_auth']); ?>
                    <?php $newImgUrl = COMIC_PATH . $chapter['cover_img_path']; ?>
                    <?php $newBookName = $chapter['book_name'] ?>
                    <?php $newBookAuth = $chapter['book_auth'] ?>
                    <?php if(mb_strlen($newBookName)>18){$newBookName = mb_substr($newBookName,0,18)."...";} ?>
                    <?php if(mb_strlen($newBookAuth)>9){$newBookAuth = mb_substr($newBookAuth,0,9)."...";} ?>
                        <li><a href="<?php echo $newPageUrl ?>"> <span class="image" style="background: url(<?php echo $newImgUrl ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $newBookName ?><span class="name"><?php echo $newBookAuth ?></span></span> </a>
                        <ul class='category clearfix'>
                            <?php
                            /* ジャンル */
                            $cateIds = explode(",", $chapter['cat_id']);
                            if (!empty($cateIds)) {
                                foreach ($cateIds as $cateId) {
                                    ?>
                                    <li>
                                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        </li>
                <?php } ?>
            </ul>
            <?php
            if ($total > $showrow) {
                $page = new page($total, $showrow, $curpage, $url, 2);
                echo $page->myde_write();
            }
            ?>
            <h3><?php echo $this_year."年.".$this_month."月更新タイトル";?></h3>
            <ul class="update-list clearfix">
                <?php foreach ($this_month_arr as $this_m) { ?>
                    <?php $rec_book_url = "index.php?action=readCnt&b_id=" . $this_m['book_id'] . "&b_name=" . urldecode($this_m['book_name']) . "&b_auth=" . urlencode($this_m['book_auth']); ?>
                    <?php $rec_cover_img = COMIC_PATH . $this_m['cover_img_path']; ?>
                    <?php $rec_book_name = $this_m['book_name'] ?>
                    <?php $rec_book_auth = $this_m['book_auth'] ?>
                    <li><a href="<?php echo $rec_book_url ?>"> <span class="image" style="background: url(<?php echo $rec_cover_img ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $rec_book_name ?><span class="name"><?php echo $rec_book_auth ?></span></span> </a>
                        <ul class='category clearfix'>
                            <?php
                            /* ジャンル */
                            $cateIds = explode(",", $this_m['cat_id']);
                            if (!empty($cateIds)) {
                                foreach ($cateIds as $cateId) {
                                    ?>
                                    <li>
                                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
            <!--
                <div class="_pagination_container">
                    <p class="count">140504タイトル中1～28タイトル1ぺージ目を表示</p>
                    <nav class="pagination"> <a href="index.html" class="prev">&lt;</a> <a href="index.html">1</a> <a href="index.html">2</a> <a href="index.html">3</a> <span>4</span> <a href="index.html">5</a> <span class="dot">...</span> <a href="index.html">5000</a> <a href="index.html" class="next">&gt;</a> </nav>
                </div>
                -->


        </div>
        <!-- /.contents-->
        <?php include("common/common_side.php") ?>
    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
</body>
<?php include_once("analyticstracking.php") ?>
</html>
