<?php
require_once 'util/include.php';
include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック
$nav_no="nav2";
//*****************************//
//本月更新予定情報取得
$this_month_first = date('Y-m-01', strtotime($sysdate));
$this_month_last = date('Y-m-d', strtotime("$this_month_first +1 month -1 day"));
$this_year = date('Y', strtotime($sysdate));
$this_month = date('m', strtotime($sysdate));
$sql = "SELECT * FROM mz_book WHERE book_id IN (SELECT DISTINCT book_id FROM mz_chapter WHERE insert_time BETWEEN unix_timestamp('".$this_month_first."') AND unix_timestamp('".$this_month_last."')) ORDER BY insert_time DESC";
$r_this_month = mysqli_query($link, $sql);
$this_month_arr = array();
while ($row_this_month = mysqli_fetch_array($r_this_month)) {
    $this_month_arr[] = $row_this_month;
}
//来月
$next_month_first = date('Y-m-01', strtotime("$this_month_first +1 month"));
$next_month_last = date('Y-m-d', strtotime("$this_month_first +2 month -1 day"));
$next_year = date('Y', strtotime($next_month_first));
$next_month = date('m', strtotime($next_month_first));
$sql = "SELECT * FROM mz_book WHERE book_id IN (SELECT DISTINCT book_id FROM mz_chapter WHERE insert_time BETWEEN unix_timestamp('".$next_month_first."') AND unix_timestamp('".$next_month_last."')) ORDER BY insert_time DESC";
$r_next_month = mysqli_query($link, $sql);
$next_month_arr = array();
while ($row_next_month = mysqli_fetch_array($r_next_month)) {
    $next_month_arr[] = $row_next_month;
}
//再来月
$nn_month_first = date('Y-m-01', strtotime("$this_month_first +2 month"));
$nn_month_last = date('Y-m-d', strtotime("$this_month_first +3 month -1 day"));
$nn_year = date('Y', strtotime($nn_month_first));
$nn_month = date('m', strtotime($nn_month_first));
$sql = "SELECT * FROM mz_book WHERE book_id IN (SELECT DISTINCT book_id FROM mz_chapter WHERE insert_time BETWEEN unix_timestamp('".$nn_month_first."') AND unix_timestamp('".$nn_month_last."')) ORDER BY insert_time DESC";
$r_nn_month = mysqli_query($link, $sql);
$nn_month_arr = array();
while ($row_nn_month = mysqli_fetch_array($r_nn_month)) {
    $nn_month_arr[] = $row_nn_month;
}
//****************************//
//カテゴリプルダウンリスト取得
include("common/common_category_list.php");
//読み回数計数
include("common/common_book_read_cnt.php");
?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top">
<header id="header">
    <?php include("common/common_header.php") ?>
    <?php include("common/common_nav.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
    <div class="inner clearfix">
        <div class="contents">
            <h2><span class="h_update pink">更新予定</span></h2>
            <h3><?php echo $nn_year."年".$nn_month."月更新タイトル";?></h3>
            <ul class="update-list clearfix">
                <?php foreach ($nn_month_arr as $nn) { ?>
                    <?php $rec_book_url = "index.php?action=readCnt&b_id=" . $nn['book_id'] . "&b_name=" . urldecode($nn['book_name']) . "&b_auth=" . urlencode($nn['book_auth']); ?>
                    <?php $rec_cover_img = COMIC_PATH . $nn['cover_img_path']; ?>
                    <?php $rec_book_name = $nn['book_name'] ?>
                    <?php $rec_book_auth = $nn['book_auth'] ?>
                    <li><a href="<?php echo $rec_book_url ?>"> <span class="image" style="background: url(<?php echo $rec_cover_img ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $rec_book_name ?><span class="name"><?php echo $rec_book_auth ?></span></span> </a>
                        <ul class='category clearfix'>
                            <?php
                            /* ジャンル */
                            $cateIds = explode(",", $nn['cat_id']);
                            if (!empty($cateIds)) {
                                foreach ($cateIds as $cateId) {
                                    ?>
                                    <li>
                                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
            <h3><?php echo $next_year."年".$next_month."月更新タイトル";?></h3>
            <ul class="update-list clearfix">
                <?php foreach ($next_month_arr as $next_m) { ?>
                    <?php $rec_book_url = "index.php?action=readCnt&b_id=" . $next_m['book_id'] . "&b_name=" . urldecode($next_m['book_name']) . "&b_auth=" . urlencode($next_m['book_auth']); ?>
                    <?php $rec_cover_img = COMIC_PATH . $next_m['cover_img_path']; ?>
                    <?php $rec_book_name = $next_m['book_name'] ?>
                    <?php $rec_book_auth = $next_m['book_auth'] ?>
                    <li><a href="<?php echo $rec_book_url ?>"> <span class="image" style="background: url(<?php echo $rec_cover_img ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $rec_book_name ?><span class="name"><?php echo $rec_book_auth ?></span></span> </a>
                        <ul class='category clearfix'>
                            <?php
                            /* ジャンル */
                            $cateIds = explode(",", $next_m['cat_id']);
                            if (!empty($cateIds)) {
                                foreach ($cateIds as $cateId) {
                                    ?>
                                    <li>
                                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
            <h3><?php echo $this_year."年".$this_month."月更新タイトル";?></h3>
            <ul class="update-list clearfix">
                <?php foreach ($this_month_arr as $this_m) { ?>
                    <?php $rec_book_url = "index.php?action=readCnt&b_id=" . $this_m['book_id'] . "&b_name=" . urldecode($this_m['book_name']) . "&b_auth=" . urlencode($this_m['book_auth']); ?>
                    <?php $rec_cover_img = COMIC_PATH . $this_m['cover_img_path']; ?>
                    <?php $rec_book_name = $this_m['book_name'] ?>
                    <?php $rec_book_auth = $this_m['book_auth'] ?>
                    <li><a href="<?php echo $rec_book_url ?>"> <span class="image" style="background: url(<?php echo $rec_cover_img ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $rec_book_name ?><span class="name"><?php echo $rec_book_auth ?></span></span> </a>
                        <ul class='category clearfix'>
                            <?php
                            /* ジャンル */
                            $cateIds = explode(",", $this_m['cat_id']);
                            if (!empty($cateIds)) {
                                foreach ($cateIds as $cateId) {
                                    ?>
                                    <li>
                                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
<!--
            <div class="_pagination_container">
                <p class="count">140504タイトル中1～28タイトル1ぺージ目を表示</p>
                <nav class="pagination"> <a href="index.html" class="prev">&lt;</a> <a href="index.html">1</a> <a href="index.html">2</a> <a href="index.html">3</a> <span>4</span> <a href="index.html">5</a> <span class="dot">...</span> <a href="index.html">5000</a> <a href="index.html" class="next">&gt;</a> </nav>
            </div>
-->
        </div>
        <!-- /.contents-->
        <?php include("common/common_side.php") ?>
    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
</body>
<?php include_once("analyticstracking.php") ?>
</html>
