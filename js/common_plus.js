/*
 *サイトで使用できる共通処理です。
 */
$(function() {
  //========================================================
  // gnav
  //========================================================
  gnav = {};
  gnav.flag_open = false;
  gnav.pulldown;
  /******************************************
   * gnavオブジェクトの初期化
   */
  gnav.initialize = function() {
    gnav.hover_nav();
    gnav.hover_close_pulldown();
    gnav.hover_change_pulldown();
  };
  /******************************************
   *  プルダウンのオープン処理
   */
  gnav.hover_nav = function(){
    $('.gnav ul li') .hover(function(){
      $('.gnav ul li').removeClass('active');
      $(this).addClass('active');
      gnav.pulldown = $(this).attr('data-pulldown');
      $('.box-pulldown').hide();
      $('.box-pulldown.' + gnav.pulldown).fadeIn('fast');
      $('#section-pulldown').fadeIn('fast');
      gnav.flag_open = true;
    },
	function(){
      $.noop(); // 空の関数オブジェクト
    });
  };
  /******************************************
   *  プルダウンのクローズ処理
   */
  gnav.hover_close_pulldown = function(){
    $('#header .logo,#section-head, #section-main').hover(function(){
      if(gnav.flag_open){
        $('#section-pulldown').fadeOut('fast');
        $('.gnav ul li').removeClass('active');
      }
    });
  };
  /******************************************
   *  プルダウンの継続切り替え時の処理
   */
  gnav.hover_change_pulldown = function(){
    $('.gnav ul li').hover(function(){
      if(gnav.flag_open && !$(this).hasClass(gnav.pulldown)){
      $('.box-pulldown').not('.' + gnav.pulldown).hide();
      }
    });
  };
  gnav.initialize();
  //========================================================
  // scroll
  //========================================================
  var scroll = {};
  /******************************************
   * scrollオブジェクトの初期化
   */
  scroll.initialize = function() {
    scroll.click();
  };
  scroll.click = function(){
   $('a[href^=#]').click(function() {
      var speed = 200; // ミリ秒
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
};
  scroll.initialize(); 
});