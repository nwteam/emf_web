<?php
require_once 'util/include.php';
//$_SESSION['read_page_count'] = 0; //control.phpで参照＆更新する
//$_SESSION['native_ad_no'] = null; //ad_data/native_x_2/index.phpで参照＆更新する
//
//$startMTime = microtime(true);
//$traceMessage = "-- Index Start --\r\n";
//$sqlTraceMessage = "\r\n";

include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック
$nav_no="";
if (isset($_GET['cat_id'])) {
    $cat_id = $_GET['cat_id'];
}
if (isset($_GET['cat_name'])) {
    $cat_name = $_GET['cat_name'];
}
if (isset($_GET['b_auth'])) {
    $b_auth = $_GET['b_auth'];
}
if (isset($_GET['searchBox'])) {
    $searchBox = $_GET['searchBox'];
}

if ($action == '') {
    exit();
}else if ($action == 'list') {
    /**
     * マンガ一覧
     */
    $searchAction = "list";
    $searchBox = $_GET['searchBox'];
    //$sql = sprintf("SELECT * FROM mz_book WHERE 1");
    $sql = "SELECT DISTINCT mz_book.*";
    $sql .= " FROM mz_book FORCE INDEX(unique_book_id)";
    $sql .= " INNER JOIN mz_chapter ON mz_book.book_id = mz_chapter.book_id";
    $sql .= " WHERE mz_chapter.insert_time <= '" . time() . "'";
    if ($searchBox != '') {
        $sql .= " AND (mz_book.book_name collate utf8_unicode_ci LIKE '%" . ($searchBox) . "%'";
        $sql .= " OR mz_book.book_auth collate utf8_unicode_ci LIKE '%" . ($searchBox) . "%')";
    }
//    $sql .= " ORDER BY book_id ";
    $sql .= " ORDER BY mz_book.insert_time DESC";
}else if ($action == 'category') {
    /**
     * カテゴリ検索
     */
    $searchAction = "category";
    $cat_id = $_GET['cat_id'];
    $cat_name = $_GET['cat_name'];
    $b_auth = $_GET['b_auth'];
    $searchBox = $_GET['searchBox'];

    //$sql = sprintf("SELECT * FROM mz_book WHERE 1");
    $sql = "SELECT DISTINCT mz_book.*";
    $sql .= " FROM mz_book FORCE INDEX(unique_book_id)";
    $sql .= " INNER JOIN mz_chapter ON mz_book.book_id = mz_chapter.book_id";
    $sql .= " WHERE mz_chapter.insert_time <= '" . time() . "'";

    if ($cat_id != '') {
//        if ($cat_id == 'CT999') {//男性向けか？
//            $sql .= " AND (find_in_set('CT002',mz_book.cat_id) OR find_in_set('CT003',mz_book.cat_id) or find_in_set('CT004',mz_book.cat_id) OR find_in_set('CT005',mz_book.cat_id) OR find_in_set('CT006',cat_id) OR find_in_set('CT007',cat_id) OR find_in_set('CT014',cat_id) OR find_in_set('CT015',cat_id) OR find_in_set('CT016',cat_id) OR find_in_set('CT018',cat_id) OR find_in_set('CT019',cat_id) OR find_in_set('CT021',cat_id) OR find_in_set('CT025',cat_id) OR find_in_set('CT026',cat_id))";
//        } elseif ($cat_id == 'CT998') {//女性向けか？
//            $sql .= " AND (find_in_set('CT001',mz_book.cat_id) OR find_in_set('CT047',mz_book.cat_id) or find_in_set('CT002',mz_book.cat_id) OR find_in_set('CT031',mz_book.cat_id))";
//        } else {//個別カテゴリ指定か？
            $sql .= " AND find_in_set('$cat_id',mz_book.cat_id)";
//        }
    }
    if ($searchBox != '') {
        $sql .= " AND (mz_book.book_name collate utf8_unicode_ci LIKE '%" . ($searchBox) . "%'";
        $sql .= " OR mz_book.book_auth collate utf8_unicode_ci LIKE '%" . ($searchBox) . "%')";
    }
    $sql .= " ORDER BY mz_book.insert_time DESC";
//    $sql .= " LIMIT 10;";
}
$r_books = mysqli_query($link, $sql);
$newChapters = array();
while ($row = mysqli_fetch_array($r_books)) {
    $newChapters[] = $row;
}
//カテゴリプルダウンリスト取得
include("common/common_category_list.php");
//読み回数計数
include("common/common_book_read_cnt.php");
?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top">
<header id="header">
    <?php include("common/common_header.php") ?>
    <?php include("common/common_nav.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
    <div class="inner clearfix">
        <div class="contents">
            <h2><span class="h_new">検索結果一覧</span></h2>
            <input type="hidden" name="action_code" value="<?php echo $action ?>">
            <input type="hidden" name="cat_id" value="<?php echo $cat_id ?>">
            <input type="hidden" name="cat_name" value="<?php echo $cat_name ?>">
            <input type="hidden" name="b_auth" value="<?php echo $b_auth ?>">
            <input type="hidden" name="searchBox" value="<?php echo $searchBox ?>">
            <ul class="new-list clearfix">
                <?php foreach ($newChapters as $chapter) { ?>
                <?php $newPageUrl = "index.php?action=readCnt&b_id=" . $chapter['book_id'] . "&b_name=" . urldecode($chapter['book_name']) . "&b_auth=" . urlencode($chapter['book_auth']); ?>
                <?php $newImgUrl = COMIC_PATH . $chapter['cover_img_path']; ?>
                <?php $newBookName = $chapter['book_name'] ?>
                <?php $newBookAuth = $chapter['book_auth'] ?>
                <?php if(mb_strlen($newBookName)>18){$newBookName = mb_substr($newBookName,0,18)."...";} ?>
                <?php if(mb_strlen($newBookAuth)>9){$newBookAuth = mb_substr($newBookAuth,0,9)."...";} ?>
                <li> <a href="<?php echo $newPageUrl ?>"> <span class="image" style="background: url(<?php echo $newImgUrl ?>) 50% 50% no-repeat #fff;  background-size:180px auto;"></span> <span class="title"><?php echo $newBookName ?><span class="name"><?php echo $newBookAuth ?></span></span> </a>
                    <ul class='category clearfix'>
                        <?php
                        /* ジャンル */
                        $cateIds = explode(",", $chapter['cat_id']);
                        if (!empty($cateIds)) {
                            foreach ($cateIds as $cateId) {
                                ?>
                                <li><a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                    <?php } ?>
            </ul>

        </div>
    <!-- /.contents-->
    <?php include("common/common_side.php") ?>
    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
</body>
<?php include_once("analyticstracking.php") ?>
</html>