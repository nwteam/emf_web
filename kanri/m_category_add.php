<?php
	session_start();
	//include
	require '../util/include.php';
	$sub_title='漫画管理　- ジャンル登録 -';
	$action = $_GET['action'];
	$sysdate=date('Y-m-d',time());
	$systime=date('Y-m-d H:i:s',time());
	$ip=get_real_ip();

//	//プルダウンリスト取得
//	$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
//	if(!$db){
//		die("connot connect:" . mysqli_error());
//	}
//	$dns = mysqli_select_db($db,DB_NAME);
//	if(!$dns){
//		die("connot use db:" . mysqli_error());
//	}
//	mysqli_set_charset($db,'utf8');
//	$sqlall = "select * from mz_category WHERE 1 and del_flg=0";
//	$result_list_cat = mysqli_query($db,$sqlall);
//	mysqli_close($db);

	//insert
if ($action=='insert'){
	//ジャンル名
    $i_cat_name=$_POST['i_cat_name'];

    $db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("connot connect:" . mysqli_error());
    }
    $dns = mysqli_select_db($db,DB_NAME);
    if(!$dns){
        die("connot use db:" . mysqli_error());
    }
    mysqli_set_charset($db,'utf8');
	
	$sql = "UPDATE mz_category_id SET id = LAST_INSERT_ID(id+1)";
	$result_id = mysqli_query($db,$sql);
	
	$sql = "select last_insert_id() as id";
	$result_b_id = mysqli_query($db,$sql);
	$row_b_id=mysqli_fetch_assoc($result_b_id);
	$i_category_id = "CT" . str_pad( $row_b_id['id'], 3,"0",STR_PAD_LEFT);
	
    $sql = sprintf("insert into mz_category (cat_id,cat_name) values ('%s','%s')",$i_category_id,$i_cat_name);
    $logstr = "$systime $ip INFO：ジャンル情報登録 INSERT SQL文： ".$sql."\r\n";
    //error_log($logstr,3,'../log/gen.log');

    $result = mysqli_query($db,$sql);
    if(!$result){
        mysqli_close($db);
        $logstr = "$systime ERR：ジャンル情報DB登録異常！ \r\n";
        $logstr .= "$systime $ip INFO：▲ジャンル情報登録異常終了 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
	$logstr = "$systime $ip INFO：▲ジャンル情報登録正常終了！！ \r\n";
	//error_log($logstr,3,'../log/gen.log');

	mysqli_close($db);
    $url= URL_PATH . "m_category.php?action=search";
    redirect($url);
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,must-revalidate">
<meta http-equiv="expires" content="Wed, 23 Aug 2006 12:40:27 UTC" />
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form enctype='multipart/form-data' method='post' name='upform'>
<div class='input-area'>
    <label class='w150'>ジャンル名</label>
    <input type='text' class='w500' name='i_cat_name' id='i_cat_name' value='<?php echo $i_cat_name;?>'/>
    <div style='clear:both;'></div>
    <input type='button' class='buttonS bGreen ml190 w200 mt40' value='登録' onclick='moveConfirm();'/>
    <input type="hidden" name='h_category_id' value="<?php echo $i_category_id;?>"/>
</div>
<script type="text/javascript" language="javascript">
	function moveConfirm() {
		//漫画名称
		 if(document.upform.i_cat_name.value == ""){
		  alert("ジャンル名称を入力してください。");
		  document.upform.i_cat_name.focus();
		  return false;
		 }
		//submit
		document.upform.action="?action=insert";
		document.upform.submit();
	}	
</script>
</form>
</div>
</body>
</html>