<?php
	//include
	require '../util/include.php';
	$sub_title='漫画管理　- 漫画一覧 -';
	$systime=date('Y-m-d H:i:s',time());

    $role=$_SESSION['role'];
    $login_user=$_SESSION['login_user'];

	$action = $_GET['action'];

    $db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("Cannot connect:" . mysqli_error($db));
    }
    $dns = mysqli_select_db($db,DB_NAME);
    if(!$dns){
        die("Cannot use db:" . mysqli_error($db));
    }
    mysqli_set_charset($db,'utf8');

	//トップ新着取得
	$sql_top_book = "select * from mz_top_book";
	$top_book_rs = mysqli_query($db,$sql_top_book);
	$top_book=mysqli_fetch_object($top_book_rs);
	$top_book_id=$top_book->book_id;

    //カテゴリプルダウンリスト取得
    $sql = "select * from mz_category WHERE 1 and del_flg=0";
    $result_list_cate = mysqli_query($db,$sql);
	//Delete
	if ($action=='delete'){
		$u_id = $_GET['u_id'];

		$sql = sprintf("delete from mz_book WHERE book_id = '%s'",$u_id);
		$result = mysqli_query($db,$sql);
		$sql = sprintf("delete from mz_chapter WHERE book_id = '%s'",$u_id);
		$result = mysqli_query($db,$sql);
	}
	//Update
	if ($action=='update'){
		$u_id = $_GET['u_id'];
		$b_id = $_GET['b_id'];
		$u_name = $_GET['u_name'];
		$auth = $_GET['auth'];
		$path = $_GET['path'];
		$sort = $_GET['sort'];
		$cat = $_GET['cat'];
		$cover_img_path="comic/".$path."/title/cover.jpg";

		$sql = sprintf("UPDATE mz_book SET cover_img_path='%s',book_name ='%s',book_auth='%s',folder_name='%s',sort_num=%d,cat_id='%s',update_time=%d WHERE id = %d",
						$cover_img_path,$u_name,$auth,$path,$sort,$cat,strtotime($systime),$u_id);

		$result = mysqli_query($db,$sql);
		
		$sql = sprintf("UPDATE mz_chapter SET page_folder_name='%s',update_time=%d WHERE book_id = '%s'",
						$path,strtotime($systime),$b_id);
		$result = mysqli_query($db,$sql);
	}
	//Active
	if ($action=='active'){
		$u_id = $_GET['u_id'];
		$sql = sprintf("UPDATE mz_book SET recommend_flg =1,update_time=%d WHERE id = %d",strtotime($systime),$u_id);
		$result = mysqli_query($db,$sql);
	}
	//unActive
	if ($action=='unactive'){
		$u_id = $_GET['u_id'];
		$sql = sprintf("UPDATE mz_book SET recommend_flg =0,update_time=%d WHERE id = %d",strtotime($systime),$u_id);
		$result = mysqli_query($db,$sql);
	}
	//Active
	if ($action=='top_on'){
		$u_id = $_GET['u_id'];
		$sql = sprintf("delete from mz_top_book WHERE book_id = '%s'",$top_book_id);
		$result = mysqli_query($db,$sql);
		$sql = sprintf("insert into mz_top_book (book_id) VALUES ('%s')",$u_id);
		$result = mysqli_query($db,$sql);
	}
	//unActive
	if ($action=='top_off'){
		$u_id = $_GET['u_id'];
		$sql = sprintf("delete from mz_top_book WHERE book_id = '%s'",$u_id);
		$result = mysqli_query($db,$sql);
	}
	//トップ新着取得
	$sql_top_book = "select * from mz_top_book";
	$top_book_rs = mysqli_query($db,$sql_top_book);
	$top_book=mysqli_fetch_object($top_book_rs);
	$top_book_id=$top_book->book_id;
	//Search
	if ($action=='search'||$action=='update'||$action=='delete'||$action=='active'||$action=='unactive'||$action=='top_on'||$action=='top_off'){

		$page_size=100;
		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;
		//Form
		$s_book_name=$_POST['s_book_name'];
		if($s_book_name==''&&$_GET['b_name']!=''){
			$s_book_name=$_GET['b_name'];
		}
		$s_category_id=$_POST['s_category_id'];
		if($s_category_id==''&&$_GET['c_id']!=''){
			$s_category_id=$_GET['c_id'];
		}
		$sort_key=$_POST['sort_key'];
		if($sort_key==''&&$_GET['sort_key']!=''){
			$sort_key=$_GET['sort_key'];
		}
		if($sort_key==''){
			$sort_key='book_id';
		}
		$sort_type=$_POST['sort_type'];
		if($sort_type==''&&$_GET['sort_type']!=''){
			$sort_type=$_GET['sort_type'];
		}
		if($sort_type==''){
			$sort_type='desc';
		}
		$good_flg=$_POST['good_flg'];
		if($good_flg==''&&$_GET['good_flg']!=''){
			$good_flg=$_GET['good_flg'];
		}
		if($good_flg==''){
			$good_flg='all';
		}
		//SQL
		$sqlall = "select * from mz_book WHERE 1";

        if($s_book_name!='') {
            $sqlall .= " and book_name like '%".mysqli_real_escape_string($db,$s_book_name)."%'";
        }
		if($s_category_id!='') {
			$sqlall .= sprintf(" and find_in_set('%s',cat_id)",$s_category_id);
		}
		if($good_flg=='nomal') {
			$sqlall .= " and recommend_flg = 0 ";
		}
		if($good_flg=='good') {
			$sqlall .= " and recommend_flg = 1 ";
		}
		$result = mysqli_query($db,$sqlall) or die(mysqli_error($db));
		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}
		$rowCntall=mysqli_num_rows($result);
		$sql = sprintf("%s order by %s %s limit %d,%d",$sqlall,$sort_key,$sort_type,($page-1)*$page_size,$page_size);
		$result = mysqli_query($db,$sql);
		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}
		$rowCnt=mysqli_num_rows($result);
		//paging
		if($rowCnt==0){
			$page_count = 0;
			mysqli_close($db);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1&b_name='.$s_book_name.'&c_id='.$s_category_id.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&good_flg='.$good_flg.'>トップページ</a>|<a href=?action=search&page='.($page-1).'&b_name='.$s_book_name.'&c_id='.$s_category_id.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&good_flg='.$good_flg.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'&b_name='.$s_book_name.'&c_id='.$s_category_id.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&good_flg='.$good_flg.'>次頁</a>|<a href=?action=search&page='.$page_count.'&b_name='.$s_book_name.'&c_id='.$s_category_id.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&good_flg='.$good_flg.'>最終ページ</a>';
		}
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
<link rel="stylesheet" type="text/css" href="../js/multiselectSrc/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../js/assets/style.css" />
<link rel="stylesheet" type="text/css" href="../js/assets/prettify.css" />
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>

<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/assets/prettify.js"></script>
<script type="text/javascript" src="../js/multiselectSrc/jquery.multiselect.js"></script>
<script type="text/javascript">
$(function(){
	for(i=0;i<101;i++){
		$("#i_category_id_"+i).multiselect({
			noneSelectedText: "",
			checkAllText: "全て選択",
			uncheckAllText: '全て選択解除',
			selectedList:3,
			selectedText: '# 個以上',
			height:200,
			minWidth:174
		});
	}
});
</script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
    <label class='search_label w100'>漫画名</label>
		<input type='text' name='s_book_name' id='s_book_name' class='w500' value='<?php echo $s_book_name;?>'/>
    <div style='clear:both;'></div>
	<label class='search_label w100'>ソート順</label>
		<input type="radio" name="sort_key" value="book_id" style='font-size:16px;' <?php if ($sort_key=='book_id') {echo 'checked=checked';}?>/>ブックID
		<input type="radio" name="sort_key" value="book_name" style='font-size:16px;' <?php if ($sort_key=='book_name') {echo 'checked=checked';}?>/>漫画名
		<input type="radio" name="sort_key" value="book_auth" style='font-size:16px;' <?php if ($sort_key=='book_auth') {echo 'checked=checked';}?>/>作者名
		<input type="radio" name="sort_key" value="folder_name" style='font-size:16px;' <?php if ($sort_key=='folder_name') {echo 'checked=checked';}?>/>パス
		<input type="radio" name="sort_key" value="sort_num" style='font-size:16px;' <?php if ($sort_key=='sort_num') {echo 'checked=checked';}?>/>並び順
    <div style='clear:both;'></div>
	<label class='search_label w100'>ソートタイプ</label>
		<input type="radio" name="sort_type" value="desc" style='font-size:16px;' <?php if ($sort_type=='desc') {echo 'checked=checked';}?>/>降順
		<input type="radio" name="sort_type" value="asc" style='font-size:16px;' <?php if ($sort_type=='asc') {echo 'checked=checked';}?>/>昇順
    <div style='clear:both;'></div>
	<label class='search_label w100'>お勧め状況</label>
	<input type="radio" name="good_flg" value="all" style='font-size:16px;' <?php if ($good_flg=='all') {echo 'checked=checked';}?>/>全て
	<input type="radio" name="good_flg" value="nomal" style='font-size:16px;' <?php if ($good_flg=='nomal') {echo 'checked=checked';}?>/>通常のみ
	<input type="radio" name="good_flg" value="good" style='font-size:16px;' <?php if ($good_flg=='good') {echo 'checked=checked';}?>/>お勧めのみ
	<div style='clear:both;'></div>
    <label class='search_label w100'>カテゴリ</label>
		<select name='s_category_id' id='s_category_id' class='re_select w200 fl'>
			<option value=''></option>
			<?php
			while($arr_list_row=mysqli_fetch_array($result_list_cate)){
				$arr_category_row[]=$arr_list_row;
				if($arr_list_row['cat_id']==$s_category_id){
					echo"<option value=".$arr_list_row['cat_id']." selected >".$arr_list_row['cat_name']. "</option>";
				}else{
					echo"<option value=".$arr_list_row['cat_id']." >".$arr_list_row['cat_name']. "</option>";
				}
			}
			?>
		</select>
    <input type='submit' class='buttonS bGreen ml100' value='絞り込み'/>
    <input type='button' class='buttonS bGreen ml20' value='新規登録' onclick='addInfo();'/>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='100px'>ブックID</th>
              <th width='240px'>漫画名</th>
              <th width='100px'>作者名</th>
              <th width='100px'>パス</th>
              <th width='50px'>並び順</th>
              <th width='50px'>総話数</th>
              <th width='50px'>状況</th>
              <th width='124px'>お勧めする</th>
              <th width='174px'>カテゴリ</th>
              <th width='60px'>操作</th>
              <th width='60px'>削除</th>
              <th width='60px'>話編集</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysqli_fetch_object($result))
	{
	  $folder_name=$rs->folder_name;
	  $folder_path='comic/'.$folder_name;
	  $chap_cnt=sizeof(myScandir($folder_path, false));
      
	  if($rs->total_chap!=$chap_cnt){
	  	$bk_red= 'background-color:red;';
	  }else{
	  	$bk_red= '';
	  }
	  $r_chap=mysqli_query($link,"select count(*) cnt from mz_chapter where book_id='".$rs->book_id."'");
	  $row=mysqli_fetch_array($r_chap);
	  $db_chapter_cnt=$row['cnt'];
	  echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<input type='hidden' name='u_id_".$i."' id='u_id_".$i."' value=".$rs->id.">
					<td width='100px'align='center'>".$rs->book_id."</td>
					<td width='240px'><input type='text' style='font-size:16px;width:97%;' name='i_book_name_".$i."' id='i_book_name_".$i."'value=".htmlspecialchars(str_replace(' ','&nbsp;',$rs->book_name))."></td>
					<td width='100px'align='center'><input type='text' style='font-size:16px;width:90%;' name='i_book_auth_".$i."' id='i_book_auth_".$i."' value=".htmlspecialchars($rs->book_auth)."></td>
					<td width='100px'align='center'><input type='text' style='font-size:16px;width:90%;' name='i_folder_name_".$i."' id='i_folder_name_".$i."' value=".$rs->folder_name."></td>
					<td width='50px'align='center'><input type='text' style='font-size:16px;width:90%;' name='i_sort_num_".$i."' id='i_sort_num_".$i."' value=".$rs->sort_num."></td>
					<td width='50px'align='center'>".$rs->total_chap."話</td>
			";
			if($rs->recommend_flg=='0'){
				echo"
					<td width='50px'align='center'>通常</td>
					<td width='124px'align='center'>
						<input type='button' class='buttonS bGreen' value='お勧めする' onclick='changeToActive(".$rs->id.");'/>
					</td>
					";
			}
			elseif($rs->recommend_flg=='1'){
				echo"
					<td width='50px'align='center'>お勧め</td>
					<td width='124px'align='center'>
						<input type='button' class='buttonS bRed' value='通常にする' onclick='changeToUnActive(".$rs->id.");'/>
					</td>
					";
			}
			else{
				echo"
					<td width='50px'align='center'></td>
					<td width='124px'align='center'></td>
					";
			}
	  echo "
					<td width='174px'align='center'>
						<select name='i_category_id_".($i-1)."' id='i_category_id_".($i-1)."' multiple='multiple' size='5'>
			";
							$j=0;

							$i_category_id=$rs->cat_id;
							$arr_category_id=explode(",",$i_category_id);
							foreach($arr_category_row as $c_id=>$c_name){
								if($c_name[cat_id]==$arr_category_id[$j]){
									echo"<option value=".$c_name[cat_id]." selected >".$c_name[cat_name]. "</option>";
									$j++;
								}else{
									echo"<option value=".$c_name[cat_id]." >".$c_name[cat_name]. "</option>";
								}
							}

	  	echo "
						</select>
	  				</td>
	  	";
		echo"
						<td width='60px'align='center'><input type='button' class='buttonS bGreen' value='更新' onclick=\"updateChange('".$i."','".$rs->book_id."')\"/></td>
						<td width='60px'align='center'><a href='#' onclick=\"var ret=confirm('漫画情報を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->book_id."');\">削除</a></td>
		";
		if($db_chapter_cnt!='0'){
			echo"<td width='60px'align='center'><input type='button' class='buttonS bGreen' value='編集' onclick=\"editInfo('".$rs->book_id."')\"/></td>";
		}else{
			echo"<td width='60px'align='center'><input type='button' class='buttonS bRed' value='登録' onclick=\"editInfo('".$rs->book_id."')\"/></td>";
		}    
        echo"
                    </tr>
                </table>
		    ";
        
		$i++;
	}
	echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysqli_close($db);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
mysqli_close($db);
?>
</form>
<script language="javascript" type="text/javascript">
	function addInfo() {
		var pageurl="m_comic_add.php";
		window.location.href=pageurl;
	}
	function editInfo(u_id) {
        var pageurl="m_comic_chap_edit.php?action=edit&b_id="+u_id;
        window.location.href=pageurl;
    }
    function deleteInfo(u_id) {
        var pageurl="?action=delete&u_id="+u_id;
        window.location.href=pageurl;
    }
	function updateChange(row,b_id) {
		var u_id_item='u_id_'+row;
		var b_name_item='i_book_name_'+row;
        var auth_item='i_book_auth_'+row;
        var path_item='i_folder_name_'+row;
        var sort_item='i_sort_num_'+row;
        var cat_item ="#i_category_id_"+(row-1);
        
		var u_id=document.getElementById(u_id_item).value;
		var b_name=document.getElementById(b_name_item).value;
        var auth=document.getElementById(auth_item).value;
        var path=document.getElementById(path_item).value;
        var sort=document.getElementById(sort_item).value;
        var cat= $(cat_item).multiselect("getChecked").map(function(){return this.value;}).get();

		var pageurl="?action=update&u_id="+u_id+"&u_name="+b_name+"&auth="+auth+"&path="+path+"&sort="+sort+"&cat="+cat+"&b_id="+b_id;
		window.location.href=pageurl;
	}
	function changeToActive(u_id) {
		var pageurl="?action=active&u_id="+u_id;
		window.location.href=pageurl;
	}
	function changeToUnActive(u_id) {
		var pageurl="?action=unactive&u_id="+u_id;
		window.location.href=pageurl;
	}
	function top_on(u_id) {
		var pageurl="?action=top_on&u_id="+u_id;
		window.location.href=pageurl;
	}
	function top_off(u_id) {
		var pageurl="?action=top_off&u_id="+u_id;
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>