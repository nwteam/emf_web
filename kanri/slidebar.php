<?php
session_start();
//include
require '../util/include.php';

if (empty($_SESSION['login_user'])) {
    header('Location: index.php');
} else {
    $login_user = $_SESSION['login_user'];
    $role = $_SESSION['role'];
}
?>
<!DOCTYPE HTML>
<html xmlns=http://www.w3.org/1999/xhtml>
    <head>
        <meta http-equiv='content-type' content='text/html; charset=utf-8' />
        <meta http-equiv='content-style-type' content='text/css' />
        <meta http-equiv='content-script-type' content='text/javascript' />
        <link href='../css/common.css' type='text/css' rel='stylesheet' />
    </head>
    <body>
        <div class='slidebar'>
            <ul>
                <?php if($role==1){?>
                    <li><a href='m_comic.php?action=search' target ='maincontent'>漫画管理</a></li>
                <!--<li><a href='m_book_read_cnt.php?action=search' target ='maincontent'>読み回数(ブック)</a></li>-->
                <!--<li><a href='m_chapter_read_cnt.php' target ='maincontent'>読み回数(巻)</a></li>-->
                    <li><a href='m_category.php?action=search' target ='maincontent'>ジャンル管理</a></li>
                <?php }?>
                <!--<li><a href='m_ad_flg.php?action=search' target ='maincontent'>広告管理</a></li>-->
                <!--<li><a href='m_ad_click.php' target ='maincontent'>広告Click回数DL</a></li>-->
                <li><a href='m_shop.php?action=search' target ='maincontent'>店舗情報管理</a></li>
                <?php if($role==1||$role==3){?>
                    <li><a href='m_show_log.php' target ='maincontent'>全体視聴管理</a></li>
                <?php }?>
				<li><a href='m_shop_show_log.php' target ='maincontent'>店舗別視聴管理</a></li>
                <?php if($role==1){?>
                    <li><a href='m_top_new.php?action=search' target ='maincontent'>トップ新着管理</a></li>
                <?php }?>
            </ul>
        </div>
    </body>
</html>