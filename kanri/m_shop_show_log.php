<?php
require '../util/include.php';
$sub_title='店舗別視聴管理　-店舗別視聴データ情報一覧 -';
$systime=date('Y-m-d H:i:s',time());

$role=$_SESSION['role'];
$login_user=$_SESSION['login_user'];

$action = $_GET['action'];
$u_id=$_GET['u_id'];

$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
if(!$db){die("Cannot connect:" . mysqli_error($db));}
$dns = mysqli_select_db($db,DB_NAME);
if(!$dns){die("Cannot use db:" . mysqli_error($db));}
mysqli_set_charset($db,'utf8');

if($role!=1&&$role!=3){
	$sql_shop_info = sprintf("select * from mz_shop WHERE mg_no = '%s'",$login_user);
	$result_shop_info = mysqli_query($db,$sql_shop_info);
	$shop_info=mysqli_fetch_object($result_shop_info);
	$mg_no=$shop_info->mg_no;
	$shop_name=$shop_info->shop_name;
	$shop_name_b=$shop_info->shop_name_b;
}
if($u_id!=""){
	$sql_shop_info = sprintf("select * from mz_shop WHERE shop_id = %d",$u_id);
	$result_shop_info = mysqli_query($db,$sql_shop_info);
	$shop_info=mysqli_fetch_object($result_shop_info);
	$mg_no=$shop_info->mg_no;
	$shop_name=$shop_info->shop_name;
	$shop_name_b=$shop_info->shop_name_b;
}
//店舗リスト取得
$sql = "select * from mz_shop WHERE 1 and del_flg=0";
$shop_list = mysqli_query($db,$sql);

//Search
if ($action=='search_all'||$action=='search_date'||$action=='search_shop_ip'||$action=='search_all_book'||$action=='search_date_book'){
	$u_id=$_POST['u_id'];
	if($u_id==''&&$_GET['u_id']!=''){$u_id=$_GET['u_id'];}
	if($mg_no==''&&$_POST['mg_no']!=''){$mg_no=$_POST['mg_no'];}
	if($shop_name==''&&$_POST['shop_name']!=''){$shop_name=$_POST['shop_name'];}
	if($shop_name_b==''&&$_POST['shop_name_b']!=''){$shop_name_b=$_POST['shop_name_b'];}
	$page_size=100;
	if( isset($_GET['page']) ){
		$page = intval( $_GET['page'] );
	}
	else{
		$page = 1;
	}
	$rowCnt = 0;
	//Form
	//店舗名
	$s_shop_name=$_POST['s_shop_name'];
	if($s_shop_name==''&&$_GET['s_shop_name']!=''){$s_shop_name=$_GET['s_shop_name'];}
	//店舗名
	$s_shop_name_b=$_POST['s_shop_name_b'];
	if($s_shop_name_b==''&&$_GET['s_shop_name_b']!=''){$s_shop_name_b=$_GET['s_shop_name_b'];}
	//管理番号
	$s_mg_no=$_POST['s_mg_no'];
	if($s_mg_no==''&&$_GET['s_mg_no']!=''){$s_mg_no=$_GET['s_mg_no'];}
	//店舗選択
	$s_shop_id=$_POST['s_shop_id'];
	if($s_shop_id==''&&$_GET['s_shop_id']!=''){$s_shop_id=$_GET['s_shop_id'];}
	//IPアドレス
	$s_shop_ip=$_POST['s_shop_ip'];
	if($s_shop_ip==''&&$_GET['s_shop_ip']!=''){$s_shop_ip=$_GET['s_shop_ip'];}
	//期間From
	$s_date_from=$_POST['s_date_from'];
	if($s_date_from==''&&$_GET['s_date_from']!=''){$s_date_from=$_GET['s_date_from'];}
	//期間To
	$s_date_to=$_POST['s_date_to'];
	if($s_date_to==''&&$_GET['s_date_to']!=''){$s_date_to=$_GET['s_date_to'];}
	//SQL
//	if($s_shop_ip!='') {
//		//$sql_all = "SELECT a.*,sum(a.read_cnt) read_cnt,c.shop_name FROM mz_book_read_cnt a left join mz_shop_ip b on a.shop_id=b.shop_id left join mz_shop c on c.shop_id=a.shop_id WHERE FIND_IN_SET('".mysqli_real_escape_string($db,trim($s_shop_ip))."',b.shop_ip) ";
//		$sql_all = "SELECT a.*,sum(a.read_cnt) read_cnt,c.shop_name,c.mg_no FROM mz_book_read_cnt a left join mz_shop c on c.shop_id=a.shop_id WHERE 1";
//	}else{
//		$sql_all = "SELECT a.*,sum(a.read_cnt) read_cnt,c.shop_name,c.mg_no FROM mz_book_read_cnt a left join mz_shop c on c.shop_id=a.shop_id WHERE 1";
//	}
	$sql_all = "SELECT a.*,sum(a.read_cnt) read_cnt_sum,c.shop_name,c.shop_name_b,c.mg_no FROM mz_book_read_cnt a
				left join mz_shop c on c.shop_id=a.shop_id
				left join mz_shop_ip d on d.shop_id=a.shop_id
				WHERE 1";
	if($s_mg_no!='') {
		$sql_all .= " and c.mg_no  = '".mysqli_real_escape_string($db,$s_mg_no)."'";
	}
	if($s_shop_ip!='') {
		$sql_all .= " and d.shop_ip = '".mysqli_real_escape_string($db,$s_shop_ip)."'";
	}
	if($s_shop_name!='') {
		$sql_all .= " and c.shop_name like '%".mysqli_real_escape_string($db,$s_shop_name)."%'";
	}
	if($s_shop_name_b!='') {
		$sql_all .= " and c.shop_name_b like '%".mysqli_real_escape_string($db,$s_shop_name_b)."%'";
	}
	if($s_shop_id!='') {
		$sql_all .= " and a.shop_id =".$s_shop_id;
	}
	if($u_id!='') {
		$sql_all .= " and a.shop_id =".$u_id;
	}
	if($s_date_from!='') {
		$s_date_from=$s_date_from." 00:00:00";
		$sql_all .= " and unix_timestamp(concat(a.year,'-',a.month,'-',a.day)) >= ".strtotime($s_date_from);
	}
	if($s_date_to!='') {
		$s_date_to=$s_date_to." 23:59:59";
		$sql_all .= " and unix_timestamp(concat(a.year,'-',a.month,'-',a.day)) <= ".strtotime($s_date_to);
	}
	$s_date_from=str_replace(" 00:00:00","",$s_date_from);
	$s_date_to=str_replace(" 23:59:59","",$s_date_to);
	if($action=='search_date'){
		$sql_all .= " GROUP BY a.year,a.month,a.day";
	}
	if($action=='search_all_book'){
		$sql_all .= " GROUP BY a.book_id";
	}
	if($action=='search_date_book'){
		$sql_all .= " GROUP BY a.book_id,a.year,a.month,a.day";
	}
	if($action=='search_shop_ip'){
		$sql_all .= " GROUP BY a.shop_id,a.shop_ip";
	}
	$result = mysqli_query($db,$sql_all) or die(mysqli_error($db));
	if(!$result){
		$rowCnt = -1;
	}
	$total=mysqli_num_rows($result);
	if($action=='search_date_book'){
		$sql = sprintf("%s order by a.year desc,a.month desc,a.day desc,read_cnt_sum desc  limit %d,%d",$sql_all,($page-1)*$page_size,$page_size);
	}elseif($action=='search_shop_ip'){
		$sql = sprintf("%s order by c.shop_name,c.shop_name_b,read_cnt_sum desc limit %d,%d",$sql_all,($page-1)*$page_size,$page_size);
	}else{
		$sql = sprintf("%s order by read_cnt_sum desc limit %d,%d",$sql_all,($page-1)*$page_size,$page_size);
	}
	$sql_cnt=sprintf("select sum(s.read_cnt_sum) read_cnt_all from (%s) s",$sql_all);
	$result_cnt = mysqli_query($db,$sql_cnt);
	$rs_cnt=mysqli_fetch_object($result_cnt);
	$show_cnt_all=$rs_cnt->read_cnt_all;
	//echo $sql."<br>";
	$result = mysqli_query($db,$sql);
	if(!$result){
		$rowCnt = -1;
	}
	$rowCnt=mysqli_num_rows($result);

	//paging
	if($rowCnt==0){
		$page_count = 0;
	}
	else{
		if( $total<$page_size ){ $page_count = 1; }
		if( $total%$page_size ){
			$page_count = (int)($total / $page_size) + 1;
		}else{
			$page_count = $total / $page_size;
		}
	}
	//echo $page."<br>";
	//echo $page_count."<br>";
	$page_string = '';
	if (($page == 1)||($page_count == 1)){
		$page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
	}
	else{
		$page_string .= '<a href=?action='.$action.'&page=1&s_date_from='.$s_date_from.'&s_date_to='.$s_date_to.'>トップページ</a>|<a href=?action='.$action.'&page='.($page-1) .'&s_date_from='.$s_date_from.'&s_date_to='.$s_date_to.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
	}
	if( ($page == $page_count) || ($page_count == 0) ){
		$page_string .= '次頁|最終ページ';
	}
	else{
		$page_string .= '<a href=?action='.$action.'&page='.($page+1).'&s_date_from='.$s_date_from.'&s_date_to='.$s_date_to .'>次頁</a>|<a href=?action='.$action.'&page='.$page_count.'&s_date_from='.$s_date_from.'&s_date_to='.$s_date_to.'>最終ページ</a>';
	}
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
	<title><?php echo $sub_title; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" >
	<meta http-equiv="content-style-type" content="text/css">
	<meta http-equiv="content-script-type" content="text/javascript">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<link href="../css/common.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="../js/common.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/assets/style.css" />
	<script>
		$(function() {
			$('#s_date_from').datepicker({
				dateFormat: 'yy-mm-dd',
				date: $('#s_date_from').val(),
				current: $('#s_date_from').val(),
				starts: 1,
				position: 'right',
				onBeforeShow: function () {
					$('#s_date_from').DatePickerSetDate($('#s_date_from').val(), true);
				},
				onChange: function (formated, dates) {
					$('#s_date_from').val(formated);
					$('#s_date_from').DatePickerHide();
				}
			});
			$('#s_date_to').datepicker({
				dateFormat: 'yy-mm-dd',
				date: $('#s_date_to').val(),
				current: $('#s_date_to').val(),
				starts: 1,
				position: 'right',
				onBeforeShow: function () {
					$('#s_date_to').DatePickerSetDate($('#s_date_to').val(), true);
				},
				onChange: function (formated, dates) {
					$('#s_date_to').val(formated);
					$('#s_date_to').DatePickerHide();
				}
			});
		});
	</script>
</head>
<body>
<div class='main'>
	<div class='subtitle'><?php echo $sub_title; ?></div>
	<form method='post' name='form1'>
		<div class='input-area'>
			<input type='hidden' name='u_id' id='u_id' value='<?php echo $u_id;?>'/>
			<input type='hidden' name='mg_no' id='mg_no' value='<?php echo $mg_no;?>'/>
			<input type='hidden' name='shop_name' id='shop_name' value='<?php echo $shop_name;?>'/>
			<input type='hidden' name='shop_name_b' id='shop_name_b' value='<?php echo $shop_name_b;?>'/>
			<label class='search_label w100'>店舗名</label>
			<?php if(($role==1||$role==3)&&$u_id==""){?>
				<input type='text' name='s_shop_name' id='s_shop_name' class='w500' value='<?php echo $s_shop_name;?>'/>
			<?php }else{?>
				<label class='w500' style='margin-left: 0px;'><?php echo $shop_name;?></label>
				<input type='hidden' name='s_shop_name' id='s_shop_name' class='w500' value='<?php echo $shop_name;?>'/>
			<?php }?>
			<div style='clear:both;'></div>
			<label class='search_label w100'>支店名</label>
			<?php if(($role==1||$role==3)&&$u_id==""){?>
				<input type='text' name='s_shop_name_b' id='s_shop_name_b' class='w500' value='<?php echo $s_shop_name_b;?>'/>
			<?php }else{?>
				<label class='w500' style='margin-left: 0px;'><?php echo $s_shop_name_b;?></label>
				<input type='hidden' name='s_shop_name_b' id='s_shop_name_b' class='w500' value='<?php echo $s_shop_name_b;?>'/>
			<?php }?>
			<div style='clear:both;'></div>
			<label class='search_label w100'>管理番号</label>
			<?php if(($role==1||$role==3)&&$u_id==""){?>
			<input type='text' name='s_mg_no' id='s_mg_no' class='w200' value='<?php echo $s_mg_no;?>'/>
			<?php }else{?>
				<label class='w500' style='margin-left: 0px;'><?php echo $mg_no;?></label>
				<input type='hidden' name='s_mg_no' id='s_mg_no' class='w500' value='<?php echo $mg_no;?>'/>
			<?php }?>
			<label class='search_label w100'>IPアドレス</label>
			<input type='text' name='s_shop_ip' id='s_shop_ip' class='w200' value='<?php echo $s_shop_ip;?>'/>

			<div style='clear:both;'></div>
			<?php if(($role==1||$role==3)&&$u_id==""){?>
			<label class='search_label w100'>店舗選択</label>
			<select name='s_shop_id' id='s_shop_id' class='re_select w500 fl'>
				<option value=''></option>
				<?php
				while($arr_list_row=mysqli_fetch_array($shop_list)){
					$arr_category_row[]=$arr_list_row;
					if($arr_list_row['shop_id']==$s_shop_id){
						echo"<option value=".$arr_list_row['shop_id']." selected >".$arr_list_row['shop_name'].$arr_list_row['shop_name_b']. "</option>";
					}else{
						echo"<option value=".$arr_list_row['shop_id']." >".$arr_list_row['shop_name'].$arr_list_row['shop_name_b']. "</option>";
					}
				}
				?>
			</select>
			<?php }?>
			<div style='clear:both;'></div>
			<label class='search_label w100'>期間指定</label>
			<input type='text' name='s_date_from' id='s_date_from' class='w100' value='<?php echo $s_date_from;?>'/>
			<span class="ftsize18 ml20 mr20 fl mt10">～</span>
			<input type='text' name='s_date_to' id='s_date_to' class='w100' value='<?php echo $s_date_to;?>'/>
			<div style='clear:both;'></div>
			<input type='button' class='buttonS bGreen ml20 mt10' value='指定期間全体サマリ' onclick='search_all();'/>
			<input type='button' class='buttonS bGreen ml20 mt10' value='指定期間日別サマリ' onclick='search_date();'/>
			<input type='button' class='buttonS bGreen ml20 mt10' value='指定期間IP別サマリ' onclick='search_shop_ip();'/>
			<div style='clear:both;'></div>
			<input type='button' class='buttonS bGreen ml20 mt10' value='期間全体作品ごと　' onclick='search_all_book();'/>
			<input type='button' class='buttonS bGreen ml20 mt10' value='日別作品ごと　　　' onclick='search_date_book();'/>
			<?php if($action=='search_all'||$action=='search_date'||$action=='search_all_book'||$action=='search_date_book'||$action=='search_shop_ip'){?>
				<span class='ml40 w100'>視聴数合計：<?php echo $show_cnt_all;?></span>
				<input type='button' class='buttonS bBlue ml190 mt10' value='CSV出力' onclick="csv_output('<?php echo $action;?>')"/>
			<?php }?>
		</div>
		<?php
		if ($rowCnt>0){
			echo "
				<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
					<tr bgcolor='#DBE6F5'>
					  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
					</tr>
				</table>";
			echo "
				<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<tr bgcolor='#DBE6F5'>
					  <th width='100px'>サマリ分類</th>
					  <th width='100px'>期間</th>
					  <th width='100px'>管理番号</th>
					  <th width='100px'>IPアドレス</th>
					  <th width='240px'>店舗名</th>
					  <th width='240px'>支店名</th>
					  <th width='240px'>作品名</th>
					  <th width='100px'>視聴数</th>
					</tr>
				</table>
			";
			if($action=="search_all"){
				$cate_name="期間全体";
				$book_name="全体";
			}
			if($action=="search_date"){
				$cate_name="期間日別";
				$book_name="全体";
			}
			if($action=="search_all"||$action=="search_all_book"||$action=="search_shop_ip") {
				if ($s_date_from != "" && $s_date_to == "") {
					$s_date_str = $s_date_from . "～".'<br>';
				}
				if ($s_date_to != "" && $s_date_from == "") {
					$s_date_str = "～" . $s_date_to;
				}
				if ($s_date_from != "" && $s_date_to != "") {
					$s_date_str = $s_date_from . "～" .'<br>'. $s_date_to;
				}
			}
			$i=1;
			while($rs=mysqli_fetch_object($result))
			{
				if($action!="search_all"&&$action!="search_date"&&$s_shop_ip=="") {
					$value_ip = array();
					$ip_info = "";
					$sql_ip = "select shop_ip from mz_shop_ip WHERE shop_id=" . $rs->shop_id;
					$result_ip = mysqli_query($db, $sql_ip) or die(mysqli_error($db));
					if (!$result_ip) {
						$rowCnt = -1;
					}
					$j = 0;
					while ($rs_ip = mysqli_fetch_object($result_ip)) {
						$value_ip[$j] = $rs_ip->shop_ip;
						$j++;
					}
					$value_ip = implode(",", $value_ip);
					$ip_info = str_replace(",", "<br>", $value_ip);
				}
				if($action=="search_all_book"){
					$cate_name="期間作品毎";
					$book_name=$rs->book_name;
				}
				if($action=="search_date_book"){
					$cate_name="期間日別作品毎";
					$book_name=$rs->book_name;
				}
				if($action=="search_date"||$action=="search_date_book") {
					$s_date_str=$rs->year.'-'.$rs->month.'-'.$rs->day;
				}
				if($action=="search_all"||$action=="search_date"){
					$list_mg_no="全体";
					$show_shop_name="全体";
					$show_shop_name_b="全体";
				}else{
					if(($role==1||$role==3)&&$u_id==""){
						//$list_mg_no=$s_mg_no;
						$list_mg_no=$rs->mg_no;
					}else{
						//$list_mg_no=$mg_no;
						$list_mg_no=$rs->mg_no;
					}
					$show_shop_name=$rs->shop_name;
					$show_shop_name_b=$rs->shop_name_b;
				}
				if($rs->read_cnt_sum==0){
					$show_read_cnt=0;
				}else{
					$show_read_cnt=$rs->read_cnt_sum;
				}
				if($s_shop_ip==""){
					$show_shop_ip=$ip_info;
				}else{
					$show_shop_ip=$s_shop_ip;
				}
				if($action=="search_shop_ip"){
					$cate_name="期間IP別";
					$book_name="全体";
					$list_mg_no=$rs->mg_no;
					$show_shop_name=$rs->shop_name;
					$show_shop_name_b=$rs->shop_name_b;
					$show_shop_ip=$rs->shop_ip;
				}
				echo "
				<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<tr align='left' bgcolor='#EEF2F4'>
						<td width='100px'align='center'>".$cate_name."</td>
						<td width='100px'align='center'>".$s_date_str."</td>
						<td width='100px'align='center'>".$list_mg_no."</td>
						<td width='100px'align='center'>".$show_shop_ip."</td>
						<td width='240px'>".$show_shop_name."</td>
						<td width='240px'>".$show_shop_name_b."</td>
						<td width='240px'>".$book_name."</td>
						<td width='100px'align='center'>".$show_read_cnt."</td>
						</tr>
					</table>
				";
				$i++;
			}
			echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
			mysqli_close($db);
		}else{
			if ($action=='search'){
				echo "検索結果がありません。";
			}
		}
		?>
	</form>
	<script language="javascript" type="text/javascript">
		function search_all() {
			//var page_url="?action=search_all";
			//window.location.href=page_url;
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_all";
			document.form1.submit();
		}
		function search_date() {
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_date";
			document.form1.submit();
		}
		function search_shop_ip() {
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_shop_ip";
			document.form1.submit();
		}
		function search_all_book() {
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_all_book";
			document.form1.submit();
		}
		function search_date_book() {
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_date_book";
			document.form1.submit();
		}
		function csv_output(action) {
			document.form1.action="m_shop_show_csv_output.php?action="+action;
			document.form1.submit();
		}
	</script>
</div>
</body>
</html>