<?php
session_start();
if(!empty($_SESSION['login_user']))
{
header('Location: m_main.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<title>EMF　管理画面</title>
<link rel="icon" href="favicon.ico">
<link rel="shortcut icon" href="favicon.ico">
<link href="../css/index.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery.ui.shake.js"></script>
<script type="text/javascript" src="../js/login.js"></script>
</head>
<body>
<div id="main">
	<h1>EMF　管理画面(新サーバー)</h1>
	<div id="box">
		<form action="" method="post">
		<label>管理者ID　</label>
		<input type="text" name="username" class="input" autocomplete="off" id="username"/>
		<div style='clear:both;'></div>
		<label>パスワード</label>
		<input type="password" name="password" class="input" autocomplete="off" id="password"/><br/>
		<input type="submit" class="button button-primary" value="登　録" id="login"/>
		<span class='msg'></span>
		<div id="error"></div>
		</div>
		</form>
	</div>
</div>
</body>
</html>
