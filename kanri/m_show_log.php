<?php
require '../util/include.php';
$sub_title='全体視聴データ管理　-全体視聴データ情報一覧 -';
$systime=date('Y-m-d H:i:s',time());

$role=$_SESSION['role'];
$login_user=$_SESSION['login_user'];

$action = $_GET['action'];

$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
if(!$db){die("Cannot connect:" . mysqli_error($db));}
$dns = mysqli_select_db($db,DB_NAME);
if(!$dns){die("Cannot use db:" . mysqli_error($db));}
mysqli_set_charset($db,'utf8');

//Search
if ($action=='search_all'||$action=='search_date'||$action=='search_all_book'||$action=='search_date_book'){
	$page_size=100;
	if( isset($_GET['page']) ){
		$page = intval( $_GET['page'] );
	}
	else{
		$page = 1;
	}
	$rowCnt = 0;
	//Form
	$s_date_from=$_POST['s_date_from'];
	if($s_date_from==''&&$_GET['s_date_from']!=''){
		$s_date_from=$_GET['s_date_from'];
	}
	$s_date_to=$_POST['s_date_to'];
	if($s_date_to==''&&$_GET['s_date_to']!=''){
		$s_date_to=$_GET['s_date_to'];
	}
	//SQL
	$sql_all = "SELECT *,sum(read_cnt) read_cnt FROM mz_book_read_cnt WHERE 1";
	if($s_date_from!='') {
		$s_date_from=$s_date_from." 00:00:00";
		$sql_all .= " and unix_timestamp(concat(year,'-',month,'-',day)) >= ".strtotime($s_date_from);
	}
	if($s_date_to!='') {
		$s_date_to=$s_date_to." 23:59:59";
		$sql_all .= " and unix_timestamp(concat(year,'-',month,'-',day)) <= ".strtotime($s_date_to);
	}
	$s_date_from=str_replace(" 00:00:00","",$s_date_from);
	$s_date_to=str_replace(" 23:59:59","",$s_date_to);
	$result = mysqli_query($db,$sql_all) or die(mysqli_error($db));
	if(!$result){
		$rowCnt = -1;
	}
	$total=mysqli_num_rows($result);
	if($action=='search_date'){
		$sql_all .= " GROUP BY year,month,day";
	}if($action=='search_all_book'){
		$sql_all .= " GROUP BY book_id";
	}if($action=='search_date_book'){
		$sql_all .= " GROUP BY book_id,year,month,day";
	}
	$sql = sprintf("%s order by read_cnt desc limit %d,%d",$sql_all,($page-1)*$page_size,$page_size);
	//echo $sql;
	$result = mysqli_query($db,$sql);
	if(!$result){
		$rowCnt = -1;
	}
	$rowCnt=mysqli_num_rows($result);
	//paging
	if($rowCnt==0){
		$page_count = 0;
	}
	else{
		if( $total<$page_size ){ $page_count = 1; }
		if( $total%$page_size ){
			$page_count = (int)($total / $page_size) + 1;
		}else{
			$page_count = $total / $page_size;
		}
	}
	$page_string = '';
	if (($page == 1)||($page_count == 1)){
		$page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
	}
	else{
		$page_string .= '<a href=?action='.$action.'&page=1&s_date_from='.$s_date_from.'&s_date_to='.$s_date_to.'>トップページ</a>|<a href=?action='.$action.'&page='.($page-1) .'&s_date_from='.$s_date_from.'&s_date_to='.$s_date_to.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
	}
	if( ($page == $page_count) || ($page_count == 0) ){
		$page_string .= '次頁|最終ページ';
	}
	else{
		$page_string .= '<a href=?action='.$action.'&page='.($page+1).'&s_date_from='.$s_date_from.'&s_date_to='.$s_date_to .'>次頁</a>|<a href=?action='.$action.'&page='.$page_count.'&s_date_from='.$s_date_from.'&s_date_to='.$s_date_to.'>最終ページ</a>';
	}
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
	<title><?php echo $sub_title; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" >
	<meta http-equiv="content-style-type" content="text/css">
	<meta http-equiv="content-script-type" content="text/javascript">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<link href="../css/common.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="../js/common.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/assets/style.css" />
	<script>
		$(function() {
			$('#s_date_from').datepicker({
				dateFormat: 'yy-mm-dd',
				date: $('#s_date_from').val(),
				current: $('#s_date_from').val(),
				starts: 1,
				position: 'right',
				onBeforeShow: function () {
					$('#s_date_from').DatePickerSetDate($('#s_date_from').val(), true);
				},
				onChange: function (formated, dates) {
					$('#s_date_from').val(formated);
					$('#s_date_from').DatePickerHide();
				}
			});
			$('#s_date_to').datepicker({
				dateFormat: 'yy-mm-dd',
				date: $('#s_date_to').val(),
				current: $('#s_date_to').val(),
				starts: 1,
				position: 'right',
				onBeforeShow: function () {
					$('#s_date_to').DatePickerSetDate($('#s_date_to').val(), true);
				},
				onChange: function (formated, dates) {
					$('#s_date_to').val(formated);
					$('#s_date_to').DatePickerHide();
				}
			});
		});
	</script>
</head>
<body>
<div class='main'>
	<div class='subtitle'><?php echo $sub_title; ?></div>
	<form method='post' name='form1'>
		<div class='input-area'>
			<label class='search_label w100'>期間指定</label>
			<input type='text' name='s_date_from' id='s_date_from' class='w100' value='<?php echo $s_date_from;?>'/>
			 <span class="ftsize18 ml20 mr20 fl mt10">～</span>
			<input type='text' name='s_date_to' id='s_date_to' class='w100' value='<?php echo $s_date_to;?>'/>
			<div style='clear:both;'></div>
			<input type='button' class='buttonS bGreen ml20 mt10' value='指定期間全体サマリ' onclick='search_all();'/>
			<input type='button' class='buttonS bGreen ml20 mt10' value='指定期間日別サマリ' onclick='search_date();'/>
			<div style='clear:both;'></div>
			<input type='button' class='buttonS bGreen ml20 mt10' value='期間全体作品ごと　' onclick='search_all_book();'/>
			<input type='button' class='buttonS bGreen ml20 mt10' value='日別作品ごと　　　' onclick='search_date_book();'/>
		</div>
		<?php
		if ($rowCnt>0){
			echo "
				<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
					<tr bgcolor='#DBE6F5'>
					  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
					</tr>
				</table>";
					echo "
				<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<tr bgcolor='#DBE6F5'>
					  <th width='100px'>サマリ分類</th>
					  <th width='100px'>期間</th>
					  <th width='240px'>作品名</th>
					  <th width='100px'>視聴数</th>
					</tr>
				</table>
			";
			if($action=="search_all"){
				$cate_name="期間全体";
				$book_name="全体";
			}
			if($action=="search_date"){
				$cate_name="期間日別";
				$book_name="全体";
			}
			if($action=="search_all"||$action=="search_all_book") {
				if ($s_date_from != "" && $s_date_to == "") {
					$s_date_str = $s_date_from . "～";
				}
				if ($s_date_to != "" && $s_date_from == "") {
					$s_date_str = "～" . $s_date_to;
				}
				if ($s_date_from != "" && $s_date_to != "") {
					$s_date_str = $s_date_from . "～" . $s_date_to;
				}
			}
			$i=1;
			while($rs=mysqli_fetch_object($result))
			{
				if($action=="search_all_book"){
					$cate_name="期間作品毎";
					$book_name=$rs->book_name;
				}
				if($action=="search_date_book"){
					$cate_name="期間日別作品毎";
					$book_name=$rs->book_name;
				}
				if($action=="search_date"||$action=="search_date_book") {
					$s_date_str=$rs->year.'-'.$rs->month.'-'.$rs->day;
				}
				echo "
				<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<tr align='left' bgcolor='#EEF2F4'>
						<td width='100px'align='center'>".$cate_name."</td>
						<td width='100px'align='center'>".$s_date_str."</td>
						<td width='240px'>".$book_name."</td>
						<td width='100px'align='center'>".$rs->read_cnt."</td>
						</tr>
					</table>
				";
				$i++;
			}
			echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
			mysqli_close($db);
		}else{
			if ($action=='search'){
				echo "検索結果がありません。";
			}
		}
		?>
	</form>
	<script language="javascript" type="text/javascript">
		function search_all() {
			//var page_url="?action=search_all";
			//window.location.href=page_url;
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_all";
			document.form1.submit();
		}
		function search_date() {
			//var page_url="?action=search_date";
			//window.location.href=page_url;
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_date";
			document.form1.submit();
		}
		function search_all_book() {
			//var page_url="?action=search_all_book";
			//window.location.href=page_url;
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_all_book";
			document.form1.submit();
		}
		function search_date_book() {
			//var page_url="?action=search_date_book";
			//window.location.href=page_url;
			//期間
			if(document.form1.s_date_from.value == ""&&document.form1.s_date_to.value == ""){
				alert("期間を指定してください。");
				document.form1.s_date_from.focus();
				return false;
			}
			document.form1.action="?action=search_date_book";
			document.form1.submit();
		}
	</script>
</div>
</body>
</html>