<?php
	//include
	require '../util/include.php';

	$sub_title='トップ新着管理';
	$systime=date('Y-m-d H:i:s',time());

    $role=$_SESSION['role'];
    $login_user=$_SESSION['login_user'];

	$action = $_GET['action'];

	$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("Cannot connect:" . mysqli_error($db));
	}
	$dns = mysqli_select_db($db,DB_NAME);
	if(!$dns){
		die("Cannot use db:" . mysqli_error($db));
	}
	mysqli_set_charset($db,'utf8');

	//Update
	if ($action=='update'){
		$i_book_id = $_POST['i_book_id'];
		$i_chapter_id = $_POST['i_chapter_id'];
		$i_top_cover_1 = $_POST['i_top_cover_1'];
		$i_top_cover_2 = $_POST['i_top_cover_2'];
		$i_page_from = $_POST['i_page_from'];
		$i_page_to = $_POST['i_page_to'];
		$sql = sprintf("UPDATE mz_top_book SET book_id='%s',chapter_id=%d,top_cover_1=%d,top_cover_2=%d,page_from=%d,page_to=%d",
						$i_book_id,$i_chapter_id,$i_top_cover_1,$i_top_cover_2,$i_page_from,$i_page_to);
		$result = mysqli_query($db,$sql);
	}
	//Search
	if ($action=='search'||$action=='update'){
		$page_size=1000;
		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;
		//All
		$sqlall = "select a.*,(select book_name from mz_book b where a.book_id=b.book_id) book_name from mz_top_book a WHERE 1";
		$result = mysqli_query($db,$sqlall) or die(mysqli_error($db));
		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}
		$rowCntall=mysqli_num_rows($result);
		//Select current all
		$sql = sprintf("%s order by a.id limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);
		$result = mysqli_query($db,$sql);
		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}
		$rowCnt=mysqli_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			mysqli_close($db);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<?php
if ($rowCnt>0){
	echo "
		<table width='280px' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='120px'>ブックID</th>
              <th width='200px'>ブック名</th>
              <th width='60px'>話番号</th>
              <th width='150px'>TOPカバー1<br>（ファイル連番）</th>
              <th width='150px'>TOPカバー2<br>（ファイル連番）</th>
              <th width='150px'>コラ(１～６)From<br>（ファイル連番）</th>
              <th width='150px'>コラ(１～６)To<br>（ファイル連番）</th>
              <th width='60px'>操作</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysqli_fetch_object($result))
	{
	  echo "
			<table width='280px' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<td width='120px'align='center'>
						<input type='text' style='font-size:16px;width:90%;' name='i_book_id' id='i_book_id' value=".$rs->book_id.">
					</td>
					<td width='200px'align='center'>$rs->book_name</td>
					<td width='60px'align='center'>
						<input type='text' style='font-size:16px;width:90%;' name='i_chapter_id' id='i_chapter_id' value=".$rs->chapter_id.">
					</td>
					<td width='150px'align='center'>
						<input type='text' style='font-size:16px;width:90%;' name='i_top_cover_1' id='i_top_cover_1' value=".$rs->top_cover_1.">
					</td>
					<td width='150px'align='center'>
						<input type='text' style='font-size:16px;width:90%;' name='i_top_cover_2' id='i_top_cover_2' value=".$rs->top_cover_2.">
					</td>
					<td width='150px'align='center'>
						<input type='text' style='font-size:16px;width:90%;' name='i_page_from' id='i_page_from' value=".$rs->page_from.">
					</td>
					<td width='150px'align='center'>
						<input type='text' style='font-size:16px;width:90%;' name='i_page_to' id='i_page_to' value=".$rs->page_to.">
					</td>
					<td width='60px'align='center'><input type='button' class='buttonS bGreen' value='更新' onclick='updateChange()'/></td>
				</tr>
			 </table>
			";
	$i++;
	}

	mysqli_close($db);
}
?>
</form>
<script language="javascript" type="text/javascript">
	function updateChange() {
		document.form1.action="?action=update";
		document.form1.submit();
	}
</script>
</div>
</body>
</html>