<?php
session_start();
header("Expires:Mon,26 Jul 1970 05:00:00 GMT");
header("Last-Modified:".gmdate("D, d M Y H:i:s")."GMT");
header("Cache-Control:no-cache,must-revalidate");
header("Pragma:no-cache");
//include
require '../util/include.php';

//初期化
$role="";
$action="";
$sysdate="";
$systime="";
$loc_ip="";
$i_shop_name = "";
$i_shop_name_b = "";
$i_post_no = "";
$i_add_1 = "";
$i_add_2 = "";
$i_tel = "";
$i_email = "";
$i_start_date = "";
$i_memo = "";
$i_pass_ip=array();

//共通変数
$role=$_SESSION['role'];
$login_user=$_SESSION['login_user'];
$action = $_GET['action'];
$sysdate=date('Y-m-d',time());
$systime=date('Y-m-d H:i:s',time());
$loc_ip=get_real_ip();
//設定
if ($action=='confirm'){
    $sub_title='店舗情報管理　- 新規追加確認 -';
    $is_disabled="disabled='disabled'";
    $havePlaceholder='NO';
}elseif($action=='edit'){
    $sub_title='店舗情報管理　- 新規追加訂正 -';
    $is_disabled="";
    $havePlaceholder='YES';
}elseif ($action=='update'){
    $sub_title='店舗情報管理　- 編集 -';
    $is_disabled="";
    $havePlaceholder='NO';
}elseif($action=='delete'){
    $sub_title='店舗情報管理　- 削除 -';
    $sub_title='店舗情報管理　- 削除 -';
    $is_disabled="disabled='disabled'";
    $havePlaceholder='NO';
}else{
    $sub_title='店舗情報管理　- 新規追加 -';
    $is_disabled="";
    $havePlaceholder='YES';
}
//FORM項目取得
if ($action=='confirm'||$action=='edit'||$action=='insert'||$action=='update_submit'){
    //form項目
    $u_id = $_POST['u_id'];
    $update_flg=$_POST['update_flg'];
    $i_mg_no = $_POST['i_mg_no'];
    $i_password=$_POST['i_pwd'];
    $i_shop_name = $_POST['i_shop_name'];
    $i_shop_name_b = $_POST['i_shop_name_b'];
    $i_post_no = $_POST['i_post_no'];
    $i_add_1 = $_POST['i_add_1'];
    $i_add_2 = $_POST['i_add_2'];
    $i_tel = $_POST['i_tel'];
    $i_email = $_POST['i_email'];
    $i_start_date = $_POST['i_start_date'];
    $i_memo = $_POST['i_memo'];
    $i_memo=htmlspecialchars($i_memo);
    $i=1;
    while($_POST['i_pass_ip'.$i]!=''){
        $i_pass_ip[$i]=$_POST['i_pass_ip'.$i];
        $i++;
    }
}elseif ($action=='update'||$action=='delete'){
    $u_id = $_GET['u_id'];
    $update_flg='1';
}else{
    $i_shop_name = "";
    $i_shop_name_b = "";
    $i_post_no = "";
    $i_add_1 = "";
    $i_add_2 = "";
    $i_tel = "";
    $i_email = "";
    $i_start_date = "";
    $i_memo = "";
    $i_pass_ip=array();
}
//DB接続
if ($action=='insert'||$action=='update_submit'||$action=='update'||$action=='delete'||$action=='delete_submit') {
    $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (!$db) {
        die("Cannot connect:" . mysqli_error($db));
    }
    $dns = mysqli_select_db($db, DB_NAME);
    if (!$dns) {
        die("Cannot use db:" . mysqli_error($db));
    }
    mysqli_set_charset($db, 'utf8');
}
if ($action=='update'||$action=='delete'){
    $log_str = "$systime $loc_ip INFO：▼店舗情報取得開始：shop_id = $u_id \r\n";
    error_log($log_str,3,'../log/gen.log');
    if($u_id==''){
        db_disConn($result, $link);
        $log_str = "$systime ERR：shop_id取得エラー！ \r\n";
        $log_str .= "$systime $loc_ip INFO：▲店舗情報取得異常終了 \r\n";
        error_log($log_str,3,'../log/gen.log');
        $err_cd_list[]="02";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    //店舗情報取得
    $sql = "select * from mz_shop WHERE 1";
    $sql .= " and shop_id = $u_id";

    $result = mysqli_query($db,$sql);
    $rs=mysqli_fetch_object($result);

    $i_mg_no = $rs->mg_no;
    $i_password=$rs->pwd;
    $i_shop_name = $rs->shop_name;
    $i_shop_name_b = $rs->shop_name_b;
    $i_post_no = $rs->post_no;
    $i_add_1 = $rs->add_1;
    $i_add_2 = $rs->add_2;
    $i_tel = $rs->tel;
    $i_email = $rs->email;
    $i_start_date = date('Y-m-d',$rs->start_date);
    $i_memo = $rs->memo;
    $i_memo=htmlspecialchars($i_memo);

    $sql = "select * from mz_shop_ip WHERE 1";
    $sql .= " and shop_id = $u_id";
    $sql .= " order by shop_ip";
    $result_shop_ip = mysqli_query($db,$sql);
    $i=1;
    while($rs_shop_ip=mysqli_fetch_object($result_shop_ip)){
        $i_pass_ip[$i]=$rs_shop_ip->shop_ip;
        $i++;
    }
    $h_num_cnt=$i+1;

    mysqli_close($db);
    $log_str = "$systime $loc_ip INFO：▲店舗情報取得正常終了：shop_id = $u_id \r\n";
    error_log($log_str,3,'../log/gen.log');
}
//確認画面チェック
if ($action=='confirm'){
    if($update_flg!='1'){
        $log_str = "$systime $loc_ip INFO：▼店舗名存在チェック開始 \r\n";
        error_log($log_str,3,'../log/gen.log');
        //店舗名存在チェック
        $ret=searchShopInfo($i_shop_name);
        if($ret=='1'){
            db_disConn($result, $link);
            $log_str = "$systime ERR：店舗名存在チェックエラー！ \r\n";
            $log_str .= "$systime $loc_ip INFO：▲店舗名存在チェック異常終了 \r\n";
            error_log($log_str,3,'../log/gen.log');

            $err_cd_list[]="02";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }
        $log_str = "$systime $loc_ip INFO：▲店舗名存在チェック正常終了！！ \r\n";
        error_log($log_str,3,'../log/gen.log');
    }
}
//店舗登録実行
if ($action=='insert'){
    $log_str = "$systime $loc_ip INFO：▼店舗名存在チェック開始 \r\n";
    error_log($log_str,3,'../log/gen.log');
    //店舗名存在チェック
    $ret=searchShopInfo($i_shop_name);
    if($ret=='1'){
        db_disConn($result, $link);
        $log_str = "$systime ERR：店舗名存在チェックエラー！ \r\n";
        $log_str .= "$systime $loc_ip INFO：▲店舗名存在チェック異常終了 \r\n";
        error_log($log_str,3,'../log/gen.log');
        $err_cd_list[]="02";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $log_str = "$systime $loc_ip INFO：▲店舗名存在チェック正常終了！！ \r\n";
    error_log($log_str,3,'../log/gen.log');
    //管理番号採番
    $sql = "UPDATE mz_shop_mg_no SET id = LAST_INSERT_ID(id+1)";
    $result_id = mysqli_query($db, $sql);
    $sql = "select last_insert_id() as id";
    $result_b_id = mysqli_query($db, $sql);
    $row_b_id = mysqli_fetch_assoc($result_b_id);
    $i_mg_no = "S" . str_pad($row_b_id['id'], 10, "0", STR_PAD_LEFT);
    $init_pwd=substr(md5(rand(111111,999999)),0,6);

    $log_str = "$systime $loc_ip INFO：▼店舗情報登録開始 \r\n";
    error_log($log_str,3,'../log/gen.log');
    $sql = sprintf(
        "insert into mz_shop
        (mg_no,pwd,shop_name,shop_name_b,post_no,add_1,add_2,tel,email,start_date,memo,del_flg,insert_time,update_time)
         values
        ('%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,'%s',%d,%d,%d)",
        $i_mg_no,$init_pwd,$i_shop_name,$i_shop_name_b,$i_post_no,$i_add_1,$i_add_2,$i_tel,$i_email,strtotime($i_start_date),$i_memo,0,strtotime($systime), strtotime($systime)
    );
    $log_str = "$systime $loc_ip INFO：店舗情報登録 INSERT SQL文： ".$sql."\r\n";
    error_log($log_str,3,'../log/gen.log');

    $result = mysqli_query($db,$sql);
    if(!$result){
        db_disConn($result, $link);
        $log_str = "$systime ERR：店舗情報DB登録異常！ \r\n";
        $log_str .= "$systime $loc_ip INFO：▲店舗情報登録異常終了 \r\n";
        error_log($log_str,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $log_str = "$systime $loc_ip INFO：▲店舗情報登録正常終了！！ \r\n";
    error_log($log_str,3,'../log/gen.log');

    $sql_shop_id = "select * from mz_shop WHERE 1 and mg_no='".$i_mg_no."'";
    $result_shop_id = mysqli_query($db,$sql_shop_id);
    $rs_shop_id=mysqli_fetch_object($result_shop_id);
    $loc_ip_shop_id=$rs_shop_id->shop_id;

    $i=1;
    while ($i_pass_ip[$i]!=''&&$loc_ip_shop_id!=''){
        $log_str = "$systime $loc_ip INFO：▼店舗IP情報登録開始 \r\n";
        error_log($log_str,3,'../log/gen.log');
        $sql = sprintf("insert into mz_shop_ip (shop_id,shop_ip) values (%d,'%s')",$loc_ip_shop_id,$i_pass_ip[$i]);
        $log_str = "$systime $loc_ip INFO：店舗IP情報登録 INSERT SQL文： ".$sql."\r\n";
        error_log($log_str,3,'../log/gen.log');

        $result = mysqli_query($db,$sql);
        if(!$result){
            db_disConn($result, $link);
            $log_str = "$systime ERR：店舗IP情報DB登録異常！ \r\n";
            $log_str .= "$systime $loc_ip INFO：▲店舗IP情報登録異常終了 \r\n";
            error_log($log_str,3,'../log/gen.log');
            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }
        $log_str = "$systime $loc_ip INFO：▲店舗IP情報登録正常終了！！ \r\n";
        error_log($log_str,3,'../log/gen.log');
        $i++;
    }
    mysqli_close($db);
    $url= URL_PATH . "m_shop.php?action=search&role=".$role."&l_id=".$login_user."#".$i_mg_no;
    redirect($url);
}
//店舗更新実行
if ($action=='update_submit'){
    $log_str = "$systime $loc_ip INFO：▼店舗名存在チェック開始 \r\n";
    error_log($log_str,3,'../log/gen.log');
    //店舗名存在チェック
    $ret=searchShopInfoForUpdate($i_shop_name,$u_id);
    if($ret=='1'){
        db_disConn($result, $link);
        $log_str = "$systime ERR：店舗名存在チェックエラー！ \r\n";
        $log_str .= "$systime $loc_ip INFO：▲店舗名存在チェック異常終了 \r\n";
        error_log($log_str,3,'../log/gen.log');
        $err_cd_list[]="02";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $log_str = "$systime $loc_ip INFO：▲店舗名存在チェック正常終了！！ \r\n";
    error_log($log_str,3,'../log/gen.log');

    $log_str = "$systime $loc_ip INFO：▼店舗情報更新開始 \r\n";
    error_log($log_str,3,'../log/gen.log');
    $sql = sprintf("update mz_shop set pwd='%s',shop_name='%s',shop_name_b='%s',add_1='%s',add_2='%s',tel='%s',email='%s',start_date=%d,memo='%s',update_time=%d where shop_id=%d",
                    $i_password,$i_shop_name,$i_shop_name_b,$i_add_1,$i_add_2,$i_tel,$i_email,strtotime($i_start_date),$i_memo,strtotime($systime),$u_id);
    $log_str = "$systime $loc_ip INFO：店舗情報更新 UPDATE SQL文： ".$sql."\r\n";
    error_log($log_str,3,'../log/gen.log');
    $result = mysqli_query($db,$sql);
    if(!$result){
        db_disConn($result, $link);
        $log_str = "$systime ERR：店舗情報DB更新異常！ \r\n";
        $log_str .= "$systime $loc_ip INFO：▲店舗情報更新異常終了 \r\n";
        error_log($log_str,3,'../log/gen.log');
        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $log_str = "$systime $loc_ip INFO：▲店舗情報更新正常終了！！ \r\n";
    error_log($log_str,3,'../log/gen.log');

    $log_str = "$systime $loc_ip INFO：▼店舗IP情報削除処理開始 \r\n";
    error_log($log_str,3,'../log/gen.log');
    $sql = sprintf("delete from mz_shop_ip where shop_id=%d",$u_id);
    $log_str = "$systime $loc_ip INFO：店舗IP情報削除 DELETE SQL文： ".$sql."\r\n";
    error_log($log_str,3,'../log/gen.log');

    $result = mysqli_query($db,$sql);
    if(!$result){
        db_disConn($result, $link);
        $log_str = "$systime ERR：店舗IP情報DB削除異常！ \r\n";
        $log_str .= "$systime $loc_ip INFO：▲店舗IP情報削除異常終了 \r\n";
        error_log($log_str,3,'../log/gen.log');
        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $log_str = "$systime $loc_ip INFO：▲店舗IP情報削除処理正常終了！！ \r\n";
    error_log($log_str,3,'../log/gen.log');

    $i=1;
    while ($i_pass_ip[$i]!=''&&$u_id!=''){
        $log_str = "$systime $loc_ip INFO：▼店舗IP情報登録開始 \r\n";
        error_log($log_str,3,'../log/gen.log');
        $sql = sprintf("insert into mz_shop_ip (shop_id,shop_ip) values (%d,'%s')",$u_id,$i_pass_ip[$i]);
        $log_str = "$systime $loc_ip INFO：店舗IP情報登録 INSERT SQL文： ".$sql."\r\n";
        error_log($log_str,3,'../log/gen.log');

        $result = mysqli_query($db,$sql);
        if(!$result){
            db_disConn($result, $link);
            $log_str = "$systime ERR：店舗IP情報DB登録異常！ \r\n";
            $log_str .= "$systime $loc_ip INFO：▲店舗IP情報登録異常終了 \r\n";
            error_log($log_str,3,'../log/gen.log');
            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }
        $log_str = "$systime $loc_ip INFO：▲店舗IP情報登録正常終了！！ \r\n";
        error_log($log_str,3,'../log/gen.log');
        $i++;
    }
    mysqli_close($db);
    $url= URL_PATH . "m_shop.php?action=search&role=".$role."&l_id=".$login_user."#".$i_mg_no;;
    redirect($url);
}
//削除実行
if ($action=='delete_submit'){

    $i_login_id=$_POST['h_login_id'];
    $u_id=$_POST['u_id'];

    $log_str = "$systime $loc_ip INFO：▼店舗情報管理者情報削除更新開始 \r\n";
    error_log($log_str,3,'../log/gen.log');

    $sql = sprintf("update den_admin set status=1 where u_id='%s'",$i_login_id);

    $log_str = "$systime $loc_ip INFO：店舗情報管理者情報削除更新 UPDATE SQL文： ".$sql."\r\n";
    error_log($log_str,3,'../log/gen.log');

    $result = mysqli_query($db,$sql);
    if(!$result){

        db_disConn($result, $link);
        $log_str = "$systime ERR：店舗情報管理者情報DB削除更新異常！ \r\n";
        $log_str .= "$systime $loc_ip INFO：▲店舗情報管理者情報削除更新異常終了 \r\n";
        error_log($log_str,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $log_str = "$systime $loc_ip INFO：▲店舗情報管理者情報削除更新正常終了！！ \r\n";
    error_log($log_str,3,'../log/gen.log');

    $log_str = "$systime $loc_ip INFO：▼店舗情報削除更新開始 \r\n";
    error_log($log_str,3,'../log/gen.log');
    $sql = sprintf("update mz_shop set del_flg=1 where shop_id=%d",$u_id);
    $log_str = "$systime $loc_ip INFO：店舗情報削除更新 UPDATE SQL文： ".$sql."\r\n";
    error_log($log_str,3,'../log/gen.log');

    $result = mysqli_query($db,$sql);
    if(!$result){
        db_disConn($result, $link);
        $log_str = "$systime ERR：店舗情報DB削除更新異常！ \r\n";
        $log_str .= "$systime $loc_ip INFO：▲店舗情報削除更新異常終了 \r\n";
        error_log($log_str,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }

    mysqli_close($db);
    $log_str = "$systime $loc_ip INFO：▲店舗情報削除更新正常終了！！ \r\n";
    error_log($log_str,3,'../log/gen.log');


    $url= URL_PATH . "m_shop.php?action=search&role=".$role."&l_id=".$login_user;
    redirect($url);
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
    <title><?php echo $sub_title; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache,must-revalidate">
    <meta http-equiv="expires" content="Wed, 23 Aug 2006 12:40:27 UTC" />
    <meta http-equiv="content-style-type" content="text/css">
    <meta http-equiv="content-script-type" content="text/javascript">
    <link href="../css/common.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../js/assets/style.css" />
    <link rel="stylesheet" type="text/css" href="../js/assets/prettify.css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../js/ui/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="../js/assets/prettify.js"></script>
</head>
<body>
<div class='main'>
    <div class='subtitle'><?php echo $sub_title; ?></div>
    <form enctype='multipart/form-data' method='post' name='upform'>
        <div class='input-area'>
            <?php
            if(($action=='confirm'||$action=='update'||$action=='delete'||$role!='1')&&$update_flg=='1') {
                echo "
                    <label class='w300'>管理番号（ログインID）</label>
                    <label class='w200' style='margin-left: 0px;'>" . $i_mg_no . "</label>
                    <input type='hidden' name='i_mg_no' id='i_mg_no' value='".$i_mg_no."'/>
                    <div style='clear:both;'></div>
                    <label class='w300'>パスワード<span style='color:red;margin-left: 10px;'>※必須※</span></label>
                    <input type='text' class='w200' name='i_pwd' id='i_pwd' value='" . $i_password . "' " . $is_disabled . "/>
                    <div style='clear:both;'></div>
                    <label class='w300'>パスワード確認<span style='color:red;margin-left: 10px;'>※必須※</span></label>
                    <input type='text' class='w200' name='i_pwd_r' id='i_pwd_r' value='" . $i_password . "' " . $is_disabled . "/>
                    <div style='clear:both;'></div>
                ";
            }
            ?>
            <label class='w300'>店舗名<span style="color:red;margin-left: 10px;">※必須※</span></label>
            <input type='text' class='w500' name='i_shop_name' id='i_shop_name' value='<?php echo $i_shop_name;?>' <?php echo $is_disabled;?>/>
            <div style='clear:both;'></div>
            <label class='w300'>支店名<span style="color:red;margin-left: 10px;">※必須※</span></label>
            <input type='text' class='w500' name='i_shop_name_b' id='i_shop_name_b' value='<?php echo $i_shop_name_b;?>' <?php echo $is_disabled;?>/>
            <div style='clear:both;'></div>
            <label class='w300'>〒</label>
            <input type='text' class='w150' name='i_post_no' id='i_post_no' value='<?php echo $i_post_no;?>' <?php echo $is_disabled;?>/>
            <div style='clear:both;'></div>
            <label class='w300'>住所1<span style="margin-left: 10px;">都道府県、市区町村</span></label>
            <input type='text' class='w500' name='i_add_1' id='i_add_1' value='<?php echo $i_add_1;?>' <?php echo $is_disabled;?>/>
            <div style='clear:both;'></div>
            <label class='w300'>住所2<span style="margin-left: 10px;">番地、建物名</span></label>
            <input type='text' class='w500' name='i_add_2' id='i_add_2' value='<?php echo $i_add_2;?>' <?php echo $is_disabled;?>/>
            <div style='clear:both;'></div>
            <label class='w300'>電話番号</label>
            <input type='text' class='w150' name='i_tel' id='i_tel' value='<?php echo $i_tel;?>' <?php echo $is_disabled;?>/>
            <div style='clear:both;'></div>
            <label class='w300'>メールアドレス</label>
            <input type='text' class='w150' name='i_email' id='i_email' value='<?php echo $i_email;?>' <?php echo $is_disabled;?>/>
            <div style='clear:both;'></div>
            <label class='w300'>開通日</label>
            <input type='text' class='w150' name='i_start_date' id='i_start_date' value='<?php echo $i_start_date;?>' <?php echo $is_disabled;?>/>
            <div style='clear:both;'></div>
            <label class='w300'>メモ（フリーワード）</label>
            <textarea name='i_memo' id='i_memo' <?php echo $is_disabled;?>><?php echo htmlspecialchars_decode($i_memo);?></textarea>
            <div style='clear:both;'></div>
            <label class='w500 mt3'>IP情報(ローカルIP：<?php echo $loc_ip;?>)<span style="color:red;margin-left: 10px;">※必須※</span></label>
            <div style='clear:both;'></div>
            <?php
            if($action=='confirm'||$action=='delete'){
                $i=1;
                while ($i_pass_ip[$i]!=''){
                    echo"
					<div name='pass_ip_row' id='pass_ip_row'>
						<label class='w300'>IP</label>
						<input type='text' class='w150' name='i_pass_ip".$i."' id='i_pass_ip".$i."' ".$is_disabled." value=".$i_pass_ip[$i].">
						<div style='clear:both;'></div>
					</div>
				";
                    $i++;
                }
            }elseif($action=='update'||$action=='edit'){
                $i=1;
                while ($i_pass_ip[$i]!=''){
                    echo"
					<div name='pass_ip_row_edit' id='pass_ip_row_edit'>
						<label class='w300'>IP</label>
						<input type='text' class='w150' name='i_pass_ip".$i."' id='i_pass_ip".$i."' ".$is_disabled." value=".$i_pass_ip[$i].">
						<div style='clear:both;'></div>
					</div>
				";
                    $i++;
                }
                echo"
				<div name='pass_ip_row' id='pass_ip_row'>
					<label class='w300'>IP</label>
					<input type='text' class='w150' name='i_pass_ip".$i."' id='i_pass_ip".$i."' ". $is_disabled." />
					<input type='button' onclick='addPassIPRow()' id='btn".$i."' value='追加'/>
					<div style='clear:both;'></div>
				</div>
			";
            }
            else{
                echo"
				<div name='pass_ip_row' id='pass_ip_row'>
					<label class='w300'>IP</label>
					<input type='text' class='w150' name='i_pass_ip1' id='i_pass_ip1' ". $is_disabled." />
					<input type='button' onclick='addPassIPRow()' id='btn1' value='追加'/>
					<div style='clear:both;'></div>
				</div>
			";
            }
            ?>
            <div name='copy_row' id='copy_row' style='display:none;'>
                <label class='w300'>IP<span style="color:red;margin-left: 10px;">※必須※</span></label>
                <input type='text' id='i_pass_ip' name='i_pass_ip' class='w150'/>
                <input type='button' onclick='addPassIPRow()' id='btn' value='追加'/>
                <div style='clear:both;'></div>
            </div>
            <input type="hidden" name='u_id' value="<?php echo $u_id;?>" />
            <input type="hidden" name='update_flg' value="<?php echo $update_flg;?>"/>
            <?php
            if($action=='update'||$action=='edit'){
                echo"<input type='hidden' name='h_num_cnt' id='h_num_cnt' value='".$h_num_cnt."'/>";
            }else{
                echo"<input type='hidden' name='h_num_cnt' id='h_num_cnt' value='2'/>";
            }
            ?>
            <?php
            if($action=='confirm'){
                if($update_flg=='1'){
                    echo "
                <input type='button' class='buttonS bGreen ml190 w200 mt40' value='訂正' onclick='editMode();'/>
                <input type='button' class='buttonS bGreen ml100 w200 mt40' value='更新' onclick='update_submit();'/>
            ";
                }else{
                    echo "
                <input type='button' class='buttonS bGreen ml190 w200 mt40' value='訂正' onclick='editMode();'/>
                <input type='button' class='buttonS bGreen ml100 w200 mt40' value='送信' onclick='confirmSubmit();'/>
            ";
                }
            }
            elseif($action=='delete'){
                echo "<input type='button' class='buttonS bGreen ml190 w200 mt40' value='削除する' onclick='delete_submit();'/>";
            }
            else{
                echo "<input type='button' class='buttonS bGreen ml190 w200 mt40' value='確認画面へ' onclick='moveConfirm();'/>";
            }
            ?>
        </div>

        <script type="text/javascript" language="javascript">
            function moveConfirm() {
                if(document.upform.update_flg.value == "1"){
                    //パスワード
                    if(document.upform.i_pwd.value == ""){
                        alert("パスワードを入力してください。");
                        document.upform.i_pwd.focus();
                        return false;
                    }
                    //パスワード確認
                    if(document.upform.i_pwd_r.value != document.upform.i_pwd.value){
                        alert("パスワードを確認してください。");
                        document.upform.i_pwd_r.focus();
                        return false;
                    }
                }
                //店舗名
                if(document.upform.i_shop_name.value == ""){
                    alert("店舗名を入力してください。");
                    document.upform.i_shop_name.focus();
                    return false;
                }
                //支店名
                if(document.upform.i_shop_name_b.value == ""){
                    alert("支店名を入力してください。");
                    document.upform.i_shop_name_b.focus();
                    return false;
                }
                //〒
//                if(document.upform.i_post_no.value == ""){
//                    alert("郵便番号を入力してください。");
//                    document.upform.i_post_no.focus();
//                    return false;
//                }
                //住所1
//                if(document.upform.i_add_1.value == ""){
//                    alert("住所1を入力してください。");
//                    document.upform.i_add_1.focus();
//                    return false;
//                }
                //住所2
//                if(document.upform.i_add_2.value == ""){
//                    alert("住所2を入力してください。");
//                    document.upform.i_add_2.focus();
//                    return false;
//                }
                //電話番号
//                if(document.upform.i_tel.value == ""){
//                    alert("電話番号を入力してください。");
//                    document.upform.i_tel.focus();
//                    return false;
//                }
                //メールアドレス
//                if(document.upform.i_email.value == ""){
//                    alert("メールアドレスを入力してください。");
//                    document.upform.i_email.focus();
//                    return false;
//                }
                //開通日
//                if(document.upform.i_start_date.value == ""){
//                    alert("開通日を入力してください。");
//                    document.upform.i_start_date.focus();
//                    return false;
//                }
                //メモ（フリーワード）
//                if(document.upform.i_memo.value == ""){
//                    alert("メモを確認してください。");
//                    document.upform.i_memo.focus();
//                    return false;
//                }
                //IP
                if(document.upform.i_pass_ip1.value == ""){
                    alert("IPを入力してください。");
                    document.upform.i_pass_ip1.focus();
                    return false;
                }

                //submit
                document.upform.action="?action=confirm";
                document.upform.submit();
            }
            function confirmSubmit() {
                document.getElementById('i_shop_name').disabled=false;
                document.getElementById('i_shop_name_b').disabled=false;
                document.getElementById('i_post_no').disabled=false;
                document.getElementById('i_add_1').disabled=false;
                document.getElementById('i_add_2').disabled=false;
                document.getElementById('i_tel').disabled=false;
                document.getElementById('i_email').disabled=false;
                document.getElementById('i_start_date').disabled=false;
                document.getElementById('i_memo').disabled=false;
                $('input').attr("disabled",false);

                document.upform.action="?action=insert";
                document.upform.submit();
            }
            function update_submit() {
                document.getElementById('i_shop_name').disabled=false;
                document.getElementById('i_shop_name_b').disabled=false;
                document.getElementById('i_post_no').disabled=false;
                document.getElementById('i_add_1').disabled=false;
                document.getElementById('i_add_2').disabled=false;
                document.getElementById('i_tel').disabled=false;
                document.getElementById('i_email').disabled=false;
                document.getElementById('i_start_date').disabled=false;
                document.getElementById('i_memo').disabled=false;
                $('input').attr("disabled",false);

                document.upform.action="?action=update_submit";
                document.upform.submit();
            }
            function delete_submit() {
                document.upform.action="?action=delete_submit";
                document.upform.submit();
            }

            function editMode(){
                document.getElementById('i_shop_name').disabled=false;
                document.getElementById('i_shop_name_b').disabled=false;
                document.getElementById('i_post_no').disabled=false;
                document.getElementById('i_add_1').disabled=false;
                document.getElementById('i_add_2').disabled=false;
                document.getElementById('i_tel').disabled=false;
                document.getElementById('i_email').disabled=false;
                document.getElementById('i_start_date').disabled=false;
                document.getElementById('i_memo').disabled=false;
                $('input').attr("disabled",false);

                document.upform.action="?action=edit";
                document.upform.submit();
            }
            function addPassIPRow(){
                var rownum=document.getElementById('h_num_cnt').value;
                var currentBtn = document.getElementById('btn'+(Number(rownum)-1));

                var thenew= document.createElement('div');
                thenew.innerHTML = document.getElementById('copy_row').innerHTML.replace(new RegExp("i_pass_ip",'g'),"i_pass_ip"+rownum);
                thenew.innerHTML = thenew.innerHTML.replace("btn","btn"+rownum);

                document.getElementById('pass_ip_row').appendChild(thenew);

                currentBtn.style.display="none";

                document.getElementById('h_num_cnt').value=(Number(rownum)+1);
            }
        </script>
    </form>
</div>
</body>
</html>