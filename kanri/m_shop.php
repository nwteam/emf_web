<?php
	//include
	require '../util/include.php';
	session_start();
	$sub_title='店舗情報登録・管理　- 店舗情報一覧 -';
	$systime=date('Y-m-d H:i:s',time());

	$role=$_SESSION['role'];
	$login_user=$_SESSION['login_user'];

	$action = $_GET['action'];

	$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){die("Cannot connect:" . mysqli_error($db));}
	$dns = mysqli_select_db($db,DB_NAME);
	if(!$dns){die("Cannot use db:" . mysqli_error($db));}
	mysqli_set_charset($db,'utf8');

	if($role!=1&&$role!=3){
		$sql_shop_info = sprintf("select * from mz_shop WHERE mg_no = '%s'",$login_user);
		$result_shop_info = mysqli_query($db,$sql_shop_info);
		$shop_info=mysqli_fetch_object($result_shop_info);
		$mg_no=$shop_info->mg_no;
		$shop_name=$shop_info->shop_name;
		$shop_name_b=$shop_info->shop_name_b;
	}
	//Delete
	if ($action=='delete'){
		$u_id = $_GET['u_id'];
		$sql = sprintf("delete from mz_shop WHERE shop_id = %d",$u_id);
		$result = mysqli_query($db,$sql);
		$sql = sprintf("delete from mz_shop_ip WHERE shop_id = %d",$u_id);
		$result = mysqli_query($db,$sql);
	}
	//Search
	if ($action=='search'||$action=='delete'){
		$page_size=100;
		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;
		//Form
		$s_mg_no=$_POST['s_mg_no'];
		if($s_mg_no==''&&$_GET['s_mg_no']!=''){
			$s_mg_no=$_GET['s_mg_no'];
		}
		$s_shop_name=$_POST['s_shop_name'];
		if($s_shop_name==''&&$_GET['s_shop_name']!=''){
			$s_shop_name=$_GET['s_shop_name'];
		}
		$s_shop_name_b=$_POST['s_shop_name_b'];
		if($s_shop_name_b==''&&$_GET['s_shop_name_b']!=''){
			$s_shop_name_b=$_GET['s_shop_name_b'];
		}
		$s_shop_ip=$_POST['s_shop_ip'];
		if($s_shop_ip==''&&$_GET['s_shop_ip']!=''){
			$s_shop_ip=$_GET['s_shop_ip'];
		}
		//SQL
		if($s_shop_ip!='') {
			$sqlall .= " select distinct a.* from mz_shop a left join mz_shop_ip b on a.shop_id=b.shop_id WHERE FIND_IN_SET('".mysqli_real_escape_string($db,trim($s_shop_ip))."',b.shop_ip) ";
		}else{
			$sqlall = "select distinct a.* from mz_shop a left join mz_shop_ip b on a.shop_id=b.shop_id WHERE 1";
		}
		if($role!=1&&$role!=3){
			$sqlall .= " and a.mg_no like '%".mysqli_real_escape_string($db,$login_user)."%'";
		}
		if($s_mg_no!='') {
				$sqlall .= " and a.mg_no like '%".mysqli_real_escape_string($db,$s_mg_no)."%'";
		}
		if($s_shop_name!='') {
			$sqlall .= " and a.shop_name like '%".mysqli_real_escape_string($db,$s_shop_name)."%'";
		}
		if($s_shop_name_b!='') {
			$sqlall .= " and a.shop_name_b like '%".mysqli_real_escape_string($db,$s_shop_name_b)."%'";
		}
		$result = mysqli_query($db,$sqlall) or die(mysqli_error($db));
		if(!$result){
			$rowCnt = -1;
		}
		$rowCntall=mysqli_num_rows($result);
		$sql = sprintf("%s order by shop_id limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);
		$result = mysqli_query($db,$sql);
		if(!$result){
			$rowCnt = -1;
		}
		$rowCnt=mysqli_num_rows($result);
		//paging
		if($rowCnt==0){
			$page_count = 0;
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1&s_mg_no='.$s_mg_no.'&s_shop_name='.$s_shop_name.'&s_shop_name_b='.$s_shop_name_b.'&s_shop_ip='.$s_shop_ip.'>トップページ</a>|<a href=?action=search&page='.($page-1).'&s_mg_no='.$s_mg_no.'&s_shop_name='.$s_shop_name.'&s_shop_name_b='.$s_shop_name_b.'&s_shop_ip='.$s_shop_ip.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'&s_mg_no='.$s_mg_no.'&s_shop_name='.$s_shop_name.'&s_shop_name_b='.$s_shop_name_b.'&s_shop_ip='.$s_shop_ip.'>次頁</a>|<a href=?action=search&page='.$page_count.'&s_mg_no='.$s_mg_no.'&s_shop_name='.$s_shop_name.'&s_shop_name_b='.$s_shop_name_b.'&s_shop_ip='.$s_shop_ip.'>最終ページ</a>';
		}
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
<link rel="stylesheet" type="text/css" href="../js/assets/style.css" />
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
    <label class='search_label w100'>管理番号</label>
	<?php if($role==1||$role==3){?>
		<input type='text' name='s_mg_no' id='s_mg_no' class='w500' value='<?php echo $s_mg_no;?>'/>
	<?php }else{?>
		<label class='w500' style='margin-left: 0px;'><?php echo $mg_no;?></label>
		<input type='hidden' name='s_mg_no' id='s_mg_no' class='w500' value='<?php echo $mg_no;?>'/>
	<?php }?>
    <div style='clear:both;'></div>

    <label class='search_label w100'>店舗名</label>
	<?php if($role==1||$role==3){?>
	<input type='text' name='s_shop_name' id='s_shop_name' class='w500' value='<?php echo $s_shop_name;?>'/>
	<?php }else{?>
		<label class='w500' style='margin-left: 0px;'><?php echo $shop_name;?></label>
		<input type='hidden' name='s_shop_name' id='s_shop_name' class='w500' value='<?php echo $shop_name;?>'/>
	<?php }?>
    <div style='clear:both;'></div>
	<label class='search_label w100'>支店名</label>
	<?php if($role==1||$role==3){?>
		<input type='text' name='s_shop_name_b' id='s_shop_name_b' class='w500' value='<?php echo $s_shop_name_b;?>'/>
	<?php }else{?>
		<label class='w500' style='margin-left: 0px;'><?php echo $shop_name_b;?></label>
		<input type='hidden' name='s_shop_name_b' id='s_shop_name_b' class='w500' value='<?php echo $shop_name_b;?>'/>
	<?php }?>
    <div style='clear:both;'></div>
	<?php if($role==1||$role==3){?>
    <label class='search_label w100'>IP</label><input type='text' name='s_shop_ip' id='s_shop_ip' class='w500' value='<?php echo $s_shop_ip;?>'/>
    <div style='clear:both;'></div>
	<?php }?>
    <input type='submit' class='buttonS bGreen ml20' value='検索'/>
	<?php if($role==1||$role==3){?>
    <input type='button' class='buttonS bGreen ml20' value='新規登録' onclick='addInfo();'/>
	<?php }?>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='60px'>通しNo</th>
              <th width='100px'>管理番号</th>
              <th width='100px'>パスワード</th>
              <th width='240px'>店舗名</th>
              <th width='240px'>支店名</th>
              <th width='100px'>IP情報</th>
              <th width='100px'>登録日</th>
              <th width='100px'>更新日</th>
              <th width='100px'>操作</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysqli_fetch_object($result))
	{
		$value_ip=array();
		$ip_info="";
		$sql_ip = "select shop_ip from mz_shop_ip WHERE shop_id=".$rs->shop_id;
		$result_ip = mysqli_query($db,$sql_ip) or die(mysqli_error($db));
		if(!$result_ip){
			$rowCnt = -1;
		}
		$j=0;
		while($rs_ip=mysqli_fetch_object($result_ip)){
			$value_ip[$j]=$rs_ip->shop_ip;
			$j++;
		}
		$value_ip= implode(",",$value_ip);
		$ip_info=str_replace(",","<br>",$value_ip);
	 	echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<td width='60px'align='center'>".$rs->shop_id."</td>
					<td width='100px'align='center' id='".$rs->mg_no."'>".$rs->mg_no."</td>
					<td width='100px'align='center'>".$rs->pwd."</td>
					<td width='240px'>".$rs->shop_name."</td>
					<td width='240px'>".$rs->shop_name_b."</td>
					<td width='100px'align='center'>".$ip_info."</td>
					<td width='100px'align='center'>".date('Y-m-d H:i:s',$rs->insert_time)."</td>
					<td width='100px'align='center'>".date('Y-m-d H:i:s',$rs->update_time)."</td>
					<td width='100px'align='center'>
						<input type='button' class='buttonS bGreen mt3' value='編集' onclick=\"editInfo('".$rs->shop_id."')\"/><br>
			";
		if($role==1||$role==3){
			echo "<input type='button' class='buttonS bRed mt3' value='削除' onclick=\"var ret=confirm('情報を削除します。よろしいですか？');if(ret)deleteInfo('" . $rs->shop_id . "');\"/><br>
			";
		}
		echo"
						<input type='button' class='buttonS bBlue mt3' value='視聴ログ' onclick=\"logInfo('".$rs->shop_id."')\"/>
					</td>
					</tr>
                </table>
		";
		$i++;
	}
	echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysqli_close($db);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
<script language="javascript" type="text/javascript">
	function addInfo() {
		var pageurl="m_shop_add.php";
		window.location.href=pageurl;
	}
	function editInfo(u_id) {
        var pageurl="m_shop_add.php?action=update&u_id="+u_id;
        window.location.href=pageurl;
    }
    function deleteInfo(u_id) {
        var pageurl="?action=delete&u_id="+u_id;
        window.location.href=pageurl;
    }
	function logInfo(u_id) {
		var pageurl="m_shop_show_log.php?action=search_all&u_id="+u_id;
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>