<?php
    //include
    require '../util/include.php';

	$role=$_SESSION['role'];
	$login_user=$_SESSION['login_user'];
	$loc_ip=get_real_ip();

    $action = $_GET['action'];

	$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){die("Cannot connect:" . mysqli_error($db));}
	$dns = mysqli_select_db($db,DB_NAME);
	if(!$dns){die("Cannot use db:" . mysqli_error($db));}
	mysqli_set_charset($db,'utf8');

	//Form
	//店舗名
	$s_shop_name=$_POST['s_shop_name'];
	if($s_shop_name==''&&$_GET['s_shop_name']!=''){$s_shop_name=$_GET['s_shop_name'];}
	//支店名
	$s_shop_name_b=$_POST['s_shop_name_b'];
	if($s_shop_name_b==''&&$_GET['s_shop_name_b']!=''){$s_shop_name_b=$_GET['s_shop_name_b'];}
	//管理番号
	$s_mg_no=$_POST['s_mg_no'];
	if($s_mg_no==''&&$_GET['s_mg_no']!=''){$s_mg_no=$_GET['s_mg_no'];}
	//IPアドレス
	$s_shop_ip=$_POST['s_shop_ip'];
	if($s_shop_ip==''&&$_GET['s_shop_ip']!=''){$s_shop_ip=$_GET['s_shop_ip'];}
	//店舗選択
	$s_shop_id=$_POST['s_shop_id'];
	if($s_shop_id==''&&$_GET['s_shop_id']!=''){$s_shop_id=$_GET['s_shop_id'];}
	//期間From
	$s_date_from=$_POST['s_date_from'];
	if($s_date_from==''&&$_GET['s_date_from']!=''){$s_date_from=$_GET['s_date_from'];}
	//期間To
	$s_date_to=$_POST['s_date_to'];
	if($s_date_to==''&&$_GET['s_date_to']!=''){$s_date_to=$_GET['s_date_to'];}


    if ($action=='search_all'||$action=='search_date'||$action=='search_all_book'||$action=='search_date_book'||$action=='search_shop_ip'){
		//SQL
//	if($s_shop_ip!='') {
//		//$sql_all = "SELECT a.*,sum(a.read_cnt) read_cnt,c.shop_name FROM mz_book_read_cnt a left join mz_shop_ip b on a.shop_id=b.shop_id left join mz_shop c on c.shop_id=a.shop_id WHERE FIND_IN_SET('".mysqli_real_escape_string($db,trim($s_shop_ip))."',b.shop_ip) ";
//		$sql_all = "SELECT a.*,sum(a.read_cnt) read_cnt,c.shop_name,c.mg_no FROM mz_book_read_cnt a left join mz_shop c on c.shop_id=a.shop_id WHERE 1";
//	}else{
//		$sql_all = "SELECT a.*,sum(a.read_cnt) read_cnt,c.shop_name,c.mg_no FROM mz_book_read_cnt a left join mz_shop c on c.shop_id=a.shop_id WHERE 1";
//	}
		$sql_all = "SELECT a.*,sum(a.read_cnt) read_cnt_sum,c.shop_name,c.shop_name_b,c.mg_no FROM mz_book_read_cnt a
				left join mz_shop c on c.shop_id=a.shop_id
				left join mz_shop_ip d on d.shop_id=a.shop_id
				WHERE 1";
		if($s_mg_no!='') {
			$sql_all .= " and c.mg_no  = '".mysqli_real_escape_string($db,$s_mg_no)."'";
		}
		if($s_shop_ip!='') {
			$sql_all .= " and d.shop_ip = '".mysqli_real_escape_string($db,$s_shop_ip)."'";
		}
		if($s_shop_name!='') {
			$sql_all .= " and c.shop_name like '%".mysqli_real_escape_string($db,$s_shop_name)."%'";
		}
		if($s_shop_name_b!='') {
			$sql_all .= " and c.shop_name_b like '%".mysqli_real_escape_string($db,$s_shop_name_b)."%'";
		}
		if($s_shop_id!='') {
			$sql_all .= " and a.shop_id =".$s_shop_id;
		}
		if($u_id!='') {
			$sql_all .= " and a.shop_id =".$u_id;
		}
		if($s_date_from!='') {
			$s_date_from=$s_date_from." 00:00:00";
			$sql_all .= " and unix_timestamp(concat(a.year,'-',a.month,'-',a.day)) >= ".strtotime($s_date_from);
		}
		if($s_date_to!='') {
			$s_date_to=$s_date_to." 23:59:59";
			$sql_all .= " and unix_timestamp(concat(a.year,'-',a.month,'-',a.day)) <= ".strtotime($s_date_to);
		}
		$s_date_from=str_replace(" 00:00:00","",$s_date_from);
		$s_date_to=str_replace(" 23:59:59","",$s_date_to);
		$result = mysqli_query($db,$sql_all) or die(mysqli_error($db));
		if(!$result){
			$rowCnt = -1;
		}
		$total=mysqli_num_rows($result);
		if($action=='search_date'){
			$sql_all .= " GROUP BY a.year,a.month,a.day";
		}
		if($action=='search_all_book'){
			$sql_all .= " GROUP BY a.book_id";
		}
		if($action=='search_date_book'){
			$sql_all .= " GROUP BY a.book_id,a.year,a.month,a.day";
		}
		if($action=='search_shop_ip'){
			$sql_all .= " GROUP BY a.shop_id,a.shop_ip";
		}
		if($action=='search_date_book'){
			$sql = sprintf("%s order by a.year desc,a.month desc,a.day desc,read_cnt_sum desc",$sql_all);
		}elseif($action=='search_shop_ip'){
			$sql = sprintf("%s order by c.shop_name,c.shop_name_b,read_cnt_sum desc",$sql_all);
		}else{
			$sql = sprintf("%s order by read_cnt_sum desc",$sql_all);
		}
		$result = mysqli_query($db,$sql);
		if(!$result){
			$rowCnt = -1;
		}
		$rowCnt=mysqli_num_rows($result);
		if ($rowCnt>0) {
			if($action=='search_shop_ip'){
				//CSVファイルHeader
				$csv_a = "期間";
				$csv_b = "店舗名";
				$csv_c = "支店名";
				$csv_d = "IPアドレス";
				$csv_e = "視聴数";

				$str_header = "";
				$str_header = $str_header . "" . $csv_a . "," . $csv_b . "," . $csv_c . "," . $csv_d . "," . $csv_e . "\n";
				if ($s_date_from != "" && $s_date_to == "") {
					$s_date_str = $s_date_from . "～";
				}
				if ($s_date_to != "" && $s_date_from == "") {
					$s_date_str = "～" . $s_date_to;
				}
				if ($s_date_from != "" && $s_date_to != "") {
					$s_date_str = $s_date_from . "～" . $s_date_to;
				}
			}else{
				//CSVファイルHeader
				$csv_a = "サマリ分類";
				$csv_b = "期間";
				$csv_c = "管理番号";
				$csv_d = "IPアドレス";
				$csv_e = "店舗名";
				$csv_h = "支店名";
				$csv_f = "作品名";
				$csv_g = "視聴数";

				$str_header = "";
				$str_header = $str_header . "" . $csv_a . "," . $csv_b . "," . $csv_c . "," . $csv_d . "," . $csv_e . "," . $csv_h . "," . $csv_f . "," . $csv_g. "\n";

				if($action=="search_all"){
					$cate_name="期間全体";
					$file_name_str="all";
					$book_name="全体";
				}
				if($action=="search_date"){
					$cate_name="期間日別";
					$file_name_str="date";
					$book_name="全体";
				}
				if($action=="search_all"||$action=="search_all_book") {
					if ($s_date_from != "" && $s_date_to == "") {
						$s_date_str = $s_date_from . "～";
					}
					if ($s_date_to != "" && $s_date_from == "") {
						$s_date_str = "～" . $s_date_to;
					}
					if ($s_date_from != "" && $s_date_to != "") {
						$s_date_str = $s_date_from . "～" . $s_date_to;
					}
				}
			}
			$i = 1;
			while ($rs = mysqli_fetch_object($result)) {
				if($action!="search_all"&&$action!="search_date"&&$s_shop_ip=="") {
					$value_ip = array();
					$ip_info = "";
					$sql_ip = "select shop_ip from mz_shop_ip WHERE shop_id=" . $rs->shop_id;
					$result_ip = mysqli_query($db, $sql_ip) or die(mysqli_error($db));
					if (!$result_ip) {
						$rowCnt = -1;
					}
					$j = 0;
					while ($rs_ip = mysqli_fetch_object($result_ip)) {
						$value_ip[$j] = $rs_ip->shop_ip;
						$j++;
					}
					$value_ip = implode(",", $value_ip);
					$ip_info = str_replace(",",";", $value_ip);
				}
				if($rs->read_cnt_sum==0){
					$show_read_cnt=0;
				}else{
					$show_read_cnt=$rs->read_cnt_sum;
				}
				if($action=='search_shop_ip'){
					$file_name_str="shop_ip";
					$csv_a = $s_date_str;
					$csv_b = $rs->shop_name;
					$csv_c = $rs->shop_name_b;
					$csv_d = $rs->shop_ip;
					$csv_e = $show_read_cnt;
					$str = $str . "" . $csv_a . "," . $csv_b . "," . $csv_c . "," . $csv_d . "," . $csv_e . "\n";
				}else{
					if($action=="search_all_book"){
						$cate_name="期間作品毎";
						$file_name_str="all_book";
						$book_name=$rs->book_name;
					}
					if($action=="search_date_book"){
						$cate_name="期間日別作品毎";
						$file_name_str="date_book";
						$book_name=$rs->book_name;
					}
					if($action=="search_date"||$action=="search_date_book") {
						$s_date_str=$rs->year.'-'.$rs->month.'-'.$rs->day;
					}
					if($action=="search_all"||$action=="search_date"){
						$list_mg_no="全体";
						$show_shop_name="全体";
						$show_shop_name_b="全体";
					}else{
						if(($role==1||$role==3)&&$u_id==""){
							//$list_mg_no=$s_mg_no;
							$list_mg_no=$rs->mg_no;
						}else{
							//$list_mg_no=$mg_no;
							$list_mg_no=$rs->mg_no;
						}
						$show_shop_name=$rs->shop_name;
						$show_shop_name_b=$rs->shop_name_b;
					}
					if($s_shop_ip==""){
						$show_shop_ip=$ip_info;
					}else{
						$show_shop_ip=$s_shop_ip;
					}
					$csv_a = $cate_name;
					$csv_b = $s_date_str;
					$csv_c = $rs->mg_no;
					$csv_d = $show_shop_ip;
					$csv_e = $rs->shop_name;
					$csv_h = $rs->shop_name_b;
					$csv_f = $book_name;
					$csv_g = $show_read_cnt;
					$str = $str . "" . $csv_a . "," . $csv_b . "," . $csv_c . "," . $csv_d . "," . $csv_e . "," . $csv_h . "," . $csv_f . "," . $csv_g . "\n";
				}

			}
			mysqli_close($db);

			//CSVファイル名
			$filename = 'emf_'.$file_name_str."_". date('Y-m-d H:i:s', time()). '.csv';

			//$str = iconv('utf-8','SJIS',$str);
			$str = $str_header . $str;
			$str = "\xEF\xBB\xBF" . $str; //BOM
			//出力処理
			export_csv($filename, $str);
		}
    }
?>