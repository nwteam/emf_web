<?php
	//include
	require '../util/include.php';
    session_start();
	$sub_title='ジャンル管理';
	$systime=date('Y-m-d H:i:s',time());

    $role=$_SESSION['role'];
    $login_user=$_SESSION['login_user'];

	$action = $_GET['action'];
//
//
//  $db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
//  if(!$db){
//      die("connot connect:" . mysqli_error($db));
//  }
//  $dns = mysqli_select_db($db,DB_NAME);
//  if(!$dns){
//      die("connot use db:" . mysqli_error($db));
//  }
//
//  mysqli_set_charset($db,'utf8');
//
//  //カテゴリプルダウンリスト取得
//  $sqlall = "select * from mz_category WHERE 1 and del_flg=0";
//  $result_list_cate = mysqli_query($db,$sqlall);
	
//	$sqlall = "select * from mz_book WHERE 1";
//  $result_list_book = mysqli_query($db,$sqlall);
//	while($rs=mysqli_fetch_object($result_list_book)){
//		$sql = sprintf("UPDATE mz_book_read_cnt SET read_cnt =%d WHERE book_id = '%s'",$rs->read_cnt,$rs->book_id);
//		$result = mysqli_query($db,$sql);
	
//	mysqli_close($db);

	//Delete
	if ($action=='delete'){
		$u_id = $_GET['u_id'];
		$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysqli_error($db));
		}
		$dns = mysqli_select_db($db,DB_NAME);
		if(!$dns){
			die("connot use db:" . mysqli_error($db));
		}
		mysqli_set_charset($db,'utf8');

		$sql = sprintf("delete from mz_category WHERE id = '%s'",$u_id);
		$result = mysqli_query($db,$sql);

		mysqli_close($db);
	}
	//Update
	if ($action=='update'){
		$u_id = $_GET['u_id'];
		$c_name = $_GET['c_name'];
		
		$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysqli_error($db));
		}
		$dns = mysqli_select_db($db,DB_NAME);
		if(!$dns){
			die("connot use db:" . mysqli_error($db));
		}
		mysqli_set_charset($db,'utf8');

		$sql = sprintf("UPDATE mz_category SET cat_name='%s' WHERE id = %d",$c_name,$u_id);
		$result = mysqli_query($db,$sql);

		mysqli_close($db);
	}

	//Search
	if ($action=='search'||$action=='update'||$action=='delete'){

		$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		mysqli_set_charset($db,'utf8');

		$page_size=1000;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;
		$s_cat_name=$_POST['s_cat_name'];
		if($s_cat_name==''){
			$s_cat_name=$_GET['s_cat_name'];			
		}
		$s_cat_id=$_POST['s_cat_id'];
		if($s_cat_id==''){
			$s_cat_id=$_GET['s_cat_id'];			
		}

		//All
		$sqlall = "select * from mz_category WHERE 1";

        if($s_cat_name!='') {
            $sqlall .= " and cat_name like '%".mysqli_real_escape_string($db,$s_cat_name)."%'";
        }
		if($s_cat_id!='') {
			$sqlall .= " and cat_id='".$s_cat_id."'";
		}

		$result = mysqli_query($db,$sqlall) or die(mysqli_error($db));

		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}
		$rowCntall=mysqli_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by id limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysqli_query($db,$sql);

		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}

		$rowCnt=mysqli_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			mysqli_close($db);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=s_cat_name='.$s_cat_name.'&s_cat_id='.$s_cat_id.'>トップページ</a>|<a href=?action=search&page='.($page-1).'&s_cat_name='.$s_cat_name.'&s_cat_id='.$s_cat_id.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'&s_cat_name='.$s_cat_name.'&s_cat_id='.$s_cat_id.'>次頁</a>|<a href=?action=search&page='.$page_count.'&s_cat_name='.$s_cat_name.'&s_cat_id='.$s_cat_id.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
    <label class='search_label w100'>ジャンルID</label>
		<input type='text' name='s_cat_id' id='s_cat_id' class='w200' value='<?php echo $s_cat_id;?>'/>
    <div style='clear:both;'></div>
    <label class='search_label w100'>ジャンル名称</label>
		<input type='text' name='s_cat_name' id='s_cat_name' class='w500' value='<?php echo $s_cat_name;?>'/>
    <input type='submit' class='buttonS bGreen ml100' value='絞り込み'/>
    <input type='button' class='buttonS bGreen ml20' value='新規登録' onclick='addInfo();'/>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='60px'>連番</th>
              <th width='60px'>ジャンルID</th>
              <th width='240px'>ジャンル名</th>
              <th width='60px'>操作</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysqli_fetch_object($result))
	{
	  echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<input type='hidden' name='u_id_".$i."' id='u_id_".$i."' value=".$rs->id.">
					<td width='60px'align='center'>".$rs->id."</td>
					<td width='60px'align='center'>".$rs->cat_id."</td>					
					<td width='240px'><input type='text' style='font-size:16px;width:97%;' name='i_category_name_".$i."' id='i_category_name_".$i."'value=".$rs->cat_name."></td>
					<td width='60px'align='center'><input type='button' class='buttonS bGreen' value='更新' onclick=\"updateChange('".$i."')\"/></td>
                </tr>
             </table>
		    ";
        
		$i++;
	}
	echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysqli_close($db);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
<script language="javascript" type="text/javascript">
	function addInfo() {
		var pageurl="m_category_add.php";
		window.location.href=pageurl;
	}
    function deleteInfo(u_id) {
        var pageurl="?action=delete&u_id="+u_id;
        window.location.href=pageurl;
    }
	function updateChange(row) {
		var u_id_item='u_id_'+row;
		var c_name_item='i_category_name_'+row;
        
		var u_id=document.getElementById(u_id_item).value;
		var c_name=document.getElementById(c_name_item).value;

		var pageurl="?action=update&u_id="+u_id+"&c_name="+c_name;
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>