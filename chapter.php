<?php
require 'util/include.php';
include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック

$b_id = $_GET['b_id']; //ブックID

//ブック情報取得
$sql_book = "select * from mz_book where book_id = '".$b_id ."'";
$r_book = mysqli_query($link, $sql_book);
$row_book = mysqli_fetch_array($r_book);
$bookOverview = $row_book['book_overview'];//概要
$b_name = $row_book['book_name'];//ブック名
$b_auth = $row_book['book_auth'];//著作者
$cover_img_path = COMIC_PATH.$row_book['cover_img_path'];//Cover
$cateIds = explode(",", $row_book['cat_id']);

//カテゴリプルダウンリスト取得
include("common/common_category_list.php");

//チャプター情報の取得
$sql_chapter = "select * from mz_chapter where book_id = '".$b_id."' and insert_time <= ". time()." order by chapter_id";
$r_chapter = mysqli_query($link,$sql_chapter);
$chapters = array();
while ($row_chapter = mysqli_fetch_array($r_chapter)) {
    $chapters[] = $row_chapter;
}
//Update
if ($action == 'readCnt') {
    $c_id = $_GET['c_id']; //チャプターID
    $i = $_GET['i']; //話No.
    $year = date('Y', time()); //現在の年
    $month = date('m', time()); //現在の月

    $sql = sprintf("select count(*) cnt from mz_chapter_read_cnt where book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $b_id, $c_id, $year, $month);
    $result_cc = mysqli_query($link, $sql);
    $rs_cc = mysqli_fetch_object($result_cc);
    $cnt = $rs_cc->cnt;
    if ($cnt == '0') {
        $sql = sprintf("insert into mz_chapter_read_cnt (book_id,chapter_id,year,month,read_cnt) values 
						('%s',%d,'%s','%s',1)"
            , $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
    } else {
        $sql = sprintf("select read_cnt from mz_chapter_read_cnt where book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
        $rs = mysqli_fetch_object($result);
        $read_cnt = ($rs->read_cnt) + 1;

        $sql = sprintf("UPDATE mz_chapter_read_cnt SET read_cnt=%d WHERE book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $read_cnt, $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
    }
    $r_chapter_read = mysqli_query($link, "select * from mz_chapter where chapter_id = '" . $c_id . "' and book_id = '" . $b_id . "'");
    $row_chapter_read = mysqli_fetch_array($r_chapter_read);
    $folder = $row_chapter_read['page_folder_name'];
    $total_page = $row_chapter_read['total_page'];
    //$url = "chapter_read.php?b_id=" . $b_id . "&c_id=" . $c_id . "&b_name=" . urlencode($b_name) . "&b_auth=" . urlencode($b_auth);
    $url="read.php?page=1&folder=".$folder."&total_page=".$total_page."&c_id=".$c_id."&b_id=".$b_id."&b_name=".urlencode($b_name)."&b_auth=".urlencode($b_auth);
    redirect($url);
}


?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top" class="select">
<header id="header">
    <?php include("common/common_header.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
    <section id="section-title">
        <div class="inner clearfix">
            <p class="image"><span class="image" style="background: url(<?php echo $cover_img_path ?>) 50% 50% no-repeat #fff;  background-size:250px auto;"></span></p>
            <div class="text">
                <h2><?php echo $b_name ?><span class="name"><?php echo $b_auth ?></span></h2>
                <p><?php echo nl2br(htmlspecialchars($bookOverview)) ?></p>
                <ul class="category clearfix">
                    <?php
                    /* ジャンル */
                    if (!empty($cateIds)) {
                        foreach ($cateIds as $cateId) {
                            ?>
                            <li>
                                <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </section>
    <!-- /#section-title-->
    <div class="inner clearfix">
        <div class="contents">
            <ul class="select-list clearfix">
                <?php $i = 1; ?>
                <?php foreach ($chapters as $chapter) { ?>
                    <li><a href="#" onclick="readCnt('<?php echo $chapter['book_id'] ?>', '<?php echo $chapter['chapter_id'] ?>', '<?php echo $b_name ?>', '<?php echo $i ?>', '<?php echo $b_auth ?>')" >
                            <span class="image" style="background: url(<?php echo COMIC_PATH ?><?php echo $chapter['cover_img_path'] ?>) 50% 50% no-repeat #fff;  background-size:184px auto;"></span>
                            <!--<span class="title new">-->
                            <span class="title">
                                <?php echo $chapter['chapter_name'] ?>
                                <span class="story">
                                    <?php
                                        if ($chapter['sub_title']!=""){
                                            echo $chapter['sub_title'];
                                        }else{
                                            echo "第".$i."話";
                                        }

                                    ?>
                                </span>
                            </span>
                        </a>
                    </li>
                    <?php $i++; ?>
                <?php } ?>
            </ul>
        </div>
        <!-- /.contents-->
    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
<script>
    function readCnt(b_id, c_id, b_name, i, b_auth) {
        //$('#chapter_btn' + c_id).css({'background-color': '#CCC'});
        var pageurl = "chapter.php?action=readCnt&b_id=" + b_id + "&c_id=" + c_id + "&b_name=" + b_name + "&i=" + i + "&b_auth=" + b_auth;
        window.location.href = pageurl;
    }
</script>
</body>
<?php include_once("analyticstracking.php") ?>
</html>

