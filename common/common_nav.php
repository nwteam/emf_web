<nav class="gnav <?php echo $nav_no?>">
    <ul class="clearfix">
        <li id="nav1"><a href="new_book.php"><span>新着マンガ</span></a></li>
        <li id="nav2"><a href="update_book.php"><span>更新予定</span></a></li>
        <li data-pulldown="nav3" id="nav3"><a href="category.php"><span>ジャンル一覧</span></a></li>
        <li data-pulldown="nav4" id="nav4"><a href="rank.php?action=week"><span>ランキング</span></a></li>
    </ul>
</nav>
<section id="section-pulldown">
    <div class="box-pulldown nav1"> </div>
    <div class="box-pulldown nav2"> </div>
    <div class="box-pulldown nav3">
        <div class="inner">
            <p class="h-pulldown">ジャンル一覧</p>
            <ul class="clearfix">
                <li><a href="search.php?action=category&cat_id=CT029">SM</a></li>
                <li><a href="search.php?action=category&cat_id=CT002">百合</a></li>
                <li><a href="search.php?action=category&cat_id=CT013">近親相姦</a></li>
                <li><a href="search.php?action=category&cat_id=CT034">学園</a></li>
                <li><a href="search.php?action=category&cat_id=CT003">ロリ</a></li>
                <li><a href="search.php?action=category&cat_id=CT047">ショタ</a></li>
                <li><a href="search.php?action=category&cat_id=CT012">NTR</a></li>
                <li><a href="search.php?action=category&cat_id=CT001">BL</a></li>
                <li><a href="search.php?action=category&cat_id=CT010">強姦</a></li>
                <li><a href="search.php?action=category&cat_id=CT004">巨乳</a></li>
                <li><a href="search.php?action=category&cat_id=CT007">ビッチ</a></li>
                <li><a href="search.php?action=category&cat_id=CT023">ハーレム</a></li>
                <li class="clear"><a href="category_all.php">全てのジャンル</a></li>
            </ul>
        </div>
    </div>
    <div class="box-pulldown nav4">
        <div class="inner">
            <p class="h-pulldown">ランキング</p>
            <ul class="clearfix">
                <li><a href="rank.php?action=week">週間ランキング</a></li>
                <li><a href="rank.php?action=month">月間ランキング</a></li>
            </ul>
        </div>
    </div>
</section>
<!-- /#section-pulldown-->