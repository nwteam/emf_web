<?php

if ($action == 'readCnt') {
$b_id = $_GET['b_id']; //ブックID
$b_name = $_GET['b_name']; //ブック名
$b_auth = $_GET['b_auth']; //著者名
$year = date('Y', time());
$month = date('m', time());
$day = date('d', time());
$week = date('W', time());
$loc_ip=get_real_ip();
//ブック情報取得
$sql_book = "select * from mz_book where book_id = '".$b_id ."'";
$r_book = mysqli_query($link, $sql_book);
$row_book = mysqli_fetch_array($r_book);
$bookOverview = $row_book['book_overview'];//概要
$b_name = $row_book['book_name'];//ブック名
$b_auth = $row_book['book_auth'];//著作者
$cover_img_path = $row_book['cover_img_path'];
$cateIds = $row_book['cat_id'];

//閲覧数を取得
$sql = sprintf("select count(*) cnt from mz_book_read_cnt where book_id = '%s' and year='%s' and month='%s'and day='%s'and week='%s'and shop_id=%d and shop_ip='%s'",$b_id,$year,$month,$day,$week,$shop_id,$loc_ip);

$result_bc = mysqli_query($link, $sql);
//$sqlTraceMessage .= "{$sql}\r\n";
$rs_bc = mysqli_fetch_object($result_bc);
$cnt = $rs_bc->cnt;

//$nowMtime = microtime(true) - $startMTime;
//$traceMessage .= "[{$nowMtime}]sec 月間の閲覧数を取得\r\n";

if ($cnt == '0') {
//閲覧数を新規登録
$sql = sprintf("insert into mz_book_read_cnt (shop_id,shop_ip,book_id,year,month,day,week,book_name,book_auth,cover_img_path,cat_id,read_cnt,c_read_cnt) values (%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',1,0)",$shop_id,$loc_ip,$b_id,$year,$month,$day,$week,$b_name,$b_auth,$cover_img_path,$cateIds);
$result = mysqli_query($link, $sql);
//$sqlTraceMessage .= "{$sql}\r\n";
//$nowMtime = microtime(true) - $startMTime;
//$traceMessage .= "[{$nowMtime}]msec 月間閲覧数を新規登録\r\n";

} else {
//月間閲覧数を更新
$sql = sprintf("select read_cnt from mz_book_read_cnt where book_id = '%s' and year='%s' and month='%s'and week='%s'and shop_id=%d and shop_ip='%s'",$b_id,$year,$month,$week,$shop_id,$loc_ip);
$result = mysqli_query($link, $sql);
//$sqlTraceMessage .= "{$sql}\r\n";
$rs = mysqli_fetch_object($result);
$read_cnt = ($rs->read_cnt) + 1;
$sql = sprintf("UPDATE mz_book_read_cnt SET read_cnt=%d WHERE book_id = '%s' and year='%s' and month='%s' and week='%s'and shop_id=%d and shop_ip='%s'", $read_cnt, $b_id, $year, $month,$week,$shop_id,$loc_ip);
$result = mysqli_query($link, $sql);
//$sqlTraceMessage .= "{$sql}\r\n";
//$nowMtime = microtime(true) - $startMTime;
//$traceMessage .= "[{$nowMtime}]sec 月間閲覧数を更新\r\n";
}
$url = "chapter.php?b_id=" . $b_id . "&b_name=" . urlencode($b_name) . "&b_auth=" . urlencode($b_auth);
redirect($url);
}

?>