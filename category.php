<?php
require_once 'util/include.php';
include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック
$nav_no="nav3";
//新着ブック情報取得
$sql_book = "select a.chapter_id,a.page_from,a.page_to,b.* from mz_top_book a,mz_book b where a.book_id=b.book_id";
$r_book = mysqli_query($link, $sql_book);
$row_new_book = mysqli_fetch_array($r_book);
$new_book_id = $row_new_book['book_id'];//ブック名
$new_book_name = $row_new_book['book_name'];//ブック名
$new_book_auth = $row_new_book['book_auth'];//著作者
$new_book_cover = COMIC_PATH.$row_new_book['cover_img_path'];//Cover
$new_book_url = "index.php?action=readCnt&b_id=" . $new_book_id . "&b_name=" . urldecode($new_book_name) . "&b_auth=" . urlencode($new_book_auth);
$chapter_id=$row_new_book['chapter_id'];
$page_from=$row_new_book['page_from'];
$page_to=$row_new_book['page_to'];
$cateIds = explode(",", $row_new_book['cat_id']);
$show_pages = array();
for($i=$page_from;$i<=$page_from+5;$i++){
    $img_name=str_pad($i,3,'0',STR_PAD_LEFT).".jpg";
    $show_pages[$i]=str_replace("title/cover.jpg","",$new_book_cover).$chapter_id."/".$img_name;
}
//お勧め
$sql = "SELECT * FROM `mz_book` WHERE 1 and recommend_flg=1 order by id desc LIMIT 16";
$r_recommend_books = mysqli_query($link, $sql);
$recommend_books_arr = array();
while ($row_recommend = mysqli_fetch_array($r_recommend_books)) {
    $recommend_books_arr[] = $row_recommend;
}
//カテゴリプルダウンリスト取得
include("common/common_category_list.php");
//読み回数計数
include("common/common_book_read_cnt.php");
?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top">
<header id="header">
    <?php include("common/common_header.php") ?>
    <?php include("common/common_nav.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
    <div class="inner clearfix">
        <div class="contents">
            <h2><span class="h_category pink">人気ジャンル</span></h2>
            <ul class="category-list clearfix">
                <li><a href="search.php?action=category&cat_id=CT029" class="c_sm"><span>SM</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT002" class="c_lesbian"><span>百合</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT013" class="c_incest"><span>近親相姦</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT034" class="c_school"><span>学園</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT003" class="c_lori"><span>ロリ</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT047" class="c_pedophile"><span>ショタ</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT012" class="c_ntr"><span>NTR</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT001" class="c_bl"><span>BL</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT010" class="c_rape"><span>強姦</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT004" class="c_big-boobs"><span>巨乳</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT007" class="c_bitch"><span>ビッチ</span></a></li>
                <li><a href="search.php?action=category&cat_id=CT023" class="c_harlem"><span>ハーレム</span></a></li>
            </ul>
            <p class="btn-all"><a href="category_all.php"><span>すべてのジャンル</span></a></p>
            <h2><span class="h_good">おすすめ</span></h2>
            <ul class="good-list clearfix">
                <?php foreach ($recommend_books_arr as $recommend) { ?>
                    <?php $rec_book_url = "index.php?action=readCnt&b_id=" . $recommend['book_id'] . "&b_name=" . urldecode($recommend['book_name']) . "&b_auth=" . urlencode($recommend['book_auth']); ?>
                    <?php $rec_cover_img = COMIC_PATH . $recommend['cover_img_path']; ?>
                    <?php $rec_book_name = $recommend['book_name'] ?>
                    <?php $rec_book_auth = $recommend['book_auth'] ?>
                    <?php if(mb_strlen($rec_book_name)>18){$rec_book_name = mb_substr($rec_book_name,0,18)."...";} ?>
                    <?php if(mb_strlen($rec_book_auth)>9){$rec_book_auth = mb_substr($rec_book_auth,0,9)."...";} ?>
                    <li><a href="<?php echo $rec_book_url ?>"> <span class="image" style="background: url(<?php echo $rec_cover_img ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $rec_book_name ?><span class="name"><?php echo $rec_book_auth ?></span></span> </a>
                        <ul class='category clearfix'>
                            <?php
                            /* ジャンル */
                            $cateIds = explode(",", $recommend['cat_id']);
                            if (!empty($cateIds)) {
                                foreach ($cateIds as $cateId) {
                                    ?>
                                    <li>
                                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
            <p class="btn-etc"><a href="recommend_book.php"><span>その他の作品はこちら</span></a></p>
        </div>
        <!-- /.contents-->
        <?php include("common/common_side.php") ?>
    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
</body>
<?php include_once("analyticstracking.php") ?>
</html>
