<?php
require_once 'util/include.php';
$_SESSION['read_page_count'] = 0; //control.phpで参照＆更新する
$_SESSION['native_ad_no'] = null; //ad_data/native_x_2/index.phpで参照＆更新する

$startMTime = microtime(true);
$traceMessage = "-- Index Start --\r\n";
$sqlTraceMessage = "\r\n";

$detect = new Mobile_Detect();
$loc_ip=get_real_ip();
//echo $loc_ip;
if ($detect->isMobile() || $detect->isTablet()) {
    
} else {
    //redirect(LP_PATH . 'pc.html');
}

$sysdate = date('Y-m-d', time());
$systime = date('Y-m-d H:i:s', time());

$action = $_GET['action'];

//Update
if ($action == 'readCnt') {

    $b_id = $_GET['b_id']; //ブックID
    $b_name = $_GET['b_name']; //ブック名
    $b_auth = $_GET['b_auth']; //著者名
    $year = date('Y', time());
    $month = date('m', time());

    //月間の閲覧数を取得
    $sql = sprintf("select count(*) cnt from mz_book_read_cnt where book_id = '%s' and year='%s' and month='%s'", $b_id, $year, $month);
    $result_bc = mysqli_query($link, $sql);
    $sqlTraceMessage .= "{$sql}\r\n";
    $rs_bc = mysqli_fetch_object($result_bc);
    $cnt = $rs_bc->cnt;

    $nowMtime = microtime(true) - $startMTime;
    $traceMessage .= "[{$nowMtime}]sec 月間の閲覧数を取得\r\n";

    if ($cnt == '0') {

        //月間閲覧数を新規登録
        $sql = sprintf("insert into mz_book_read_cnt (book_id,year,month,read_cnt) values 
						('%s','%s','%s',1)"
            , $b_id, $year, $month);
        $result = mysqli_query($link, $sql);
        $sqlTraceMessage .= "{$sql}\r\n";

        $nowMtime = microtime(true) - $startMTime;
        $traceMessage .= "[{$nowMtime}]msec 月間閲覧数を新規登録\r\n";
    } else {

        //月間閲覧数を更新
        $sql = sprintf("select read_cnt from mz_book_read_cnt where book_id = '%s' and year='%s' and month='%s'", $b_id, $year, $month);
        $result = mysqli_query($link, $sql);
        $sqlTraceMessage .= "{$sql}\r\n";

        $rs = mysqli_fetch_object($result);
        $read_cnt = ($rs->read_cnt) + 1;
        $sql = sprintf("UPDATE mz_book_read_cnt SET read_cnt=%d WHERE book_id = '%s' and year='%s' and month='%s'", $read_cnt, $b_id, $year, $month);
        $result = mysqli_query($link, $sql);
        $sqlTraceMessage .= "{$sql}\r\n";

        $nowMtime = microtime(true) - $startMTime;
        $traceMessage .= "[{$nowMtime}]sec 月間閲覧数を更新\r\n";
    }
    $url = "chapter.php?b_id=" . $b_id . "&b_name=" . urlencode($b_name) . "&b_auth=" . urlencode($b_auth);
    redirect($url);
}

$nowMtime = microtime(true) - $startMTime;
$traceMessage .= "[{$nowMtime}]sec HTML出力開始\r\n";
?>
<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="「漫ZOKU」とは全巻無料でマンガが読める電子書籍サービスです。話題のwebマンガから、人気漫画雑誌で連載していた不朽の名作まで、様々な種類のマンガを無料でお読みいただけます。">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="apple-touch-fullscreen" content="no"/>
        <title>漫ZOKU</title>        
        <script type="text/javascript" src="js/road/ie_element.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/road/java.js"></script>
        <script src="js/modal.js"></script>
        <script src="js/cmn.js"></script>

        <script src="js/modernizr-transitions.js"></script>
        <script src="js/jquery.masonry.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/jquery.leanModal.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jquery.meerkat.1.3.min.js"></script>
        <script src="js/read/jquery.lazyload.js"></script>
        <!--<script src="js/jquery.flexslider-min.js"></script>-->
        <script src="js/flipsnap.js"></script>
        <script src="js/flipsnap_script.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('a[rel*=leanModal]').leanModal({top: 100, closeButton: ".modal_close"});
            });
        </script>        
        <link href="css/base.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/new.css" rel="stylesheet" type="text/css" media="all">
        <link rel="stylesheet" type="text/css" href="css/meerkat.css">
        <link href="css/flexslider.css" rel="stylesheet" media="all">
        <link rel="shortcut icon" href="images/favicon.ico">
        <style type="text/css"> 
            /* レクタングルボックス */
            #lean_overlay { position: fixed; z-index: 100; top: 0px; left: 0px; height: 100%; width: 100%; background: #000; display: none; }
            .modal_close {
                background: url("images/modal_close.png") repeat scroll 0 0 transparent;
                display: block;
                height: 14px;
                position: relative;
                top:-12px;
                right: 0px;
                bottom: 14px;
                width: 14px;
                z-index: 2;
            }
            #pageTop{
                background:#000;
                border-radius:5px;
                color:#FFF;
                padding:20px;
                position:fixed;
                bottom:50px;
                right:20px;
                z-index:1;
            }
            #pageTop:hover{
                background:#999;
            }
        </style>
        <script type="text/javascript">
            window.onload = function() {
                setTimeout(scrollTo, 100, 0, 1);
            };
        </script>
        <script type="text/javascript">
            $(function() {
                $('.meerkat').meerkat({
                    height: '50px',
                    width: '100%',
                    position: 'top',
                    close: '.close-meerkat',
                    dontShowAgain: '.dont-show',
                    animationIn: 'slide',
                    animationSpeed: 500,
                    removeCookie: '.reset'
                }).addClass('pos-bot');
            });
        </script>
        <script type="text/javascript">
            $(function() {
                var topBtn = $('#pageTop');
                topBtn.hide();
                $(window).scroll(function() {
                    if ($(this).scrollTop() > 400) {
                        topBtn.fadeIn();
                    } else {
                        topBtn.fadeOut();
                    }
                });
                topBtn.click(function() {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 500);
                    return false;
                });
            });
        </script>
        <script>
            //アクセスロギング＆別ウィンドウ表示
            function log_and_redirect(redirect_url, log_url, log_data) {
                $.get(log_url, {title: log_data});
                window.open(redirect_url, '_blank');
            }
        </script>
    </head>
    <body>
        <a id="pageTop" href="#page_top">▲</a>
        <div id="page_top" style="margin: 0 auto; height:50px; background:#666;">&nbsp;</div>
        <header class="clearfix">
            <h1><a href="<?php echo LP_PATH ?>"><img src="images/logo.png" alt="漫ZOKU"/></a></h1>
            <h2>
                <span style="font-size:10px;line-height:1.5em;color:#EFEFEF;">
                    毎日新着更新！<br>
                    TL・BL・成年漫画<br>
                </span>
                <b style="font-size:10px;line-height:1.5em;">無料で読み放題！</b>
            </h2>
            <p class="button-toggle">HELP</p>
            <div class="menu" style="display:none;">
                <ul>
                    <li><a href="about.html">「漫ZOKU」ってなに？</a></li>
                    <li><a href="policy.html">利用規約</a></li>
                    <li><a href="faq.html">よくある質問</a></li>
                    <li><a href="https://www.mangazokuzoku.com/web_push/">お知らせの通知</a></li>
                    <li><a href="mailto:support@mangazokuzoku.com?subject=お問い合わせ">お問い合わせ</a></li>
                </ul>
            </div>
        </header>

        <!-- ヘッダー広告 -->
        <?php require_once 'ad_data/mobile/header.php'; ?>
        <!--// ヘッダー広告 -->    

        <!-- 男性向け -->
        <?php require_once 'ad_data/mobile/for_man.php'; ?>
        <!--// 男性向け -->

        <!-- 女性向け -->
        <?php require_once 'ad_data/mobile/for_woman.php'; ?>
        <!--// 女性向け -->

        <!-- ランキング広告 -->
        <?php //include_once("ad_data/" . DIRECT_AD . "/ranking/index.php"); ?>
        <!--// ランキング広告 -->

        <!-- 検索フォーム -->
        <section id="search">
            <form method="get" class="clearfix" action="search.php" >
                <input type="hidden" name="action" value="list">
                <input type="search" data-mini="true" data-clear-btn-text="入力クリア" value="<?php echo $searchBox; ?>" placeholder=""name="searchBox" id="searchBox">
                <input type="submit" id="send" value="検索">
            </form>
        </section>
        <!--// 検索フォーム -->

        <!-- フッターメニュー -->
        <nav>
            <ul class="clearfix">
                <li id="nav01"><a href="rank.php"><img  style="min-width: 160px;width:50%;max-height:50px;height:auto;"src='images/rank_pink.png'></a></li>
                <li id="nav02"><a href="category_list.php"><img  style="min-width: 160px;width:50%;max-height:50px;height:auto;"src='images/genre_pink.png'></a></li>
            </ul>
        </nav>
        <!--// フッターメニュー -->

        <!-- テロップ -->
        <!--
        <a href="https://www.mangazokuzoku.com/web_push/">
            <marquee behavior="scroll" scrolldelay="200" scrollamount="6" style="background:#000;color:#FFF;padding:10px;font-size:15px;">
                ★★★ お知らせ通知を始めました！今すぐここをクリック！ ★★★
            </marquee>
        </a>
        -->
        <!--// テロップ -->

        <!-- 新着マンガ -->
        <?php require_once 'new.php'; ?>
        <!--// 新着マンガ -->

        <!--  Code Start -->
        <script type="text/javascript" charset="utf-8" src="http://smanavi.net/user/ranking/1321964506.js">
        </script>
        <!--  Code End -->

        <!-- ランダム広告 -->
        <?php //include_once("ad_data/" . DIRECT_AD . "/banner_x_5/index.php"); ?>
        <!--// ランダム広告 -->

        <section class="mangaList">
            <h2>マンガ一覧</h2>
            <div style="margin-left:5%;">
                <div id="Image_ViewArea" class="container" ></div>
            </div>
        </section>

        <!-- ポップアップ広告 -->
        <?php require_once 'ad_data/mobile/popup.php'; ?>
        <!--// ポップアップ広告 -->

        <footer>
            <p><small>&#169; 漫ZOKU</small></p>
        </footer>
        <script>
            $(function() {
                $(".menu").css("display", "none");
                $(".button-toggle").on("click", function() {
                    $(".menu").slideToggle();
                });
            });
        </script>
        <script>
            $(function() {
                var $container = $('.container');
                $(function() {
                    $('.container').masonry({
                        itemSelector: '.book',
                        isAnimated: !Modernizr.csstransitions,
                        isFitWidth: true,
                        isResizable: true,
                        gutterWidth: 12
                    });
                });
            });
        </script>
    </body>
    <?php include_once("analyticstracking.php") ?>
</html>

<?php
$nowMtime = microtime(true) - $startMTime;
$traceMessage .= "[{$nowMtime}]sec HTML出力終了\r\n";
manlog($traceMessage, "debug");
manlog($sqlTraceMessage, "sql");
?>
