<?php
require_once 'util/include.php';
//$_SESSION['read_page_count'] = 0; //control.phpで参照＆更新する
//$_SESSION['native_ad_no'] = null; //ad_data/native_x_2/index.phpで参照＆更新する
//
//$startMTime = microtime(true);
//$traceMessage = "-- Index Start --\r\n";
//$sqlTraceMessage = "\r\n";
include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック
$nav_no="";
$month = date('m', time());
$week = date('W', time());
//新着ブック情報取得
$sql_book = "select a.chapter_id,a.page_from,a.page_to,a.top_cover_1,a.top_cover_2,b.* from mz_top_book a,mz_book b where a.book_id=b.book_id";
$r_book = mysqli_query($link, $sql_book);
$row_new_book = mysqli_fetch_array($r_book);
$new_book_id = $row_new_book['book_id'];//ブック名
$new_book_name = $row_new_book['book_name'];//ブック名
$new_book_auth = $row_new_book['book_auth'];//著作者
if(mb_strlen($new_book_name)>18){$new_book_name = mb_substr($new_book_name,0,18)."...";}
if(mb_strlen($new_book_auth)>9){$new_book_auth = mb_substr($new_book_auth,0,11)."...";}
$new_book_cover = COMIC_PATH.$row_new_book['cover_img_path'];//Cover
$new_book_url = "index.php?action=readCnt&b_id=" . $new_book_id . "&b_name=" . urldecode($new_book_name) . "&b_auth=" . urlencode($new_book_auth);
$chapter_id=$row_new_book['chapter_id'];
$page_from=$row_new_book['page_from'];
$page_to=$row_new_book['page_to'];
$top_cover_1=$row_new_book['top_cover_1'];
$top_cover_1_no=$row_new_book['top_cover_1'];
$img_name=str_pad($top_cover_1,3,'0',STR_PAD_LEFT).".jpg";
$top_cover_1=str_replace("title/cover.jpg","",$new_book_cover).$chapter_id."/".$img_name;
$top_cover_2=$row_new_book['top_cover_2'];
$top_cover_2_no=$row_new_book['top_cover_2'];
$img_name=str_pad($top_cover_2,3,'0',STR_PAD_LEFT).".jpg";
$top_cover_2=str_replace("title/cover.jpg","",$new_book_cover).$chapter_id."/".$img_name;
$cateIds = explode(",", $row_new_book['cat_id']);
$show_pages = array();
for($i=$page_from;$i<=$page_from+5;$i++){
  $img_name=str_pad($i,3,'0',STR_PAD_LEFT).".jpg";
  $show_pages[$i]=str_replace("title/cover.jpg","",$new_book_cover).$chapter_id."/".$img_name;
}
$folder=str_replace(COMIC_PATH,"",$new_book_cover);
$folder=str_replace("title/cover.jpg","",$folder);
$page_dir = "{$folder}{$chapter_id}";
//echo $page_dir;
$filesnames = array();
$tempArray = getS3FileList($page_dir);
//print_r($tempArray);
$tempArray = array_reverse($tempArray);
$total_page = 0;
foreach ($tempArray as $name) {
  if (($name != ".") AND ( $name != "..")) {
    $filesnames[] = COMIC_PATH."{$folder}{$chapter_id}/{$name}";
    $total_page=$total_page+1;
  }
}
$total_f=sizeof($filesnames);
//echo $filesnames[$total_f-$page_from]."<br>";
//echo $filesnames[$total_f-$page_to]."<br>";
$top_cover_1= $filesnames[$total_f-$top_cover_1_no];
$top_cover_2= $filesnames[$total_f-$top_cover_2_no];
//echo $readPage;
//カテゴリプルダウンリスト取得
include("common/common_category_list.php");
//週間Rank
$sql = "SELECT * FROM `mz_book_read_cnt` WHERE 1 and week='".$week."' group by `book_id`,`year`,`week` order by sum(read_cnt) desc  LIMIT 10";
//$sql_echo = "SELECT * FROM `mz_book_read_cnt` WHERE 1 and week='".$week."' group by `book_id`,`year`,`month`,`week` order by sum(read_cnt) desc  LIMIT 10";
$r_new_books = mysqli_query($link, $sql);
$newChapters = array();
while ($row = mysqli_fetch_array($r_new_books)) {
  $newChapters[] = $row;
}
//お勧め
$sql = "SELECT * FROM `mz_book` WHERE 1 and recommend_flg=1 order by id desc LIMIT 8";
$r_recommend_books = mysqli_query($link, $sql);
$recommend_books_arr = array();
while ($row_recommend = mysqli_fetch_array($r_recommend_books)) {
  $recommend_books_arr[] = $row_recommend;
}
//読み回数計数
include("common/common_book_read_cnt.php");
//$nowMtime = microtime(true) - $startMTime;
//$traceMessage .= "[{$nowMtime}]sec HTML出力開始\r\n";
?>
<!doctype html>
<html lang="ja">
<head>
  <?php include("common/common_head.php") ?>
</head>
<body id="top">
<header id="header">
  <?php include("common/common_header.php") ?>
  <?php include("common/common_nav.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
  <section id="section-mainimage">
    <div class="inner">
      <h1><img src="images/img-main.png" alt=""></h1>
    </div>
  </section>
  <!-- /#section-mainimage-->
  <div class="inner clearfix">
    <div class="contents">
      <h2><span class="h_new pink">新着マンガ</span></h2>
      <section id="section-slider">
          <div class="image">
            <p><img src="<?php echo $top_cover_1 ?>" alt=""><img src="<?php echo $top_cover_2 ?>" alt=""></p>
          </div>
        <div class="list">
        <a href="<?php echo $new_book_url ?>">
          <span class="image" style="background: url(<?php echo $new_book_cover ?>) 50% 50% no-repeat #fff;  background-size:180px auto;"></span>
          <span class="title"><?php echo $new_book_name ?>
            <span class="name"><?php echo $new_book_auth ?></span>
          </span>
        </a>
          <ul class="category clearfix">
              <?php
              /* ジャンル */
              if (!empty($cateIds)) {
                  foreach ($cateIds as $cateId) {
                      ?>
                      <li>
                          <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                      </li>
                      <?php
                  }
              }
              ?>
          </ul>
        </div>
        <div class="image-list clear clearfix">
          <ul>
            <?php for($i=0;$i<=5;$i++){?>
             <li><a href="<?php echo $new_book_url ?>"><img src="<?php echo $filesnames[$total_f-$page_from-$i] ?>" style="width: 120px;height: 160px;" alt=""></a></li>
            <?php }?>
          </ul>
        </div>
      </section>
      <!-- /#section-slider-->
      <p class="btn-etc"><a href="new_book.php"><span>その他の作品はこちら</span></a></p>
      <h2><span class="h_ranking gold">ランキング</span></h2>
      <ul class="ranking-list clearfix">
        <?php $i=1; ?>
        <?php foreach ($newChapters as $chapter) { ?>
          <?php $newPageUrl = "index.php?action=readCnt&b_id=" . $chapter['book_id'] . "&b_name=" . urldecode($chapter['book_name']) . "&b_auth=" . urlencode($chapter['book_auth']); ?>
          <?php $newImgUrl = COMIC_PATH . $chapter['cover_img_path']; ?>
          <?php if(mb_strlen($chapter['book_name'])>28){$newBookName = mb_substr($chapter['book_name'],0,28)."...";}else{$newBookName = $chapter['book_name'];} ?>
          <?php if(mb_strlen($chapter['book_auth'])>9){$newBookAuth = mb_substr($chapter['book_auth'],0,9)."...";}else{$newBookAuth = $chapter['book_auth'];} ?>
          <?php if($i<=10){$rank_no = "no".$i;}else{$rank_no = "";} ?>
          <li><a href="<?php echo $newPageUrl ?>"> <span class="image" style="background: url(<?php echo $newImgUrl ?>) 50% 50% no-repeat #fff;  background-size:140px auto;"><span class="rank <?php echo $rank_no;?>"></span></span> <span class="title"><?php echo $newBookName ?><span class="name"><?php echo $newBookAuth ?></span></span> </a>
            <ul class='category clearfix'>
              <?php
              /* ジャンル */
              $cateIds = explode(",", $chapter['cat_id']);
              if (!empty($cateIds)) {
                foreach ($cateIds as $cateId) {
                  ?>
                  <li>
                    <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                  </li>
                  <?php
                }
              }
              ?>
            </ul>
          </li>
          <?php $i++; ?>
        <?php } ?>
      </ul>
      <p class="btn-etc"><a href="rank.php?action=week"><span>その他の作品はこちら</span></a></p>
      <h2><span class="h_good bronze">おすすめ</span></h2>
      <ul class="good-list clearfix">
            <?php foreach ($recommend_books_arr as $recommend) { ?>
              <?php $rec_book_url = "index.php?action=readCnt&b_id=" . $recommend['book_id'] . "&b_name=" . urldecode($recommend['book_name']) . "&b_auth=" . urlencode($recommend['book_auth']); ?>
              <?php $rec_cover_img = COMIC_PATH . $recommend['cover_img_path']; ?>
              <?php $rec_book_name = $recommend['book_name'] ?>
              <?php $rec_book_auth = $recommend['book_auth'] ?>
              <?php if(mb_strlen($rec_book_name)>18){$rec_book_name = mb_substr($rec_book_name,0,18)."...";} ?>
              <?php if(mb_strlen($rec_book_auth)>9){$rec_book_auth = mb_substr($rec_book_auth,0,9)."...";} ?>
              <li><a href="<?php echo $rec_book_url ?>"> <span class="image" style="background: url(<?php echo $rec_cover_img ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $rec_book_name ?><span class="name"><?php echo $rec_book_auth ?></span></span> </a>
                <ul class='category clearfix'>
                  <?php
                  /* ジャンル */
                  $cateIds = explode(",", $recommend['cat_id']);
                  if (!empty($cateIds)) {
                    foreach ($cateIds as $cateId) {
                      ?>
                      <li>
                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                      </li>
                      <?php
                    }
                  }
                  ?>
                </ul>
              </li>
            <?php } ?>
      </ul>
      <p class="btn-etc"><a href="recommend_book.php"><span>その他の作品はこちら</span></a></p>
    </div>
    <!-- /.contents-->
    <?php include("common/common_side.php") ?>
  </div>
  <div class="inner clearfix">
    <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
  </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
</body>
<?php include_once("analyticstracking.php") ?>
</html>
<?php
//$nowMtime = microtime(true) - $startMTime;
//$traceMessage .= "[{$nowMtime}]sec HTML出力終了\r\n";
//manlog($traceMessage, "debug");
//manlog($sqlTraceMessage, "sql");
//echo $sql_echo;
?>
