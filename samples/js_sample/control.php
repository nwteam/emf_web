<?php
//１ページ当たりの表示画像件数
define(_PAGE_MAX,24);

//画像データ一覧の取得
$full_path = realpath(".");	
$img_ary = scandir($full_path."/image/",1);
$cnt = 1;
$p = 1;

if(!empty($img_ary)){

	//画像データデータが少ないので水増し！
	$img_ary = array_merge((array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary,(array)$img_ary);

    //$image_datas[ページNo(1〜)][表示No(0〜)]の取得
	foreach($img_ary as $val){
		if(ereg(".jpg|.JPEG",$val)){
			if($cnt <=_PAGE_MAX){
				$image_datas[$p][] = $val;
				$cnt++;
			}else{
				$p++;
				$cnt=1;
			}
		}
	}
}


/*
 * データをhtmlに書き換えています。
 * $_REQUEST['PAGE_NUM']はページ番号です。
 * phpでhtmlを生成していますが、javascritp部分でhtmlを生成してもOKです。
 * class「page_block」は1頁の高さを決定しているので、名前を変更する時は、JavaScriptファイルも変更してください。
 */
if(!empty($_REQUEST['PAGE_NUM']) && !empty($image_datas[$_REQUEST['PAGE_NUM']])){
	$html .= '<div class="page_block">';
	$html .= '<div class="page_title">';
	$html .= '<p>'.$_REQUEST['PAGE_NUM'].' ページ</p>';	
	$html .= '</div>';
	$html .= '<div class="page_viewarea">';	
	foreach($image_datas[$_REQUEST['PAGE_NUM']] as $val){
		$html .='<div class="data_block"><img src="./image/'.$val.'" alt="'.$val.'" ></div>';
	}
	$html .= '</div>';
	$html .= '</div>';
	if(!empty($html)){
		echo $html;
	}
}else{
	
}
?>
