<?php
require_once 'util/include.php';
include("common/common_var.php");//共通変数
//include("common/common_ip_chk.php");//IPチェック
$nav_no="";
?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top">
<header id="header">
    <?php include("common/common_header.php") ?>
    <?php include("common/common_nav.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
    <div class="inner clearfix">
        <div class="contents">
            <h1 class="company_title">会社概要</h1>
            <p class="company_content">社名　Streetwise co., ltd.</p>
            <p class="company_content">設立　2014年8月</p>
            <p class="company_content">所在地　東京都豊島区南池袋1-9-18</p>
            <p class="company_content">メールアドレス　<a href="mailto:info@streetwise.xyz?subject=お問い合わせ">info@streetwise.xyz</p>
        </div>
        <!-- /.contents-->
        <?php include("common/common_side.php") ?>
    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
</body>
<?php include_once("analyticstracking.php") ?>
</html>
