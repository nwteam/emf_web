<?php
require_once 'util/include.php';
include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック
$nav_no="";
//お勧め
$sql = "SELECT * FROM `mz_book` WHERE 1 and recommend_flg=1 order by id desc";
$r_recommend_books = mysqli_query($link, $sql);
$recommend_books_arr = array();
while ($row_recommend = mysqli_fetch_array($r_recommend_books)) {
    $recommend_books_arr[] = $row_recommend;
}
//カテゴリプルダウンリスト取得
include("common/common_category_list.php");
//読み回数計数
include("common/common_book_read_cnt.php");
?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top">
<header id="header">
    <?php include("common/common_header.php") ?>
    <?php include("common/common_nav.php") ?>
</header>
<!-- /#header-->
<section id="section-main">
    <div class="inner clearfix">
        <div class="contents">
            <h2><span class="h_good">おすすめ</span></h2>
            <ul class="new-list clearfix">
                <?php foreach ($recommend_books_arr as $recommend) { ?>
                    <?php $rec_book_url = "index.php?action=readCnt&b_id=" . $recommend['book_id'] . "&b_name=" . urldecode($recommend['book_name']) . "&b_auth=" . urlencode($recommend['book_auth']); ?>
                    <?php $rec_cover_img = COMIC_PATH . $recommend['cover_img_path']; ?>
                    <?php $rec_book_name = $recommend['book_name'] ?>
                    <?php $rec_book_auth = $recommend['book_auth'] ?>
                    <?php if(mb_strlen($rec_book_name)>18){$rec_book_name = mb_substr($rec_book_name,0,18)."...";} ?>
                    <?php if(mb_strlen($rec_book_auth)>9){$rec_book_auth = mb_substr($rec_book_auth,0,9)."...";} ?>
                    <li><a href="<?php echo $rec_book_url ?>"> <span class="image" style="background: url(<?php echo $rec_cover_img ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $rec_book_name ?><span class="name"><?php echo $rec_book_auth ?></span></span> </a>
                        <ul class='category clearfix'>
                            <?php
                            /* ジャンル */
                            $cateIds = explode(",", $recommend['cat_id']);
                            if (!empty($cateIds)) {
                                foreach ($cateIds as $cateId) {
                                    ?>
                                    <li>
                                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.contents-->
        <?php include("common/common_side.php") ?>
    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
</body>
<?php include_once("analyticstracking.php") ?>
</html>
