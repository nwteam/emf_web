
#インデックス
ALTER TABLE  `mz_book` ADD INDEX  `idx_update_time` (`update_time`);
ALTER TABLE  `mz_book` ADD UNIQUE  `unique_book_id` (`book_id`(11));

ALTER TABLE  `mz_chapter` ADD INDEX  `idx_book_id` (`book_id`(11));
ALTER TABLE  `mz_chapter` ADD INDEX  `idx_insert_time` (`insert_time`);

ALTER TABLE  `mz_category` ADD INDEX  `idx_del_flg` (`del_flg`);

#検索をひらがなカタカタ混じりでも可能にする
ALTER TABLE `mz_book` collate utf8_unicode_ci;