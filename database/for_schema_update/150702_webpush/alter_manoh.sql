-- -----------------------------------------------------
-- Table `mz_push_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_push_users` ;

CREATE TABLE IF NOT EXISTS `mz_push_users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` VARCHAR(255) NULL COMMENT 'Subscription ID',
  `endpoint` VARCHAR(255) NULL COMMENT 'Endpoint',
  `user_agent` VARCHAR(255) NULL COMMENT 'ﾕｰｻﾞｰｴｰｼﾞｪﾝﾄ',
  `created` DATETIME NULL,
  `modified` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `push_id_UNIQUE` (`subscription_id` ASC))
ENGINE = MyISAM
COMMENT = 'Webﾌﾟｯｼｭのﾕｰｻﾞ情報';


-- -----------------------------------------------------
-- Table `mz_push_queue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_push_queue` ;

CREATE TABLE IF NOT EXISTS `mz_push_queue` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` VARCHAR(255) NULL COMMENT 'Subscription ID',
  `action` TINYINT(3) NULL COMMENT '1:許可, 2:解除',
  `created` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
COMMENT = 'Webﾌﾟｯｼｭのｴﾝﾄﾘｷｭｰ';


-- -----------------------------------------------------
-- Table `mz_admin`
-- -----------------------------------------------------