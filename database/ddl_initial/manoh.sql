SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `mz_push_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_push_users` ;

CREATE TABLE IF NOT EXISTS `mz_push_users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` VARCHAR(255) NULL COMMENT 'Subscription ID',
  `endpoint` VARCHAR(255) NULL COMMENT 'Endpoint',
  `user_agent` VARCHAR(255) NULL COMMENT 'ﾕｰｻﾞｰｴｰｼﾞｪﾝﾄ',
  `endpoint_arn` VARCHAR(255) NULL COMMENT 'AWS EndpointArn',
  `subscription_arn` VARCHAR(255) NULL COMMENT 'AWS SubscriptionArn',
  `created` DATETIME NULL,
  `modified` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `push_id_UNIQUE` (`subscription_id` ASC))
ENGINE = MyISAM
COMMENT = 'Webﾌﾟｯｼｭのﾕｰｻﾞ情報';


-- -----------------------------------------------------
-- Table `mz_push_queue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_push_queue` ;

CREATE TABLE IF NOT EXISTS `mz_push_queue` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` VARCHAR(255) NULL COMMENT 'Subscription ID',
  `action` TINYINT(3) NULL COMMENT '1:許可, 2:解除',
  `endpoint_arn` VARCHAR(255) NULL COMMENT 'AWS EndpointArn',
  `subscription_arn` VARCHAR(255) NULL COMMENT 'AWS SubscriptionArn',
  `created` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
COMMENT = 'Webﾌﾟｯｼｭのｴﾝﾄﾘｷｭｰ';


-- -----------------------------------------------------
-- Table `mz_admin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_admin` ;

CREATE TABLE IF NOT EXISTS `mz_admin` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `u_id` VARCHAR(255) NOT NULL,
  `pwd` VARCHAR(255) NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `role` TINYINT(1) NOT NULL DEFAULT '1',
  `insert_time` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COMMENT = '管理ﾂｰﾙｱｶｳﾝﾄ';


-- -----------------------------------------------------
-- Table `mz_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_category` ;

CREATE TABLE IF NOT EXISTS `mz_category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `cat_id` VARCHAR(5) NOT NULL COMMENT 'ｶﾃｺﾞﾘID',
  `cat_name` VARCHAR(255) NOT NULL COMMENT 'ｶﾃｺﾞﾘ名',
  `del_flg` INT(1) NOT NULL DEFAULT '0' COMMENT '1: 論理削除',
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 60
DEFAULT CHARACTER SET = utf8
COMMENT = 'ｶﾃｺﾞﾘﾏｽﾀ';


-- -----------------------------------------------------
-- Table `mz_book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_book` ;

CREATE TABLE IF NOT EXISTS `mz_book` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sort_num` INT(5) NOT NULL DEFAULT '99999' COMMENT 'ｿｰﾄ順',
  `book_id` VARCHAR(11) NOT NULL COMMENT 'Book ID',
  `book_name` VARCHAR(255) NOT NULL COMMENT '本のﾀｲﾄﾙ',
  `book_overview` TEXT NOT NULL COMMENT '概要',
  `book_auth` VARCHAR(255) NOT NULL COMMENT '本の著者',
  `total_chap` INT(11) NOT NULL COMMENT '話数 (ﾁｬﾌﾟﾀｰ数)',
  `folder_name` VARCHAR(255) NOT NULL COMMENT 'ﾃﾞｰﾀﾌｫﾙﾀﾞ名',
  `cover_img_path` VARCHAR(255) NOT NULL COMMENT 'ｶﾊﾞｰ画像へのﾊﾟｽ',
  `read_cnt` INT(11) NOT NULL DEFAULT '0',
  `cat_id` VARCHAR(255) NOT NULL COMMENT 'ｶﾃｺﾞﾘID (複数はｶﾝﾏ区切)',
  `insert_time` INT(11) NOT NULL,
  `update_time` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_mz_book_mz_category1_idx` (`cat_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 30
DEFAULT CHARACTER SET = utf8
COMMENT = 'ﾌﾞｯｸﾏｽﾀ';


-- -----------------------------------------------------
-- Table `mz_book_id`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_book_id` ;

CREATE TABLE IF NOT EXISTS `mz_book_id` (
  `id` INT(11) NOT NULL AUTO_INCREMENT)
ENGINE = MyISAM
AUTO_INCREMENT = 92
DEFAULT CHARACTER SET = utf8
COMMENT = 'ﾌﾞｯｸID採番用ｶｳﾝﾀ';


-- -----------------------------------------------------
-- Table `mz_book_read_cnt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_book_read_cnt` ;

CREATE TABLE IF NOT EXISTS `mz_book_read_cnt` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `book_id` VARCHAR(255) NOT NULL COMMENT 'Book ID',
  `year` VARCHAR(4) NOT NULL COMMENT '年',
  `month` VARCHAR(2) NOT NULL COMMENT '月',
  `read_cnt` INT(11) NOT NULL COMMENT '閲覧回数',
  PRIMARY KEY (`id`),
  INDEX `fk_mz_book_read_cnt_mz_book1_idx` (`book_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 77
DEFAULT CHARACTER SET = utf8
COMMENT = '月間の閲覧回数ｶｳﾝﾀ(本ごと)';


-- -----------------------------------------------------
-- Table `mz_category_id`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_category_id` ;

CREATE TABLE IF NOT EXISTS `mz_category_id` (
  `id` INT(11) NOT NULL AUTO_INCREMENT)
ENGINE = MyISAM
AUTO_INCREMENT = 60
DEFAULT CHARACTER SET = utf8
COMMENT = 'ｶﾃｺﾞﾘID採番用ｶｳﾝﾀ';


-- -----------------------------------------------------
-- Table `mz_chapter`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_chapter` ;

CREATE TABLE IF NOT EXISTS `mz_chapter` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `book_id` VARCHAR(11) NOT NULL,
  `chapter_id` INT(11) NOT NULL,
  `chapter_name` VARCHAR(255) NOT NULL,
  `sub_title` VARCHAR(255) NOT NULL,
  `cover_img_path` VARCHAR(255) NOT NULL,
  `page_folder_name` VARCHAR(255) NOT NULL,
  `total_page` INT(11) NOT NULL,
  `read_cnt` INT(11) NOT NULL DEFAULT '0',
  `insert_time` INT(11) NOT NULL,
  `update_time` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_mz_chapter_mz_book_idx` (`book_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1403
DEFAULT CHARACTER SET = utf8
COMMENT = 'ﾁｬﾌﾟﾀｰﾏｽﾀ(話ごと)';


-- -----------------------------------------------------
-- Table `mz_chapter_read_cnt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_chapter_read_cnt` ;

CREATE TABLE IF NOT EXISTS `mz_chapter_read_cnt` (
  `id` INT(11) NOT NULL,
  `book_id` VARCHAR(255) NOT NULL,
  `chapter_id` INT(11) NOT NULL,
  `year` VARCHAR(4) NOT NULL,
  `month` VARCHAR(2) NOT NULL,
  `read_cnt` INT(11) NOT NULL,
  INDEX `fk_mz_chapter_read_cnt_mz_book1_idx` (`book_id` ASC),
  INDEX `fk_mz_chapter_read_cnt_mz_chapter1_idx` (`chapter_id` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COMMENT = '月間の閲覧回数ｶｳﾝﾀ(話ごと)';


-- -----------------------------------------------------
-- Table `mz_last_ad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_last_ad` ;

CREATE TABLE IF NOT EXISTS `mz_last_ad` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(256) NOT NULL COMMENT '未使用',
  `ad_img` VARCHAR(2000) NOT NULL COMMENT '未使用',
  `ad_text` VARCHAR(255) NOT NULL COMMENT 'ﾀｲﾄﾙ',
  `ad_url` VARCHAR(255) NOT NULL COMMENT 'ﾘﾝｸ先URL',
  `del_flg` INT(11) NOT NULL DEFAULT '0' COMMENT '1: 論理削除',
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COMMENT = '話の最終ﾍﾟｰｼﾞﾘﾝｸﾏｽﾀ';


-- -----------------------------------------------------
-- Table `mz_read_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_read_history` ;

CREATE TABLE IF NOT EXISTS `mz_read_history` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `login_id` VARCHAR(50) NOT NULL,
  `book_id` VARCHAR(11) NOT NULL,
  `chapter_id` VARCHAR(11) NOT NULL,
  `page_no` INT(11) NOT NULL,
  `insert_time` INT(11) NOT NULL,
  `update_time` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mz_setting`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_setting` ;

CREATE TABLE IF NOT EXISTS `mz_setting` (
  `ad_flg` INT(1) NOT NULL COMMENT '0:imobil 1:nend')
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COMMENT = '広告設定管理ﾏｽﾀ';


-- -----------------------------------------------------
-- Table `mz_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mz_user` ;

CREATE TABLE IF NOT EXISTS `mz_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `login_id` VARCHAR(50) NOT NULL,
  `pwd` VARCHAR(50) NOT NULL,
  `nick` VARCHAR(50) NOT NULL,
  `insert_time` INT(11) NOT NULL,
  `update_time` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_id` (`login_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
