<?php
require 'util/include.php';
include("common/common_var.php");//共通変数
include("common/common_ip_chk.php");//IPチェック

$b_id = $_GET['b_id']; //ブックID
$c_id = $_GET['c_id']; //チャプターID
$b_name = $_GET['b_name']; //ブック名

$sql = sprintf("select * from mz_last_ad where del_flg = 0 order by id");
$result_ad = mysqli_query($link, $sql);

//トータル話数取得
//$sql = sprintf("SELECT chapter_id FROM mz_chapter WHERE insert_time<'%s'", time());
$sql = sprintf("SELECT MAX(chapter_id) as total_chap FROM mz_chapter WHERE book_id='%s'", $b_id);
$result_total_chap = mysqli_query($link, $sql);
$total_chap_row = mysqli_fetch_array($result_total_chap);
$total_chap = $total_chap_row['total_chap'];

//次の話の配信日付を取得
$sql = sprintf("SELECT insert_time FROM mz_chapter WHERE book_id='%s' AND chapter_id=%d", $b_id, $c_id + 1);
$result_insert_time = mysqli_query($link, $sql);
$total_time_row = mysqli_fetch_array($result_insert_time);
$nextInsertTime = $total_time_row['insert_time'];

//作者情報取得
$sql = sprintf("SELECT * FROM mz_book WHERE book_id='%s'", $b_id);
$result_auth = mysqli_query($link, $sql);
$book_auth_row = mysqli_fetch_array($result_auth);
$book_auth = $book_auth_row['book_auth'];

//お勧め
$sql = "SELECT * FROM `mz_book` WHERE 1 and recommend_flg=1 order by id desc LIMIT 16";
$r_recommend_books = mysqli_query($link, $sql);
$recommend_books_arr = array();
while ($row_recommend = mysqli_fetch_array($r_recommend_books)) {
    $recommend_books_arr[] = $row_recommend;
}

$rands = rand(1, 10);
//Update
if ($action == 'readCnt') {
    $b_id = $_GET['b_id'];
    $c_id = $_GET['c_id'];
    $b_name = $_GET['b_name'];
    $i = $_GET['i'];
    $year = date('Y', time());
    $month = date('m', time());

    $sql = sprintf("select count(*) cnt from mz_chapter_read_cnt where book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $b_id, $c_id, $year, $month);
    $result_cc = mysqli_query($link, $sql);
    $rs_cc = mysqli_fetch_object($result_cc);
    $cnt = $rs_cc->cnt;

    if ($cnt == '0') {
        $sql = sprintf("insert into mz_chapter_read_cnt (book_id,chapter_id,year,month,read_cnt) values 
						('%s',%d,'%s','%s',1)"
            , $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
    } else {
        $sql = sprintf("select read_cnt from mz_chapter_read_cnt where book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
        $rs = mysqli_fetch_object($result);
        $read_cnt = ($rs->read_cnt) + 1;

        $sql = sprintf("UPDATE mz_chapter_read_cnt SET read_cnt=%d WHERE book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $read_cnt, $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
    }

    $r_chapter_read = mysqli_query($link, "select * from mz_chapter where chapter_id = '" . $c_id . "' and book_id = '" . $b_id . "'");
    $row_chapter_read = mysqli_fetch_array($r_chapter_read);
    $folder = $row_chapter_read['page_folder_name'];
    $total_page = $row_chapter_read['total_page'];
    //$url = "chapter_read.php?b_id=" . $b_id . "&c_id=" . $c_id . "&b_name=" . urlencode($b_name) . "&b_auth=" . urlencode($b_auth);
    $url="read.php?page=1&folder=".$folder."&total_page=".$total_page."&c_id=".$c_id."&b_id=".$b_id."&b_name=".urlencode($b_name)."&b_auth=".urlencode($b_auth);
    redirect($url);
}
//カテゴリプルダウンリスト取得
include("common/common_category_list.php");
?>
<!doctype html>
<html lang="ja">
<head>
    <?php include("common/common_head.php") ?>
</head>
<body id="top" class="last">
<header id="header">
    <?php include("common/common_header.php") ?>
</header>
<section id="section-main">
    <div class="inner clearfix" style="margin-top: 40px;">
        <div class="last-menu">
            <h2>最終話まで随時更新中！</h2>
            <p class="btn-last">
                <?php if ($c_id > 1) { ?>
                    <a href="#" onclick="readCnt('<?php echo $b_id; ?>', '<?php echo (int) $c_id - 1; ?>', '<?php echo $b_name; ?>', '1', '<?php echo $book_auth ?>')"  class="prev"><span>前の話へ</span></a>
                <?php } else { ?>
                    <a href="javascript:void(0);" class="prev no-hover" onclick="this.setAttribute('disabled','disabled')"><span>前の話へ</span></a>
                <?php } ?>
                <a href="chapter.php?b_id=<?php echo $b_id; ?>&b_name=<?php echo urlencode($b_name); ?>&b_auth=<?php echo urlencode($book_auth); ?>" class="etc"><span>他の話を読む</span></a>
                <?php if ($total_chap > $c_id) { ?>
                    <?php if ($nextInsertTime <= time()) { ?>
                        <a href="#" onclick="readCnt('<?php echo $b_id; ?>', '<?php echo (int) $c_id + 1; ?>', '<?php echo $b_name; ?>', '1', '<?php echo $book_auth ?>')" class="next"><span>次の話へ</span></a>
                    <?php } else { ?>
                        <a href="#" class="next"><span><?php echo date("m/d更新", $nextInsertTime) ?></span></a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="javascript:void(0);" class="next no-hover"  onclick="this.setAttribute('disabled','disabled')"><span>次の話へ</span></a>
                <?php } ?>
            </p>
        </div>
        <div class="contents">
            <h2>その他“おすすめ”はコチラ！</h2>
            <ul class="good-list clearfix">
                <?php foreach ($recommend_books_arr as $recommend) { ?>
                    <?php $rec_book_url = "index.php?action=readCnt&b_id=" . $recommend['book_id'] . "&b_name=" . urldecode($recommend['book_name']) . "&b_auth=" . urlencode($recommend['book_auth']); ?>
                    <?php $rec_cover_img = COMIC_PATH . $recommend['cover_img_path']; ?>
                    <?php $rec_book_name = $recommend['book_name'] ?>
                    <?php $rec_book_auth = $recommend['book_auth'] ?>
                    <?php if(mb_strlen($rec_book_name)>18){$rec_book_name = mb_substr($rec_book_name,0,18)."...";} ?>
                    <?php if(mb_strlen($rec_book_auth)>9){$rec_book_auth = mb_substr($rec_book_auth,0,9)."...";} ?>
                    <li><a href="<?php echo $rec_book_url ?>"> <span class="image" style="background: url(<?php echo $rec_cover_img ?>) 50% 50% no-repeat #fff;  background-size:178px auto;"></span> <span class="title"><?php echo $rec_book_name ?><span class="name"><?php echo $rec_book_auth ?></span></span> </a>
                        <ul class='category clearfix'>
                            <?php
                            /* ジャンル */
                            $cateIds = explode(",", $recommend['cat_id']);
                            if (!empty($cateIds)) {
                                foreach ($cateIds as $cateId) {
                                    ?>
                                    <li>
                                        <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.contents-->

    </div>
    <div class="inner clearfix">
        <p class="pagetop clearfix"><a href="#top"><img src="images/pagetop.png" alt="pagetop"></a></p>
    </div>
</section>
<!-- /#section-main-->
<?php include("common/common_footer.php") ?>
<script>
    function readCnt(b_id, c_id, b_name, i, b_auth) {
        var pageurl = "chapter.php?action=readCnt&b_id=" + b_id + "&c_id=" + c_id + "&b_name=" + b_name + "&i=" + i + "&b_auth=" + b_auth;
        window.location.href = pageurl;
    }
</script>
</body>
<?php include_once("analyticstracking.php") ?>
</html>