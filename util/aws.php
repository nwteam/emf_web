<?php

include_once dirname(dirname(__FILE__)) . '/web_push/AWS/aws.phar';

use Aws\S3\S3Client;
use Aws\Common\Enum\Region;

/**
 * ページリストファイル（comic/list/[タイトル].txt）から、該当の話の「ページ一覧」を取得する
 * 
 * @param  string  $fileDir  該当話データへのパス（例「comic/naburarezou/1」）
 *                           ※上記の場合には「1」フォルダに入っている *.jpg ファイルのみ取得する。
 * @return 結果（ページ一覧）
 */
function getS3FileList($fileDir) {

    $fileDirs = explode("/", $fileDir);
    list($baseDir, $title, $cId) = $fileDirs;

    $result = array();
    $result['Contents'] = array();

    $fname = dirname(dirname(__FILE__)) . "/comic/list/{$title}.txt";
    $fp = @fopen($fname, "r");
    if ($fp) {
        while (!feof($fp)) { // 読み込みデータが無くなるまでループ
            $tmp = trim(fgets($fp, 4096));

            if (!empty($tmp)) {
                if (strstr($tmp, "{$title}/{$cId}/") !== false) {
                    $result['Contents'][] = array("Key" => $tmp);
                }
            }
        }
        fclose($fp);
    }
    $fileList = array();

    foreach ($result['Contents'] as $object) {
        $keyArray = explode("/", $object['Key']);
        $fileName = $keyArray[sizeof($keyArray) - 1];
        if (strstr($fileName, '.jpg') !== false) {
            $fileList[] = $fileName;
        }
    }
    return $fileList;
}

/**
 * S3の中にある「該当タイトル」データから、ページリストファイル（comic/list/[タイトル].txt）を作成する
 * 
 * @param  string  タイトル（comic/[タイトル]、または[タイトル]）
 * @param  boolean S3から最新のファイル一覧を取得するか否か（true:取得する）
 * @return 結果（タイトルフォルダ内の全ファイル一覧）
 */
function setGetS3FileList($title, $makeFromS3 = true) {

    $fname = dirname(dirname(__FILE__)) . "/comic/list/{$title}.txt";
    $fileArray = array();

    if ($makeFromS3) {

        // キー、シークレットキー、リージョンを指定
        $client = S3Client::factory(array(
                'key' => S3_ACCESS_KEY,
                'secret' => S3_SECRET_ACCESS_KEY,
                'region' => Region::AP_NORTHEAST_1
        ));

        $page = 0;
        $marker = null;
        do {
            $page++;
            if ($page > 100) {//無限ループ防止
                break;
            }
            $result = $client->listObjects(array(
                'Bucket' => S3_BUCKET,
                'Delimiter' => "comic/",
                'Prefix' => "comic/" . trim($title) . "/",
                'Marker' => $marker,
                'MaxKeys' => 1000
            ));

            if (!empty($result)) {
                // ファイルの一覧にセット
                foreach ($result['Contents'] as $object) {
                    $fileArray[] = $object['Key'];
                }
            }

            if (!$result['IsTruncated']) {
                break;
            } else {
                $marker = $result['NextMarker'];
            }
        } while (1);

        if (sizeof($fileArray) > 0) {
            $fileData = implode("\n", $fileArray);

            $fp = @fopen($fname, "w");
            if ($fp) {
                $ret = @flock($fp, LOCK_EX);  // 排他ロック
                if ($ret == true) {
                    fputs($fp, $fileData);  // 書き込み
                    flock($fp, LOCK_UN);  // ロック解除
                }
                fclose($fp);
            }
        }
    } else {
        $fileData = @file_get_contents($fname);
        $fileArray = explode("\n", $fileData);
    }

    return $fileArray;
}

/**
 * タイトルフォルダ内の「話一覧」を取得
 * 
 * @param  string  $folderPath  パス（例「comic/[タイトル]」）
 * @param  boolean S3から最新のファイル一覧を取得するか否か（true:取得する）
 * @return 結果（話一覧の配列）
 */
function myScandir($folderPath, $makeFromS3 = true) {

    $paths = explode("/", $folderPath);
    $title = $paths[sizeof($paths) - 1];
    $chapterArray = array();

    //S3から、該当タイトルの全漫画データ一覧取得、テキストファイル化。
    $fileArray = setGetS3FileList($title, $makeFromS3);
    foreach ($fileArray as $file) {

        $file = str_replace($folderPath . "/", "", $file);
        $paths = explode("/", $file);
        if (!empty($paths[0]) && $paths[0] != "title") {
            $chapterArray[] = $paths[0];
        }
    }

    $result = array_unique($chapterArray);
    $result = array_values($result);

    return $result;
}

/**
 * 該当の話の最初のページパスを取得（話毎のカバー画像に利用する）
 * 
 * @param  string  $folderPath  パス（例「comic/[タイトル]/1」）
 * @param  boolean S3から最新のファイル一覧を取得するか否か（true:取得する）
 * @return 結果（該当の話のページ数）
 */
function getFirstPagePath($folderPath, $makeFromS3 = true) {

    $paths = explode("/", $folderPath);
    $title = $paths[sizeof($paths) - 2];
    $capterNo = $paths[sizeof($paths) - 1];
    $result = "";

    //S3から、該当タイトルの全漫画データ一覧取得、テキストファイル化。
    $fileArray = setGetS3FileList($title, $makeFromS3);
    foreach ($fileArray as $file) {

        $paths = explode("/", $file);
        $coverFile = $paths[sizeof($paths) - 1];
        $chkCapterNo = $paths[sizeof($paths) - 2];
        if ($capterNo == $chkCapterNo && !empty($coverFile)) {
            $result = $file;
            break;
        }
    }
    return $result;
}

/**
 * 話毎のページ数取得
 * 
 * @param  string  $folderPath  パス（例「comic/[タイトル]/1」）
 * @param  boolean S3から最新のファイル一覧を取得するか否か（true:取得する）
 * @return 結果（該当の話のページ数）
 */
function getPageCnt($folderPath, $makeFromS3 = true) {

    $paths = explode("/", $folderPath);
    $title = $paths[sizeof($paths) - 2];
    $capterNo = $paths[sizeof($paths) - 1];
    $result = 0;

    //S3から、該当タイトルの全漫画データ一覧取得、テキストファイル化。
    $fileArray = setGetS3FileList($title, $makeFromS3);
    foreach ($fileArray as $file) {

        $paths = explode("/", $file);
        $chkCapterNo = $paths[sizeof($paths) - 2];
        if ($capterNo == $chkCapterNo) {
            $result++;
        }
    }
    return $result;
}
