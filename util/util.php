<?php

function redirect($url) {
    header("HTTP/1.1 301 Moved Permanently");
    header("Content-type: text/html; charset=utf-8");
    header("Location: " . $url);
// 	http_redirect($url, $arrPam, true, HTTP_REDIRECT_PERM);
    exit();
}

function printErr($errMsgArr) {
    $errMsg = '';
    $err_cd_list = $_SESSION['err_cd_list'];
    if (!empty($err_cd_list)) {
        foreach ($err_cd_list as $errCd) {
            $errMsg.=$errMsgArr[$errCd] . "<br>";
        }

        print($errMsg);
    }
}

function export_csv($filename, $data) {
    header("Content-type:text/csv");
    header("Content-Disposition:attachment;filename=" . $filename);
    header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
    header('Expires:0');
    header('Pragma:public');
    echo $data;
}

function input_csv($handle) {
    $out = array();
    $n = 0;
    while ($data = fgetcsv($handle, 10000)) {
        $num = count($data);
        for ($i = 0; $i < $num; $i++) {
            $out[$n][$i] = $data[$i];
        }
        $n++;
    }
    return $out;
}

//日本語版を使う　ただし、from_name　に日本語入れると文字化け
function sendMail($to, $subject, $body, $from_email, $from_name) {
    $headers = "MIME-Version: 1.0 \n";

    $headers .= "From: " .
        "" . mb_encode_mimeheader(mb_convert_encoding($from_name, "ISO-2022-JP", "AUTO")) . "" .
        "<" . $from_email . "> \n";
    $headers .= "Reply-To: " .
        "" . mb_encode_mimeheader(mb_convert_encoding($from_name, "ISO-2022-JP", "AUTO")) . "" .
        "<" . $from_email . "> \n";


    $headers .= "Content-Type: text/plain;charset=ISO-2022-JP \n";


    /* Convert body to same encoding as stated
      in Content-Type header above */

    $body = mb_convert_encoding($body, "ISO-2022-JP", "AUTO");

    /* Mail, optional paramiters. */
    $sendmail_params = "-f$from_email";

    mb_language("ja");
    $subject = mb_convert_encoding($subject, "ISO-2022-JP", "AUTO");
    $subject = mb_encode_mimeheader($subject);

//$subject =mb_encode_mimeheader( mb_convert_encoding($subject, "JIS", "EUC-JP") );
//$strMBS = mb_convert_encoding($strMailBodySend, "JIS", "EUC-JP");
//$strMS  = mb_encode_mimeheader( mb_convert_encoding($strMailSubj, "JIS", "EUC-JP") );

    $result = mail($to, $subject, $body, $headers, $sendmail_params);

    return $result;
}

//-----------下記クラスは文字化け発生するため廃止---
class smail {

    var $smtp = ADMIN_MAIL_SMTP;
    var $check = 1;
    var $username = ADMIN_MAIL;
    var $password = ADMIN_MAIL_PASS;
    var $s_from = ADMIN_MAIL;

    function smail($from, $password, $smtp, $check = 1) {
        if (preg_match("/^[^\d\-_][\w\-]*[^\-_]@[^\-][a-zA-Z\d\-]+[^\-](\.[^\-][a-zA-Z\d\-]*[^\-])*\.[a-zA-Z]{2,3}/", $from)) {
            //$this->username = substr( $from, 0, strpos( $from , "@" ) );
            $this->username = ADMIN_MAIL;
            $this->password = $password;
            $this->smtp = $smtp ? $smtp : $this->smtp;
            $this->check = $check;
            $this->s_from = $from;
        }
    }

    function send($to, $from, $subject, $message) {
        $fp = fsockopen($this->smtp, 25, $errno, $errstr, 60);
        if (!$fp)
            return "サーバ接続失敗" . __LINE__;
        set_socket_blocking($fp, true);
        $lastmessage = fgets($fp, 512);
        if (substr($lastmessage, 0, 3) != 220)
            return "Err1:$lastmessage" . __LINE__;
        $yourname = "YOURNAME";
        if ($this->check == "1")
            $lastact = "EHLO " . $yourname . "\r\n";
        else
            $lastact = "HELO " . $yourname . "\r\n";
        fputs($fp, $lastact);
        $lastmessage == fgets($fp, 512);
        if (substr($lastmessage, 0, 3) != 220)
            return "Err2:$lastmessage" . __LINE__;
        while (true) {
            $lastmessage = fgets($fp, 512);
            if ((substr($lastmessage, 3, 1) != "-") or ( empty($lastmessage)))
                break;
        }
        if ($this->check == "1") {
            $lastact = "AUTH LOGIN" . "\r\n";
            fputs($fp, $lastact);
            $lastmessage = fgets($fp, 512);
            if (substr($lastmessage, 0, 3) != 334)
                return "Err3:$lastmessage" . __LINE__;
            $lastact = base64_encode($this->username) . "\r\n";
            fputs($fp, $lastact);
            $lastmessage = fgets($fp, 512);
            if (substr($lastmessage, 0, 3) != 334)
                return "Err4:$lastmessage" . __LINE__;
            $lastact = base64_encode($this->password) . "\r\n";
            fputs($fp, $lastact);
            $lastmessage = fgets($fp, 512);
            if (substr($lastmessage, 0, 3) != "235")
                return "Err5:$lastmessage" . __LINE__;
        }
        //FROM:
        $lastact = "MAIL FROM: <" . $this->s_from . ">\r\n";
        fputs($fp, $lastact);
        $lastmessage = fgets($fp, 512);
        if (substr($lastmessage, 0, 3) != 250)
            return "Err6:$lastmessage" . __LINE__;
        //TO:
        $lastact = "RCPT TO: <" . $to . "> \r\n";
        fputs($fp, $lastact);
        $lastmessage = fgets($fp, 512);
        if (substr($lastmessage, 0, 3) != 250)
            return "Err7:$lastmessage" . __LINE__;
        //BCC:
//	if ($bcc!=""){
//		die($bcc);
//		$lastact="RCPT TO: <". $bcc ."> \r\n";
//		fputs( $fp, $lastact);
//		$lastmessage = fgets ($fp,512);
//		if (substr($lastmessage,0,3) != 250) return "Err8:bcc:$lastmessage".__LINE__;
//	}
        //DATA
        $lastact = "DATA\r\n";
        fputs($fp, $lastact);
        $lastmessage = fgets($fp, 512);
        if (substr($lastmessage, 0, 3) != 354)
            return "Err8:$lastmessage" . __LINE__;

        //Subject
        $head = "Subject: $subject\r\n";
        $message = $head . "\r\n" . $message;

        //From
        $head = "From: $from\r\n";
        $message = $head . $message;
        //To
        $head = "To: $to\r\n";
        $message = $head . $message;

        //処理Bcc
//	if ($bcc!=""){
//		$head="Bcc: $bcc\r\n";
//		$message = $head.$message;
//	}

        $message .= "\r\n.\r\n";
        fputs($fp, $message);
        $lastact = "QUIT\r\n";
        fputs($fp, $lastact);
        fclose($fp);
        return 0;
    }

}

function get_real_ip() {
    $ip = false;
    if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
        $ip = $_SERVER["HTTP_CLIENT_IP"];
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ips = explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
        if ($ip) {
            array_unshift($ips, $ip);
            $ip = FALSE;
        }
        for ($i = 0; $i < count($ips); $i++) {
            if (!eregi("^(10|172\.16|192\.168)\.", $ips[$i])) {
                $ip = $ips[$i];
                break;
            }
        }
    }
    return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}

function manlog($message, $mode = "err") {
    if (strstr(URL_PATH, 'mangazokuzoku.com') && $mode == "debug") {//本番サーバーでのデバッグ文か？
        return;
    }
    $date = date("Ymd");
    $dateTime = date("Y-m-d H:i:s");
    $filePath = dirname(dirname(__FILE__)) . "/log/{$mode}_{$date}.log";
    error_log("[{$dateTime}]" . $message . "\r\n", 3, $filePath);
}

?>